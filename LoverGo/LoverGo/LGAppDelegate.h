//
//  LGAppDelegate.h
//  LoverGo
//
//  Created by YeXiao on 14-1-10.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
