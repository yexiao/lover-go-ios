//
//  LGShowView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-7.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGShowView.h"

@implementation LGShowView
@synthesize webView = _webView;
@synthesize aIView  = _aIView;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor grayColor];
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"活动详情"];
        titleLab.userInteractionEnabled = true;
        [self addSubview:titleLab];
        
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, STATUSBAR +40, frame.size.width, frame.size.height)];
        _webView.scalesPageToFit =YES;
        [self addSubview:_webView];
        
        _aIView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
        [_aIView setCenter:self.center];
        [_aIView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite] ;
        [self addSubview:_aIView];
        
        
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:1];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        //...
        UIButton *rightBtn = [LGUtil returnRightBtn];
        [rightBtn setTag:2];
        [rightBtn setCenter:CGPointMake(290, STATUSBAR)];
        [rightBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:rightBtn];
        
    }
    return self;
}

-(void)btnClieck:(UIButton*)sender
{
    if (sender.tag == 2) {
        //
    }else{
        [_delegate backLeft];
    }
}
-(void)loadDetailAticleByURL:(NSString*)url
{
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];//创建NSURLRequest
    [_webView loadRequest:request];
}
@end
