//
//  LGShowViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-7.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtilDefine.h"
#import "Toast+UIView.h"
#import "LGShowViewController.h"

@interface LGShowViewController ()

@end

@implementation LGShowViewController

-(id)initWithURL:(NSString*)url
{
    self = [super init];
    if (self) {
        _url = url;
    }
    return self;
}
-(void)loadView
{
    _aView = [[LGShowView alloc] initWithFrame:CGRectMake(0, STATUSBAR +TOOLBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR +TOOLBAR))];
    _aView.webView.delegate = self;
    _aView.delegate = self;
    [self setView:_aView];
    [_aView loadDetailAticleByURL:[NSString stringWithFormat:@"%@lover-go/%@",baseUrl,_url]];
}
-(void)backLeft
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_aView.aIView startAnimating] ;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_aView.aIView stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.view makeToast:@"网页加载失败!"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
