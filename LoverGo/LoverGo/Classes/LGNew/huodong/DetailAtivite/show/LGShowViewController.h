//
//  LGShowViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-7.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGShowView.h"
#import "LXActionSheet.h"

@interface LGShowViewController : UIViewController<UIWebViewDelegate,LXActionSheetDelegate,LGShowViewDelegate>
{
    NSString* _url;
    LGShowView *_aView;
}
-(id)initWithURL:(NSString*)url;
@end
