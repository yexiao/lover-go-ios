//
//  LGShowView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-7.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGShowViewDelegate <NSObject>

-(void)backLeft;

@end

@interface LGShowView : UIView{
    
    UIWebView *_webView;
    UIActivityIndicatorView *_aIView;
    id<LGShowViewDelegate> _delegate;
}
@property(nonatomic,strong)UIActivityIndicatorView *aIView;
@property(nonatomic,strong)UIWebView* webView;
@property(nonatomic,strong)id<LGShowViewDelegate> delegate;

-(void)loadDetailAticleByURL:(NSString*)url;

@end
