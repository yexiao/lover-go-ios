//
//  DetailAtiviteViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGActivityInfo.h"
#import "LXActionSheet.h"
#import "LGLGOrderInfo.h"
#import "LGDetailAtiviteView.h"

@interface DetailAtiviteViewController : UIViewController<LGDetailAtiviteViewDelegate,LXActionSheetDelegate>
{
    LGDetailAtiviteView* _daView;
    LGActivityInfo* _info;
    LGLGOrderInfo* _orderInfo;
}
@property (nonatomic,strong) LXActionSheet *actionSheet;

-(id)initWithActivityInfo:(LGActivityInfo*)info;
-(void)reloadDetailAtiviteData:(id)response;
-(void)showToPay:(id)response;

@end
