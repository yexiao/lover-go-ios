//
//  DetailAtiviteViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "MJExtension.h"
#import "LGUserInfo.h"
#import "Toast+UIView.h"
#import "LGUtilModel.h"
#import "LGUtilDefine.h"
#import "SVHTTPClient.h"
#import "LGDataCenter.h"
#import "LGActivityImageInfo.h"
#import "LGShowViewController.h"
#import "LGUtil.h"
#import "DetailAtiviteViewController.h"
#import "LGOrderViewController.h"

@interface DetailAtiviteViewController ()

@end

@implementation DetailAtiviteViewController

-(id)initWithActivityInfo:(LGActivityInfo*)info
{
    self = [super init];
    if (self) {
        _info = info;
        _orderInfo = NULL;
        __block DetailAtiviteViewController *_blockSelf = self;
        NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                              info.uuid,@"activityUuid",
                              nil];
        [[SVHTTPClient sharedClient] GET:findActivityByUuidSPath
                              parameters:para
                              completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                                  if (error) {
                                      [_blockSelf.view makeToast:@"网络请求失败"];
                                      return ;
                                  }
                                  [_blockSelf reloadDetailAtiviteData:response];
                              }];
    }
    return self;
}
-(void)loadView
{
    _daView = [[LGDetailAtiviteView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - TOOLBAR - (40+44))];
    _daView.delegate = self;
    [self setView:_daView];
}
-(void)reloadDetailAtiviteData:(id)response
{
    if (response == nil) {
        [self.view makeToast:@"没有数据"];
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        [self.view makeToast:@"解析失败"];
    }else{
        LGUtilModel* _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        
        NSArray* listArray = [LGActivityInfo objectArrayWithKeyValuesArray:_utilModel.listData];
        if (listArray !=nil && listArray.count > 0) {
            _info =[listArray objectAtIndex:0];
            [_daView loadDetailAtivite:_info];
        }
    }
}
-(void)turnBackLeft
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)gotoShow
{
    LGShowViewController *vc = [[LGShowViewController alloc]initWithURL:_info.htmlName];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)addGroup:(LGDetailAtiviteView*)view
{
    self.actionSheet = [[LXActionSheet alloc]initWithTitle:@"是否报名参加此活动" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"确认" otherButtonTitles:nil];
    self.actionSheet.tag = 1;
    [self.actionSheet showInView:self.view];
    
}
-(void)imageShow:(LGDetailAtiviteView *)view
{
    __block DetailAtiviteViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          view.aInfo.placeUuid,@"uuid",
                          nil];
    [[SVHTTPClient sharedClient] GET:getImageByUuid
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  [_blockSelf.view makeToast:@"请求网络失败"];
                                  return ;
                              }if (response == nil) {
                                  [_blockSelf.view makeToast:@"没有数据"];
                                  return;
                              }
                              NSError *jsonError = nil;
                              id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
                              if (jsonError) {
                                  [_blockSelf.view makeToast:@"解析失败"];
                                  return;
                              }else{
                                  NSArray *tmpArray = [LGActivityImageInfo objectArrayWithKeyValuesArray:[jsonObject objectForKey:@"list"]];
                                  if (tmpArray.count == 0) {
                                      [_blockSelf.view makeToast:@"没有更多图片了"];
                                      return;
                                  }
                                  
                                  [LGUtil showImages:tmpArray];
                              }
                          }];
    
}
#pragma mark - LXActionSheetDelegate

- (void)didClickOnButtonIndex:(NSInteger *)buttonIndex
{
    
    NSLog(@"%d",(int)buttonIndex);
}

- (void)didClickOnDestructiveButton
{
    if (self.actionSheet.tag == 1) {
        //报名
        if (_info == NULL) {
            return;
        }
        LGUserInfo  *uInfo= [LGDataCenter getSingleton].userInfo;
        if (uInfo == nil) {
            //用户未设置数据，请跳转至数据界面
            [self.view makeToast:@"先登录才能参与活动哦"];
            return;
        }
        CFUUIDRef theUUID = CFUUIDCreate(NULL);
        CFStringRef guid = CFUUIDCreateString(NULL, theUUID);
        CFRelease(theUUID);
        NSString *uuidString = [((__bridge NSString *)guid) stringByReplacingOccurrencesOfString:@"-" withString:@""];
        CFRelease(guid);
        srand((unsigned)time(NULL));
//        NSLog(@"email:%@ activityUuid:%@ orderUuid:%@ orderName:%@ userUuid:%@",uInfo.mobile,_info.uuid,[NSString stringWithFormat:@"%@%d",[uuidString lowercaseString],rand()%10000],_info.title,uInfo.uuid);
        
        NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                              _info.uuid,@"activityUuid",
                              [NSString stringWithFormat:@"%@%d",[uuidString lowercaseString],rand()%10000],@"orderUuid",
                               _info.title,@"orderName",
                              uInfo.uuid,@"appUserUuid",
                              uInfo.mobile,@"email",//手机号
                              //                          uInfo.userName,@"name",
                              nil];
        //中文问题，所有改用post请求
        __block DetailAtiviteViewController *_blockSelf = self;
        [[SVHTTPClient sharedClient] POST:addActivityOrderSPath
                              parameters:para
                              completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                                  if (error) {
                                      //抛报错通知
                                      [self.view makeToast:@"订单提交失败"];
                                      return ;
                                  }
                                  [_blockSelf showToPay:response];
                              }];
        
    }else{
        LGOrderViewController *ocl = [[LGOrderViewController alloc] initWithOrderInfo:_orderInfo];
        [self.navigationController pushViewController:ocl animated:YES];
    }
}
-(void)showToPay:(id)response
{
    if (response == nil) {
        [self.view makeToast:@"订单提交失败"];
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        [self.view makeToast:@"订单提交失败"];
    }else{
        LGUtilModel* _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        
        NSArray* listArray = [LGLGOrderInfo objectArrayWithKeyValuesArray:_utilModel.listData];
        if (listArray !=nil && listArray.count > 0) {
            _orderInfo =[listArray objectAtIndex:0];
            if (_orderInfo) {
                self.actionSheet = [[LXActionSheet alloc]initWithTitle:@"订单提交成功，是否跳转前去支付" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"确认" otherButtonTitles:nil];
                self.actionSheet.tag = 2;
                [self.actionSheet showInView:self.view];
            }
        }
    }
}

- (void)didClickOnCancelButton
{
    NSLog(@"cancelButton");
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
