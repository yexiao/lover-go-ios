//
//  LGDetailAtiviteView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGActivityInfo.h"
#import "EMAsyncImageView.h"

@class LGDetailAtiviteView;

@protocol LGDetailAtiviteViewDelegate <NSObject>

-(void)turnBackLeft;
-(void)gotoShow;
-(void)addGroup:(LGDetailAtiviteView*)view;
-(void)imageShow:(LGDetailAtiviteView*)view;

@end

@interface LGDetailAtiviteView : UIView
{
    EMAsyncImageView *_itemIView;
    UIScrollView *_contentScrollView;
    LGActivityInfo* _aInfo;
    UIButton *_addBtn;
    id<LGDetailAtiviteViewDelegate> _delegate;
}

@property(nonatomic,strong)LGActivityInfo *aInfo;
@property(nonatomic,strong)id<LGDetailAtiviteViewDelegate> delegate;

-(void)loadDetailAtivite:(LGActivityInfo*)info;

@end
