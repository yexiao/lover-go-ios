//
//  LGDetailAtiviteView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGUtilDefine.h"
#import "EMAsyncImageView.h"
#import "LGDetailAtiviteView.h"

@implementation LGDetailAtiviteView

@synthesize aInfo = _aInfo;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor grayColor];
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"活动详情"];
        titleLab.userInteractionEnabled = true;
        [self addSubview:titleLab];
        
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:1];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        _itemIView = [[EMAsyncImageView alloc] initWithFrame:CGRectMake(1, STATUSBAR+41, 320, 196)];//701,466
        [self addSubview:_itemIView];
        _itemIView.userInteractionEnabled = YES;
        
        UILabel* _titleLab = [self getLabelWithText:@"点击查看更多图片" fontSize:18];
        [_titleLab setBackgroundColor:[UIColor lightGrayColor]];
        [_titleLab setCenter:CGPointMake(160, 180)];
        [_titleLab setTextAlignment:NSTextAlignmentCenter];
        [_itemIView addSubview:_titleLab];
        
    }
    return self;
}

-(void)btnClieck:(UIButton*)sender
{
    [_delegate turnBackLeft];
}
-(void)imageClieck:(id)sender
{
    [_delegate imageShow:self];
}

-(void)loadDetailAtivite:(LGActivityInfo*)info
{
    _aInfo = info;
    if ([info.indexImage compare:@""] != 0) {
        _itemIView.imageUrl = [NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,info.indexImage];
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClieck:)];
        [_itemIView addGestureRecognizer:singleTap];
    }else{
        [_itemIView setImage:[LGUtil getUnFoundImage]];
    }
    
    _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 200+STATUSBAR+41, SCREEN_WIDTH, SCREEN_HEIGHT - (200+STATUSBAR+41))];
    _contentScrollView.backgroundColor = [UIColor grayColor];
    [_contentScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, 330)];
    [self addSubview:_contentScrollView];
    
    UILabel* _titleLab = [self getLabelWithText:info.title fontSize:18];
    [_titleLab setCenter:CGPointMake(160, 12)];
    [_contentScrollView addSubview:_titleLab];
    
    UILabel* _numLab =[self getLabelWithText:[NSString stringWithFormat:@"\n人均%0.2f元",[info.price floatValue]] fontSize:15];
    _numLab.lineBreakMode = UILineBreakModeWordWrap;
    _numLab.numberOfLines = 0;
    [_numLab setFrame:CGRectMake(5, 26, 310, 50)];
    _numLab.userInteractionEnabled = true;
    [_contentScrollView addSubview:_numLab];
    
    _addBtn = [LGUtil get9ScalBtn:CGRectMake(205, 5, 100, 40) type:TypeColorOrange];
    [_addBtn setTag:1];
    if ([info.canJoin intValue] == 0) {
        [_addBtn setTitle:@"活动已截止" forState:UIControlStateNormal];
    }else{
        [_addBtn setTitle:@"点击参与" forState:UIControlStateNormal];
        [_addBtn addTarget:self action:@selector(addBtnClieck:) forControlEvents:UIControlEventTouchUpInside];
    }
    [_numLab addSubview:_addBtn];
    
    //
    UILabel* _totalLab = [self getLabelWithText:@"参与人数(已参加/总名额)" fontSize:18];
    [_totalLab setCenter:CGPointMake(160, 100)];
    [_contentScrollView addSubview:_totalLab];
    UILabel* _tLab = [self getLabelWithText:[NSString stringWithFormat:@"%@/%@",info.joinNum,info.activityNum] fontSize:15];
    [_tLab setCenter:CGPointMake(160, 126)];
    [_contentScrollView addSubview:_tLab];
    
    UILabel* _timelLab = [self getLabelWithText:@"活动时间" fontSize:18];
    [_timelLab setCenter:CGPointMake(160, 162)];
    [_contentScrollView addSubview:_timelLab];
    UILabel* _atimeLabb = [self getLabelWithText:[NSString stringWithFormat:@"%@",info.actTime] fontSize:15];
    [_atimeLabb setCenter:CGPointMake(160, 188)];
    [_contentScrollView addSubview:_atimeLabb];
    
    UILabel* _endTimelLab = [self getLabelWithText:@"报名截止时间" fontSize:18];
    [_endTimelLab setCenter:CGPointMake(160, 224)];
    [_contentScrollView addSubview:_endTimelLab];
    UILabel* _eTimeLabb = [self getLabelWithText:[NSString stringWithFormat:@"%@",info.endTime] fontSize:15];
    [_eTimeLabb setCenter:CGPointMake(160, 250)];
    [_contentScrollView addSubview:_eTimeLabb];
    
    UILabel* _desLab = [self getLabelWithText:@"活动简介" fontSize:18];
    [_desLab setFrame:CGRectMake(5, 280, 310, 40)];
    _desLab.userInteractionEnabled = true;
    [_contentScrollView addSubview:_desLab];
    UITapGestureRecognizer *tapGestureTel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelEvent:)];
    [_desLab addGestureRecognizer:tapGestureTel];
    
    //右边箭头
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"next_pic.png"]];
    [imgView setCenter:CGPointMake(290, STATUSBAR)];
    [_desLab addSubview:imgView];
    
}
-(void)labelEvent:(UILabel*)sender
{
    [_delegate gotoShow];
}
-(void)addBtnClieck:(UIButton*)sender
{
    [_delegate addGroup:self];
}

-(UILabel* )getLabelWithText:(NSString*)title fontSize:(int)fSize
{
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 310, 25)];
    [label setBackgroundColor:[UIColor whiteColor]];
    [label setShadowColor:[UIColor whiteColor]];
    [label setShadowOffset:CGSizeMake(0.1, 0.1)];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:fSize];
    [label setFont:font];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label setText:title];
    return label;
}
@end
