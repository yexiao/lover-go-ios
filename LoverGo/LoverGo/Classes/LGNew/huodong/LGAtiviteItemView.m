//
//  LGAtiviteItemView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGUtilDefine.h"
#import "LGAtiviteItemView.h"

@implementation LGAtiviteItemView

-(void)setDefauleLabel:(UILabel*)lab
{
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:15];
    [lab setFont:font];
    [lab setBackgroundColor:[UIColor clearColor]];
    [lab setShadowColor:[UIColor whiteColor]];
    [lab setShadowOffset:CGSizeMake(0.5, 0.5)];
    [lab setTextColor:[UIColor blackColor]];
    [lab setTextAlignment:NSTextAlignmentLeft];
}

- (id)init
{
    self = [super init];
    if (self) {
        
        _iconIView = [[EMAsyncImageView alloc] initWithFrame:CGRectMake(2, 2, 154, 96) cornerRadius:0];//701,466
        [self addSubview:_iconIView];
        _iconIView.userInteractionEnabled = YES;
        
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(160, 5, 160, 50)];
        [self setDefauleLabel:_titleLab];
        [self addSubview:_titleLab];
        
        _timeLab = [[UILabel alloc] initWithFrame:CGRectMake(160, 50, 160, 50)];
        [self setDefauleLabel:_timeLab];
        [self addSubview:_timeLab];
        
        _numLab = [[UILabel alloc] initWithFrame:CGRectMake(160, 75, 160, 25)];
        [self setDefauleLabel:_numLab];
        [_numLab setTextColor:[UIColor blueColor]];
        [_numLab setTextAlignment:NSTextAlignmentRight];
        [self addSubview:_numLab];
    }
    return self;
}

-(void)reloadAtiviteData:(LGActivityInfo*)info
{
    _timeLab.lineBreakMode = UILineBreakModeWordWrap;
    _timeLab.numberOfLines = 0;
    [_timeLab setText:info.actTime];
    [_numLab setText:[NSString stringWithFormat:@"%@/%@",info.joinNum,info.activityNum]];
    _titleLab.lineBreakMode = UILineBreakModeWordWrap;
    _titleLab.numberOfLines = 0;
    NSString* cStr =info.title;
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:15];
    [_titleLab setFont:font];
    [_titleLab setText:cStr];
    CGSize labelsize = [cStr sizeWithFont:font constrainedToSize:CGSizeMake(160, 2000) lineBreakMode:NSLineBreakByCharWrapping];
    [_titleLab setFrame:CGRectMake(160, 5, labelsize.width, labelsize.height)];
    
    if ([info.indexImage compare:@""] != 0) {
        _iconIView.imageUrl = [NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,info.indexImage];
    }else
    {
        [_iconIView setImage:[LGUtil getUnFoundImage]];
    }
}
-(void)removeAll
{
    _iconIView.image = NULL;
    [_numLab setText:NULL];
    [_titleLab setText:NULL];
}

@end
