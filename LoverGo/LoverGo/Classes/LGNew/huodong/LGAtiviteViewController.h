//
//  LGAtiviteViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGAtiviteView.h"
#import "LGUtilModel.h"

@interface LGAtiviteViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    LGAtiviteView *_ativiteView;
    NSMutableArray* _ativiteArray;
    LGUtilModel *_utilModel;
}
-(void)loadFirst;
-(void)refreshView:(id)response ativitePage:(int)page;
-(void)sendRequestByAtivitePage:(int)page;
@end
