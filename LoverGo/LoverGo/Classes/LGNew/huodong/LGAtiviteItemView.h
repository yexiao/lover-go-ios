//
//  LGAtiviteItemView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGActivityInfo.h"
#import "EMAsyncImageView.h"

@interface LGAtiviteItemView : UITableViewCell
{
    EMAsyncImageView* _iconIView;
    UILabel *_titleLab;
    UILabel *_numLab;
    UILabel *_timeLab;
}
-(void)reloadAtiviteData:(LGActivityInfo*)info;

-(void)removeAll;
@end
