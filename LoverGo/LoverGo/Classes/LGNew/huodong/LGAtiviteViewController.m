//
//  LGAtiviteViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "MJExtension.h"
#import "Toast+UIView.h"
#import "LGActivityInfo.h"
#import "LGAtiviteItemView.h"
#import "LGRequestManager.h"
#import "LGAtiviteViewController.h"
#import "DetailAtiviteViewController.h"

@interface LGAtiviteViewController ()

@end

@implementation LGAtiviteViewController
-(void)loadView
{
    _ativiteArray = [[NSMutableArray alloc] init];
    _ativiteView = [[LGAtiviteView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - STATUSBAR - TOOLBAR - (40+44))];
    _ativiteView.tableView.dataSource = self;
    _ativiteView.tableView.delegate = self;
    [self setView:_ativiteView];
    _utilModel = nil;
}
-(void)loadFirst
{__block LGAtiviteViewController *_blockSelf = self;
    _ativiteView.header.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //发送初始请求
        [_blockSelf sendRequestByAtivitePage:1];
    };
    [_ativiteView.header beginRefreshing];
    
    _ativiteView.footer.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //进入刷新状态
        if (_blockSelf->_utilModel) {
            int nextPage = [_blockSelf->_utilModel.nowPage intValue] +1;
            if (nextPage > [_blockSelf->_utilModel.totalPage intValue]) {
                [_blockSelf->_ativiteView.footer endRefreshing];
                [_blockSelf.view makeToast:@"没有下一页了"];
                return ;
            }
            [_blockSelf sendRequestByAtivitePage:nextPage];
        }
    };
}
-(void)sendRequestByAtivitePage:(int)page
{
    __block LGAtiviteViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          @"tj",@"regionCode",
                          [NSString stringWithFormat:@"%d",page],@"page",
                          nil];
    [[SVHTTPClient sharedClient] GET:findActivityByMapSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  //抛报错通知
                                  [_blockSelf.view makeToast:@"网络连接失败"];
                                  return ;
                              }
                              [_blockSelf refreshView:response ativitePage:page];
                          }];
}
-(void)refreshView:(id)response ativitePage:(int)page
{
    [_ativiteView.header endRefreshing];
    [_ativiteView.footer endRefreshing];
    if (response == nil) {
        [self.view makeToast:@"没有数据"];
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        [self.view makeToast:@"数据解析失败"];
        return;
    }else{
        if (page == 1) {
            [_ativiteArray removeAllObjects];
        }
        _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        [_ativiteArray addObjectsFromArray:[LGActivityInfo objectArrayWithKeyValuesArray:_utilModel.listData]];
        [_ativiteView.tableView reloadData];//刷新数据
        
    }
}
#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGActivityInfo *aInfo= [_ativiteArray objectAtIndex:indexPath.row];

    DetailAtiviteViewController *vc = [[DetailAtiviteViewController alloc] initWithActivityInfo:aInfo];
    //获取view的controller
    for (UIView* next = [self.view superview]; next; next = next.superview){
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            UIViewController *mV = (UIViewController *)nextResponder;
            [mV.navigationController pushViewController:vc animated:YES];
            break;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _ativiteArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGAtiviteItemView * cell = [[LGAtiviteItemView alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    LGActivityInfo *aInfo= [_ativiteArray objectAtIndex:indexPath.row];
    [cell reloadAtiviteData:aInfo];
    return cell;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
