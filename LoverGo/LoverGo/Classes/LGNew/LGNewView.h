//
//  LGNewView.h
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QCSlideSwitchView.h"

@interface LGNewView : UIView
{
    QCSlideSwitchView *_slideSwitchView;
}
@property (nonatomic, strong) QCSlideSwitchView *slideSwitchView;

@end
