//
//  LGNewViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGNewView.h"
#import "LGAtiviteViewController.h"
#import "LGAticleViewController.h"
#import "LGRoadViewController.h"
#import "BaseViewController.h"

@interface LGNewViewController : UIViewController<QCSlideSwitchViewDelegate>
{
    LGNewView *_newView;
    LGRoadViewController *_vc1;
    LGAticleViewController *_vc2;
    LGAtiviteViewController *_vc3;
}


-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype;

@end
