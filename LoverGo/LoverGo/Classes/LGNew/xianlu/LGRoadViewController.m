//
//  LGRoadViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "MJExtension.h"
#import "Toast+UIView.h"
#import "LGCityCodeInfo.h"
#import "LGRoadItemView.h"
#import "LGRequestManager.h"
#import "LGRoadViewController.h"
#import "DetailRoadViewController.h"


@implementation LGRoadViewController

-(void)loadView
{
    _roadArray = [[NSMutableArray alloc] init];
    _roadView = [[LGRoadView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - TOOLBAR - (40+44))];
    _roadView.tableView.dataSource = self;
    _roadView.tableView.delegate = self;
    [self setView:_roadView];
    _utilModel = nil;
    
}
-(void)loadFirst
{
    __block LGRoadViewController *_blockSelf = self;
    _roadView.header.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //发送初始请求
        [_blockSelf sendRequestByPage:1];
    };
    _roadView.header.endStateChangeBlock = ^(MJRefreshBaseView* refreshViews){
        //刷新完毕
        NSLog(@"刷新完毕");
        
    };
    _roadView.header.refreshStateChangeBlock = ^(MJRefreshBaseView* refreshViews,MJRefreshState state){
        //状态改变
    };
    [_roadView.header beginRefreshing];
    
    _roadView.footer.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //进入刷新状态
        if (_blockSelf->_utilModel) {
            int nextPage = [_blockSelf->_utilModel.nowPage intValue] +1;
            if (nextPage > [_blockSelf->_utilModel.totalPage intValue]) {
                [_blockSelf->_roadView.footer endRefreshing];
                [_blockSelf.view makeToast:@"没有下一页了"];
                return ;
            }
            [_blockSelf sendRequestByPage:nextPage];
        }
    };
}
-(void)refreshView:(id)response page:(int)page
{
    [_roadView.header endRefreshing];
    if (response == nil) {
        [self.view makeToast:@"加载失败!请检查网络"];
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        [self.view makeToast:@"数据解析错误"];
    }else{
        if (page == 1) {
            [_roadArray removeAllObjects];
        }
        _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        [_roadArray addObjectsFromArray:[LGCityCodeInfo objectArrayWithKeyValuesArray:_utilModel.listData]];
        [_roadView.tableView reloadData];//刷新数据
        
    }
}
-(void)sendRequestByPage:(int)page
{
    __block LGRoadViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          @"tj",@"cityCode",
                          [NSString stringWithFormat:@"%d",page],@"page",
                          nil];
    [[SVHTTPClient sharedClient] GET:findListByCityCodeSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  [_blockSelf.view makeToast:@"网络连接失败"];
                              }
                              [_blockSelf refreshView:response page:page];
                          }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGCityCodeInfo *ccInfo= [_roadArray objectAtIndex:indexPath.row];
    
    DetailRoadViewController *vc = [[DetailRoadViewController alloc] initWithCityCodeInfo:ccInfo];
    //获取view的controller
    for (UIView* next = [self.view superview]; next; next = next.superview){
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            UIViewController *mV = (UIViewController *)nextResponder;
            [mV.navigationController pushViewController:vc animated:YES];
            break;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _roadArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGRoadItemView * cell = [[LGRoadItemView alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    LGCityCodeInfo *ccInfo= [_roadArray objectAtIndex:indexPath.row];
    [cell reloadRoadData:ccInfo];
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
