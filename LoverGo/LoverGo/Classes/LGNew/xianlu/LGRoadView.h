//
//  LGRoadView.h
//  LoverGo
//
//  Created by YeXiao on 14-5-25.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"

@class LGRoadView;


@interface LGRoadView : UIView
{
    MJRefreshHeaderView *_header;
    MJRefreshFooterView *_footer;
    UITableView* _tableView;
}

@property(nonatomic,strong)MJRefreshHeaderView *header;
@property(nonatomic,strong)MJRefreshFooterView *footer;
@property(nonatomic,strong)UITableView* tableView;


@end
