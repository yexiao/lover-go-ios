//
//  LGRoadViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGRoadView.h"
#import "LGUtilModel.h"

@interface LGRoadViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    LGRoadView *_roadView;
    NSMutableArray* _roadArray;
    LGUtilModel *_utilModel;
}
-(void)loadFirst;
-(void)refreshView:(id)response page:(int)page;

@end
