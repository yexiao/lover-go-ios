//
//  LGRoadItemView.h
//  LoverGo
//
//  Created by YeXiao on 14-5-25.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMAsyncImageView.h"
#import "LGCityCodeInfo.h"

@interface LGRoadItemView : UITableViewCell
{
    EMAsyncImageView* _iconIView;
    UILabel *_titleLab;
    UILabel *_timeLab;
    UILabel *_userLab;
    UILabel *_costLab;
}
-(void)reloadRoadData:(LGCityCodeInfo*)info;
-(void)removeAll;
@end
