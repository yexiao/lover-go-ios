//
//  LGDetailRoadView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGUtilDefine.h"
#import "EMAsyncImageView.h"
#import "LGRoadDetailInfo.h"
#import "LGDetailRoadView.h"

@implementation LGDetailRoadView

@synthesize tView = _tView;
@synthesize delegate = _delegate;


-(UIScrollView*)getLabel:(NSString*)str frame:(CGRect)f
{
    //自适应文字
    UIScrollView *labScrollView= [[UIScrollView alloc] initWithFrame:f];
    UILabel*_contentLab = [[UILabel alloc] init];
    [_contentLab setBackgroundColor:[UIColor lightGrayColor]];
    [_contentLab setShadowColor:[UIColor grayColor]];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:15];
    [_contentLab setFont:font];
    [_contentLab setShadowOffset:CGSizeMake(0.05, 0.05)];
    [_contentLab setTextColor:[UIColor blackColor]];
    [_contentLab setTextAlignment:NSTextAlignmentLeft];
    _contentLab.lineBreakMode = UILineBreakModeWordWrap;
    _contentLab.numberOfLines = 0;
    NSString* cStr =str;
    [_contentLab setText:cStr];
    [labScrollView addSubview:_contentLab];
    CGSize labelsize = [str sizeWithFont:font constrainedToSize:CGSizeMake(f.size.width, 2000) lineBreakMode:NSLineBreakByWordWrapping];
    [_contentLab setFrame:CGRectMake(0, 0, labelsize.width, labelsize.height)];
    [labScrollView setContentSize:labelsize];
    return labScrollView;
}
- (id)initWithFrame:(CGRect)frame info:(LGCityCodeInfo*)info
{
    return [self initWithFrame:frame info:info haveR:YES];
}
- (id)initWithFrame:(CGRect)frame info:(LGCityCodeInfo*)info haveR:(BOOL)b
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor lightGrayColor]];
        
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"线路"];
        titleLab.userInteractionEnabled = true;
        [self addSubview:titleLab];
        //返回按钮
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:1];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        if (b) {
            //收藏分享按钮returnRightBtn
            UIButton *rightBtn = [LGUtil returnRightBtn];
            [rightBtn setTag:2];
            [rightBtn setCenter:CGPointMake(290, 20)];
            [rightBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
            [titleLab addSubview:rightBtn];
            
            _tView = [[UITableView alloc]initWithFrame:CGRectMake(0,STATUSBAR+40, frame.size.width, frame.size.height) style:UITableViewStylePlain];
            _tView.backgroundColor = [UIColor clearColor];
            [self addSubview:_tView];
        }
        
    }
    return self;
}

-(void)btnClieck:(UIButton*)sender
{
    if (sender.tag ==2) {
        [_delegate turnBackRight];
    }else{
        [_delegate turnBackLeft];
    }
}
@end
