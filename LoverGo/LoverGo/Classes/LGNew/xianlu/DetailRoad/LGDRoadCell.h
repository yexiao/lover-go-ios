//
//  LGDRoadCell.h
//  LoverGo
//
//  Created by YeXiao on 14-6-20.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMAsyncImageView.h"
#import "LGRoadDetailInfo.h"

@protocol LGDRoadCellDelegate <NSObject>

-(void)itemClick:(NSString*)placeUUid;

@end


@interface LGDRoadCell : UITableViewCell
{
    UILabel *_tLabel;
    UILabel *_cLabel;
    UIImageView* _tipIView;
    EMAsyncImageView *_itemIView;
    LGRoadDetailInfo *_rdInfo;
    id<LGDRoadCellDelegate> _delegate;
}
@property(nonatomic,strong)id<LGDRoadCellDelegate> delegete;

-(void)reloadData:(LGRoadDetailInfo *)rdInfo;
@end
