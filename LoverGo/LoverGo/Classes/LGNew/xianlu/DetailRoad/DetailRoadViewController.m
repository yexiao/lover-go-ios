//
//  DetailRoadViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "Toast+UIView.h"
#import "MJExtension.h"
#import "LGUtilModel.h"
#import "LGPlaceInfo.h"
#import "LGUtilDefine.h"
#import "SVHTTPClient.h"
#import "LGDRoadCell.h"
#import "LGDataCenter.h"
#import "LGRoadDetailInfo.h"
#import "LGPlaceDetailViewController.h"
#import "DetailRoadViewController.h"
#import "UMSocialShakeService.h"
#import "UMSocialScreenShoter.h"


@interface DetailRoadViewController ()

@end

@implementation DetailRoadViewController

-(id)initWithCityCodeInfo:(LGCityCodeInfo*)info
{
    return [self initWithCityCodeInfo:info haveRight:YES];
}
-(id)initWithCityCodeInfo:(LGCityCodeInfo *)info haveRight:(BOOL)b
{
    self = [super init];
    if (self) {
        _isHaveR = b;
        _info = info;
        __block DetailRoadViewController *_blockSelf = self;
        NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                              info.uuid,@"uuid",
                              nil];
        [[SVHTTPClient sharedClient] GET:findRouteByUuidSPath
                              parameters:para
                              completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                                  if (error) {
                                      //抛报错通知
                                      return ;
                                  }
                                  [_blockSelf reloadDetailData:response];
                              }];
    }
    return self;
}
-(void)loadView
{
    _listArray = [[NSArray alloc] init];
    _dRoadView = [[LGDetailRoadView alloc] initWithFrame:CGRectMake(0, STATUSBAR +TOOLBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR +TOOLBAR)) info:_info haveR:_isHaveR];
    _dRoadView.delegate = self;
    _dRoadView.tView.dataSource = self;
    _dRoadView.tView.delegate = self;
    [self setView:_dRoadView];
    _shareImgView = nil;
}
-(void)reloadDetailData:(id)response
{
    if (response == nil) {
        NSLog(@"数据为空啦");
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        NSLog(@"解析失败");
    }else{
        LGUtilModel* _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        
        _listArray = [LGRoadDetailInfo objectArrayWithKeyValuesArray:_utilModel.listData];
        [_dRoadView.tView reloadData];
    }
}
-(void)turnBackLeft
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)itemClieck:(NSString *)placeUUid
{
    LGPlaceInfo *pInfo= [[LGPlaceInfo alloc]init];
    pInfo.uuid = placeUUid;
    
    LGPlaceDetailViewController *vc = [[LGPlaceDetailViewController alloc] initWithPlaceInfo:pInfo];
    
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)turnBackRight
{
    LXActionSheet* asheet = [[LXActionSheet alloc]initWithTitle:@"喜欢就收藏和分享吧！" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"收藏" otherButtonTitles:@[@"分享"]];
    [asheet showInView:self.view];
}

#pragma mark - LXActionSheetDelegate

- (void)didClickOnButtonIndex:(NSInteger *)buttonIndex
{
    if ((int)buttonIndex == 1) {
        UIImage *shareImg = nil;
        if (_shareImgView) {
            shareImg = _shareImgView.image;
        }
        [LGUtil shareToSNSURL:[NSString stringWithFormat:@"%@%@?routeUuid=%@",baseUrl,getFindRouteInfoSPath,_info.uuid] title:@"情侣去哪儿" img:shareImg content:_info.routeName
                   controller:self];
    }
}

- (void)didClickOnDestructiveButton
{
    //收藏按钮
    LGUserInfo* info =[LGDataCenter getSingleton].userInfo;
    if (info == NULL) {
        [self.view makeToast:@"请先登录才能收藏哦!"];
        return;
    }
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          info.uuid,@"userUuid",
                          _info.uuid,@"objectUuid",
                          @"route",@"collectionType",
                          nil];
    [[SVHTTPClient sharedClient] GET:addCollectionByUserSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  [self.view makeToast:@"请求失败!"];
                                  return ;
                              }
                              [self.view makeToast:@"收藏成功!"];
                          }];
}

- (void)didClickOnCancelButton
{
    NSLog(@"cancelButton donothing");
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1+_listArray.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        UITableViewCell* cell = [[UITableViewCell alloc]init];
        
        _shareImgView = [[EMAsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)cornerRadius:0];
        _shareImgView.userInteractionEnabled = true;
        if ([_info.headImg compare:@""] == 0) {
            [_shareImgView setImage:[LGUtil getUnFoundImage]];
        }else
        {
            _shareImgView.imageUrl = [NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,_info.headImg];
        }
        [cell addSubview:_shareImgView];
        
        UIImageView *pIView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"place_Item.png"]];
        [pIView setCenter:CGPointMake(40, 150)];
        [cell addSubview:pIView];
        
        UILabel* _tLab = [[UILabel alloc] initWithFrame:CGRectMake(51, 135, 269, 25)];
        [_tLab setBackgroundColor:[UIColor clearColor]];
        [_tLab setShadowColor:[UIColor grayColor]];
        [_tLab setFont:[UIFont fontWithName:@"Helvetica" size:22]];
        [_tLab setShadowOffset:CGSizeMake(0.05, 0.05)];
        [_tLab setTextColor:[UIColor whiteColor]];
        [_tLab setTextAlignment:NSTextAlignmentLeft];
        [cell addSubview:_tLab];
        [_tLab setText:_info.routeName];
        
        UIImageView *plineView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"place_Item_line.png"]];
        [plineView setCenter:CGPointMake(40, 150)];
        [cell addSubview:plineView];
        
        CGSize size = [_info.routeDesc sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(316, 20000)];
        UILabel* _tLabel=[[UILabel alloc]initWithFrame:CGRectMake(2, 201, 316, 50)];
        _tLabel.numberOfLines=0;
        _tLabel.lineBreakMode=NSLineBreakByCharWrapping;
        [_tLabel setBackgroundColor:[UIColor lightGrayColor]];
        [_tLabel setFont:[UIFont systemFontOfSize:16]];
        [_tLabel setTextColor:[UIColor blackColor]];
        [_tLabel setTextAlignment:NSTextAlignmentLeft];
        [cell addSubview:_tLabel];
        _tLabel.frame=CGRectMake(2, 201, 316, size.height);
        [_tLabel setText:_info.routeDesc];
        return cell;
    }else{
        
        LGDRoadCell *cell=[[LGDRoadCell alloc] init];
        cell.delegete = self;
        [cell reloadData:[_listArray objectAtIndex:indexPath.row -1]];
        LGRoadDetailInfo *info =[_listArray objectAtIndex:indexPath.row -1];
        
            NSLog(@"index:%d url:%@",indexPath.row,[NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,info.signImg]);
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        CGSize size = [_info.routeDesc sizeWithFont:[UIFont systemFontOfSize:16.0f] constrainedToSize:CGSizeMake(316, 20000)];
        return size.height+200;
    }else
    {
        LGRoadDetailInfo *rdInfo =[_listArray objectAtIndex:indexPath.row -1];
        CGSize size = [rdInfo.content sizeWithFont:[UIFont systemFontOfSize:16.0f] constrainedToSize:CGSizeMake(316, 20000)];
        CGFloat height = size.height;
        if ([rdInfo.signImg compare:@""] != 0){
            height += 200;
        }
        height += 25;
        return height;
    }
}
-(void)itemClick:(NSString*)placeUUid
{
    LGPlaceInfo *pInfo= [[LGPlaceInfo alloc]init];
    pInfo.uuid = placeUUid;
    
    LGPlaceDetailViewController *vc = [[LGPlaceDetailViewController alloc] initWithPlaceInfo:pInfo];
    
    [self.navigationController pushViewController:vc animated:YES];
}
@end
