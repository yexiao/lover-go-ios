//
//  DetailRoadViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGDRoadCell.h"
#import "LGCityCodeInfo.h"
#import "LXActionSheet.h"
#import "LGDetailRoadView.h"
#import "UMSocialShakeService.h"

@interface DetailRoadViewController : UIViewController<LGDetailRoadViewDelegate,LXActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,LGDRoadCellDelegate,UMSocialUIDelegate>{
    LGCityCodeInfo* _info;
    LGDetailRoadView* _dRoadView;
    NSArray* _listArray;
    EMAsyncImageView *_shareImgView;
    BOOL _isHaveR;
}
-(id)initWithCityCodeInfo:(LGCityCodeInfo*)info;
-(id)initWithCityCodeInfo:(LGCityCodeInfo*)info haveRight:(BOOL)b;
-(void)reloadDetailData:(id)response;

@end
