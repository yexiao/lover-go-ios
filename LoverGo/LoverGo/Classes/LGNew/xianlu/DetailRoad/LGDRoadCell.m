//
//  LGDRoadCell.m
//  LoverGo
//
//  Created by YeXiao on 14-6-20.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "UIViewExt.h"
#import "MJPhoto.h"
#import "MJPhotoBrowser.h"
#import "LGDRoadCell.h"
#import "LGUtilDefine.h"
#import "SJAvatarBrowser.h"

@implementation LGDRoadCell

@synthesize delegete = _delegate;

- (id)init
{
    self = [super init];
    if (self) {
        _tipIView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"place_Item_total.png"]];
        [_tipIView setCenter:CGPointMake(40, 15)];
        [self addSubview:_tipIView];
        _tLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 3, 320, 21)];
        [_tLabel setBackgroundColor:[UIColor clearColor]];
        [_tLabel setShadowColor:[UIColor blackColor]];
        [_tLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
        [_tLabel setShadowOffset:CGSizeMake(0.05, 0.05)];
        [_tLabel setTextColor:[UIColor blackColor]];
        [_tLabel setTextAlignment:NSTextAlignmentLeft];
        [self addSubview:_tLabel];
        
        _itemIView = [[EMAsyncImageView alloc] initWithFrame:CGRectMake(2, 25, 316, 200) cornerRadius:0];
        _itemIView.userInteractionEnabled = YES;
        [self addSubview:_itemIView];
        UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageClieck:)];
        [_itemIView addGestureRecognizer:tgr];
        
        _cLabel=[[UILabel alloc]initWithFrame:CGRectMake(2, 225, 316, 60)];
        _cLabel.numberOfLines=0;
        _cLabel.lineBreakMode=NSLineBreakByCharWrapping;
        [_cLabel setBackgroundColor:[UIColor lightGrayColor]];
        [_cLabel setFont:[UIFont systemFontOfSize:16]];
        [_cLabel setTextColor:[UIColor blackColor]];
        [_cLabel setTextAlignment:NSTextAlignmentLeft];
        [self addSubview:_cLabel];
        
    }
    return self;
}
-(void)reloadData:(LGRoadDetailInfo *)rdInfo
{
    _rdInfo = rdInfo;
    CGSize size = [rdInfo.content sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(316, 20000)];
    [_tLabel setText:rdInfo.signText];
    [_cLabel setText:rdInfo.content];
    if ([rdInfo.uuidPlace compare:@""]) {
        //可点击
        [_tLabel setTextColor:[UIColor redColor]];
        _tLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(titleEvent:)];
        [_tLabel addGestureRecognizer:tgr];
    }else{
        [_tLabel setTextColor:[UIColor blackColor]];
    }
    if ([rdInfo.signImg compare:@""] == 0)
    {
        _cLabel.frame=CGRectMake(2, 25, 316, size.height);
        _itemIView.image = nil;
    }else{
        _cLabel.frame=CGRectMake(2, 225, 316, size.height);
        _itemIView.imageSize = 3;
        _itemIView.imageUrl = [NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,rdInfo.signImg];
    }
}
- (CGFloat)heightContentBackgroundView:(NSString *)content b:(BOOL)b
{
    CGSize size = [content sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(316, 20000)];
    CGFloat height = size.height;
    if (b){
        height += 200;
    }
    height += 25;
    
    return height;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}
-(void)titleEvent:(id)sender{
    if (_delegate) {
        [_delegate itemClick:_rdInfo.uuidPlace];
    }
}
-(void)imageClieck:(id)sender
{
    NSMutableArray *photos = [NSMutableArray arrayWithCapacity: 2 ];
//    for (int i =0; i<2; i++) {
        MJPhoto *photo = [[MJPhoto alloc] init];
        photo.srcImageView = _itemIView;
        photo.url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,_rdInfo.signImg] ]; // 图片路径
        [photos addObject:photo];
//    }
    
    // 2.显示相册
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.currentPhotoIndex = 0; // 弹出相册时显示的第一张图片是？
    browser.photos = photos; // 设置所有的图片
    [browser show];

//    [SJAvatarBrowser showImage:_itemIView];
}
@end
