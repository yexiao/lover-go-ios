//
//  LGDetailRoadView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMAsyncImageView.h"
#import "LGCityCodeInfo.h"

@class LGDetailRoadView;

@protocol LGDetailRoadViewDelegate <NSObject>

-(void)turnBackLeft;
-(void)turnBackRight;
@end

@interface LGDetailRoadView : UIView
{
    EMAsyncImageView* _titleIView;
    UILabel *_titleLab;
    UIScrollView* _cScrollView;
    UITableView* _tView;
    id<LGDetailRoadViewDelegate> _delegate;
}
@property(nonatomic,strong)UITableView* tView;
@property(nonatomic,strong)id<LGDetailRoadViewDelegate> delegate;

- (id)initWithFrame:(CGRect)frame info:(LGCityCodeInfo*)info;
- (id)initWithFrame:(CGRect)frame info:(LGCityCodeInfo*)info haveR:(BOOL)b;

@end
