//
//  LGRoadItemView.m
//  LoverGo
//
//  Created by YeXiao on 14-5-25.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGRoadItemView.h"
#import "LGUtilDefine.h"
#import "LGDataCenter.h"
#import "LGUtil.h"
#import "MJPhoto.h"

@implementation LGRoadItemView
-(void)setDefauleLabel:(UILabel*)lab
{
    [lab setBackgroundColor:[UIColor clearColor]];
    [lab setShadowColor:[UIColor grayColor]];
    [lab setFont:[UIFont fontWithName:@"Helvetica" size:22]];
    [lab setShadowOffset:CGSizeMake(0.05, 0.05)];
    [lab setTextColor:[UIColor whiteColor]];
    [lab setTextAlignment:NSTextAlignmentLeft];
}

- (id)init
{
    self = [super init];
    if (self) {
        _iconIView = [[EMAsyncImageView alloc] initWithFrame:CGRectMake(2, 2, 316, 196) cornerRadius:5];//701,466
        [self addSubview:_iconIView];
        _iconIView.userInteractionEnabled = YES;
        _userLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 1, 160, 25)];
        [self setDefauleLabel:_userLab];
        [self addSubview:_userLab];
        _timeLab = [[UILabel alloc] initWithFrame:CGRectMake(161, 1, 95, 25)];
        [self setDefauleLabel:_timeLab];
        [_timeLab setTextAlignment:NSTextAlignmentRight];
        [self addSubview:_timeLab];
        _costLab= [[UILabel alloc] initWithFrame:CGRectMake(261, 1, 60, 25)];
        [self setDefauleLabel:_costLab];
        [self addSubview:_costLab];
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 175, 319, 25)];
        [self setDefauleLabel:_titleLab];
        [_titleLab setShadowOffset:CGSizeMake(0.5, 0.5)];
        [self addSubview:_titleLab];
    }
    return self;
}
-(void)reloadRoadData:(LGCityCodeInfo*)info
{
    [_userLab setText:info.userName];
    [_titleLab setText:info.routeName];
    [_timeLab setText:info.goTime];
    [_costLab setText:[NSString stringWithFormat:@"%@元",info.totalPrice]];
    if ([info.headImg compare:@""] != 0) {
        _iconIView.imageUrl = [NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,info.headImg];
    }else{
        [_iconIView setImage:[LGUtil getUnFoundImage]];
    }
    
    MJPhoto *photo = [[MJPhoto alloc] init];
    photo.srcImageView = _iconIView;
    photo.url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,info.headImg] ]; // 图片路径
    
    [self bringSubviewToFront:_titleLab];
}
-(void)removeAll
{
    [_iconIView setImage:NULL];
    [_userLab setText:NULL];
    [_titleLab setText:NULL];
    [_timeLab setText:NULL];
}

@end
