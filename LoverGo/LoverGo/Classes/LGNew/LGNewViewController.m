//
//  LGNewViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGNewViewController.h"

@interface LGNewViewController ()

@end

@implementation LGNewViewController

-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype
{
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:img tag:mvtype];
    }
    return self;
}

-(void)loadView
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    _newView = [[LGNewView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _newView.slideSwitchView.slideSwitchViewDelegate = self;
    [self setView:_newView];
    
    
    _vc1 = [[LGRoadViewController alloc] init];
    _vc1.title = @"线路";
    _vc2 =[[LGAticleViewController alloc] init];
    _vc2.title = @"推荐";
    _vc3 = [[LGAtiviteViewController alloc] init];
    _vc3.title = @"活动";
    
    
    [_newView.slideSwitchView buildUI];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}
#pragma mark - 滑动tab视图代理方法


- (NSUInteger)numberOfTab:(QCSlideSwitchView *)view
{
    return 3;
}

- (UIViewController *)slideSwitchView:(QCSlideSwitchView *)view viewOfTab:(NSUInteger)number
{
    switch (number) {
        case 0:
            return _vc1;
            break;
        case 1:
            return _vc2;
            break;
        case 2:
            return _vc3;
            break;
        default:
            break;
    }
    return NULL;
}
- (void)slideSwitchView:(QCSlideSwitchView *)view didselectTab:(NSUInteger)number
{
    UIViewController *vController;
    switch (number) {
        case 0:
            [_vc1 loadFirst];
            vController = _vc1;
            break;
        case 1:
            [_vc2 loadFirst];
            vController = _vc2;
            break;
        case 2:
            [_vc3 loadFirst];
            vController = _vc3;
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
