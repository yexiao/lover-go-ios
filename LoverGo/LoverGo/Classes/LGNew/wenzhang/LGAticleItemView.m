//
//  LGAticleItemView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtilDefine.h"
#import "LGUtil.h"
#import "LGAticleItemView.h"

@implementation LGAticleItemView
- (id)init
{
    self = [super init];
    if (self) {
        _iconIView = [[EMAsyncImageView alloc] initWithFrame:CGRectMake(2, 2, 154, 98) cornerRadius:0];
        [self addSubview:_iconIView];
        _valueView = [[UIView alloc] initWithFrame:CGRectMake(160, 2, 160, 98)];
        [self addSubview:_valueView];
        
        _titleLab= [[UILabel alloc] initWithFrame:CGRectMake(1, 1, 160, 50)];
        [_titleLab setBackgroundColor:[UIColor clearColor]];
        [_titleLab setShadowColor:[UIColor grayColor]];
        [_titleLab setShadowOffset:CGSizeMake(0.05, 0.05)];
        [_titleLab setTextColor:[UIColor blackColor]];
        [_titleLab setTextAlignment:NSTextAlignmentLeft];
        [_valueView addSubview:_titleLab];
        _tapLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 50, 160, 50)];
        [_tapLab setBackgroundColor:[UIColor clearColor]];
        [_tapLab setShadowColor:[UIColor grayColor]];
        [_tapLab setShadowOffset:CGSizeMake(0.05, 0.05)];
        [_tapLab setTextColor:[UIColor blackColor]];
        [_tapLab setTextAlignment:NSTextAlignmentLeft];
        [_valueView addSubview:_tapLab];
    }
    return self;
}
-(void)reloadArticleData:(LGArticleInfo*)info imgIsLeft:(BOOL)b
{
    if ([info.indexImage compare:@""] != 0) {
        _iconIView.userInteractionEnabled = YES;
        _iconIView.imageUrl = [NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,info.indexImage];
    }else
    {
        [_iconIView setImage:[LGUtil getUnFoundImage]];
    }
    
    if (b) {
        [_iconIView setCenter:CGPointMake(81, 51)];
        [_valueView setCenter:CGPointMake(160+80, 51)];
    }else
    {
        [_iconIView setCenter:CGPointMake(160+80, 51)];
        [_valueView setCenter:CGPointMake(81, 51)];
    }
    
    _titleLab.lineBreakMode = UILineBreakModeWordWrap;
    _titleLab.numberOfLines = 0;
    NSString* cStr =info.articleTitle;
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:15];
    [_titleLab setFont:font];
    [_titleLab setText:cStr];
    CGSize labelsize = [cStr sizeWithFont:font constrainedToSize:CGSizeMake(160, 2000) lineBreakMode:NSLineBreakByCharWrapping];
    [_titleLab setFrame:CGRectMake(1, 1, labelsize.width, labelsize.height)];
    
    _tapLab.lineBreakMode = UILineBreakModeWordWrap;
    _tapLab.numberOfLines = 0;
    NSString* aStr =info.articleIntro;
    UIFont *font1 = [UIFont fontWithName:@"Helvetica" size:12];
    [_tapLab setFont:font1];
    [_tapLab setText:aStr];
    CGSize labelsize1 = [cStr sizeWithFont:font constrainedToSize:CGSizeMake(160, 2000) lineBreakMode:NSLineBreakByCharWrapping];
    [_tapLab setFrame:CGRectMake(1, 50, labelsize1.width, labelsize1.height)];
    
}
-(void)removeAll
{
    _iconIView.image = NULL;
    [_tapLab setText:NULL];
    [_titleLab setText:NULL];
}

@end
