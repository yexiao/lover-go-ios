//
//  LGAticleViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "MJExtension.h"
#import "Toast+UIView.h"
#import "LGArticleInfo.h"
#import "LGAticleItemView.h"
#import "LGRequestManager.h"
#import "LGAticleViewController.h"
#import "LGDetailAticleViewViewController.h"

@interface LGAticleViewController ()

@end

@implementation LGAticleViewController

-(void)loadView
{
    _aticleArray = [[NSMutableArray alloc] init];
    _aticleView = [[LGAticleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - TOOLBAR - (40+44))];
    _aticleView.tableView.dataSource = self;
    _aticleView.tableView.delegate = self;
    [self setView:_aticleView];
    _utilModel = nil;
}
-(void)loadFirst
{__block LGAticleViewController *_blockSelf = self;
    _aticleView.header.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //发送初始请求
        [_blockSelf sendRequestByAticlePage:1];
    };
    [_aticleView.header beginRefreshing];
    
    _aticleView.footer.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //进入刷新状态
        if (_blockSelf->_utilModel) {
            int nextPage = [_blockSelf->_utilModel.nowPage intValue] +1;
            if (nextPage > [_blockSelf->_utilModel.totalPage intValue]) {
                [_blockSelf->_aticleView.footer endRefreshing];
                [_blockSelf.view makeToast:@"没有下一页了"];
                return ;
            }
            [_blockSelf sendRequestByAticlePage:nextPage];
        }
    };
}
-(void)sendRequestByAticlePage:(int)page
{
    __block LGAticleViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          @"art001",@"catCode",
                          @"tj",@"city",
                          [NSString stringWithFormat:@"%d",page],@"page",
                          nil];
    [[SVHTTPClient sharedClient] GET:getArticleByMapSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  [_blockSelf.view makeToast:@"网络连接"];
                                  return ;
                              }
                              [_blockSelf refreshView:response aticlePage:page];
                          }];
}
-(void)refreshView:(id)response aticlePage:(int)page
{
    [_aticleView.header endRefreshing];
    [_aticleView.footer endRefreshing];
    if (response == nil) {
        [self.view makeToast:@"没有数据"];
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        [self.view makeToast:@"数据解析失败"];
        return;
    }else{
        if (page == 1) {
            [_aticleArray removeAllObjects];
        }
        _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        [_aticleArray addObjectsFromArray:[LGArticleInfo objectArrayWithKeyValuesArray:_utilModel.listData]];
        [_aticleView.tableView reloadData];//刷新数据
        
    }
}
#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGArticleInfo *ccInfo= [_aticleArray objectAtIndex:indexPath.row];
    
    LGDetailAticleViewViewController *vc = [[LGDetailAticleViewViewController alloc] initWithActivityInfo:ccInfo];
    //获取view的controller
    for (UIView* next = [self.view superview]; next; next = next.superview){
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            UIViewController *mV = (UIViewController *)nextResponder;
            [mV.navigationController pushViewController:vc animated:YES];
            break;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _aticleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGAticleItemView * cell = [[LGAticleItemView alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    LGArticleInfo *aInfo= [_aticleArray objectAtIndex:indexPath.row];
    [cell reloadArticleData:aInfo  imgIsLeft:indexPath.row%2];
    return cell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
