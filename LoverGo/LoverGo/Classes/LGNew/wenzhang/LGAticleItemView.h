//
//  LGAticleItemView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGArticleInfo.h"
#import "EMAsyncImageView.h"

@interface LGAticleItemView : UITableViewCell
{
    EMAsyncImageView* _iconIView;
    UIView* _valueView;
    UILabel *_titleLab;
    UILabel *_tapLab;
}
-(void)reloadArticleData:(LGArticleInfo*)info imgIsLeft:(BOOL)b;

-(void)removeAll;
@end
