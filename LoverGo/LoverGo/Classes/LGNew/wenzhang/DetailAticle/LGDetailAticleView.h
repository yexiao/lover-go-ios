//
//  LGDetailAticleView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-2.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGDetailAticleViewDelegate <NSObject>

-(void)turnBackLeft;
-(void)turnRight;

@end

@interface LGDetailAticleView : UIView
{
    UIWebView *_webView;
    UIActivityIndicatorView *_aIView;
    id<LGDetailAticleViewDelegate> _delegate;
}

@property(nonatomic,strong)UIActivityIndicatorView *aIView;
@property(nonatomic,strong)UIWebView* webView;
@property(nonatomic,strong)id<LGDetailAticleViewDelegate> delegate;

-(id)initWithFrame:(CGRect)frame haveR:(BOOL)b;

-(void)loadDetailAticleByURL:(NSString*)url;

@end
