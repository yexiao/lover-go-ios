//
//  LGDetailAticleViewViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-2.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LXActionSheet.h"
#import "LGArticleInfo.h"
#import "EMAsyncImageView.h"
#import "LGDetailAticleView.h"

@interface LGDetailAticleViewViewController : UIViewController<UIWebViewDelegate,LGDetailAticleViewDelegate,LXActionSheetDelegate>
{
    BOOL _haveRight;
    LGDetailAticleView *_aView;
    LGArticleInfo* _info;
    EMAsyncImageView* _shareIView;
}
-(id)initWithActivityInfo:(LGArticleInfo*)info;
-(id)initWithActivityInfo:(LGArticleInfo*)info haveR:(BOOL)b;
@end
