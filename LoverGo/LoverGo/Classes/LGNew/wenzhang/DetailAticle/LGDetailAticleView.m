//
//  LGDetailAticleView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-2.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGDetailAticleView.h"

@implementation LGDetailAticleView

@synthesize webView = _webView;
@synthesize aIView  = _aIView;
@synthesize delegate = _delegate;

-(id)initWithFrame:(CGRect)frame haveR:(BOOL)b
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor grayColor];
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"精品文章"];
        titleLab.userInteractionEnabled = true;
        [self addSubview:titleLab];
        
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, STATUSBAR +40, frame.size.width, frame.size.height)];
        _webView.scalesPageToFit =YES;
        [self addSubview:_webView];
        
        _aIView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
        [_aIView setCenter:self.center];
        [_aIView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite] ;
        [self addSubview:_aIView];
        
        
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:1];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        if (b) {
            //分享、参看商家
            UIButton *rightBtn = [LGUtil returnRightBtn];
            [rightBtn setTag:2];
            [rightBtn setCenter:CGPointMake(290, 20)];
            [rightBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
            [titleLab addSubview:rightBtn];
        }
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame haveR:YES];
}

-(void)btnClieck:(UIButton*)sender
{
    if (sender.tag == 2) {
        [_delegate turnRight];
    }else
    {
        [_delegate turnBackLeft];
    }
}
-(void)loadDetailAticleByURL:(NSString*)url
{
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];//创建NSURLRequest
    [_webView loadRequest:request];
}
@end
