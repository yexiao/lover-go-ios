//
//  LGDetailAticleViewViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-2.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGDataCenter.h"
#import "SVHTTPClient.h"
#import "LGUtilDefine.h"
#import "Toast+UIView.h"
#import "LGDetailAticleViewViewController.h"

@interface LGDetailAticleViewViewController ()

@end

@implementation LGDetailAticleViewViewController

-(id)initWithActivityInfo:(LGArticleInfo*)info
{
    return [self initWithActivityInfo:info haveR:YES];
}

-(id)initWithActivityInfo:(LGArticleInfo*)info haveR:(BOOL)b
{
    self = [super init];
    if (self) {
        _info = info;
        _shareIView = nil;
        _haveRight = b;
    }
    return self;
}
-(void)loadView
{
    _aView = [[LGDetailAticleView alloc] initWithFrame:CGRectMake(0, STATUSBAR +TOOLBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR +TOOLBAR)) haveR:_haveRight];
    _aView.delegate = self;
    _aView.webView.delegate = self;
    [self setView:_aView];
    [_aView loadDetailAticleByURL:[NSString stringWithFormat:@"%@lover-go/%@",baseUrl,_info.htmlName]];
    _shareIView = [[EMAsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)cornerRadius:0];
    _shareIView.userInteractionEnabled = true;
    if ([_info.indexImage compare:@""] == 0) {
        [_shareIView setImage:[LGUtil getUnFoundImage]];
    }else
    {
        _shareIView.imageUrl = [NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,_info.indexImage];
    }
}
-(void)turnBackLeft
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)turnRight
{
    LXActionSheet* asheet = [[LXActionSheet alloc]initWithTitle:@"分享或者来看看吧！" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"收藏" otherButtonTitles:@[@"分享",@"参看商家"]];
    [asheet showInView:self.view];
}
#pragma mark - LXActionSheetDelegate

- (void)didClickOnButtonIndex:(NSInteger *)buttonIndex
{
    int tag = (int)buttonIndex;
    if ( tag== 1) {
        //分享
        UIImage *shareImg = nil;
        if (_shareIView) {
            shareImg = _shareIView.image;
        }
        [LGUtil shareToSNSURL:[NSString stringWithFormat:@"%@lover-go/%@",baseUrl,_info.htmlName] title:@"情侣去哪儿" img:shareImg content:_info.articleTitle
                   controller:self];
    }
    if (tag == 2) {
        //参看商家
        [self.view makeToast:@"暂无关联商家"];
    }
}

- (void)didClickOnDestructiveButton
{
    //收藏按钮
    LGUserInfo* info =[LGDataCenter getSingleton].userInfo;
    if (info == NULL) {
        [self.view makeToast:@"请先登录才能收藏哦!"];
        return;
    }
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          info.uuid,@"userUuid",
                          _info.uuid,@"objectUuid",
                          @"article",@"collectionType",
                          nil];
    [[SVHTTPClient sharedClient] GET:addCollectionByUserSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  [self.view makeToast:@"请求失败!"];
                                  return ;
                              }
                              [self.view makeToast:@"收藏成功!"];
                          }];
}

- (void)didClickOnCancelButton
{
    NSLog(@"cancelButton donothing");
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_aView.aIView startAnimating] ;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_aView.aIView stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.view makeToast:@"网页加载失败!"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
