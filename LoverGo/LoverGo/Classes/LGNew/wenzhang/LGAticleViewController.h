//
//  LGAticleViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGAticleView.h"
#import "LGUtilModel.h"

@interface LGAticleViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    LGAticleView *_aticleView;
    NSMutableArray* _aticleArray;
    LGUtilModel *_utilModel;
}
-(void)loadFirst;
-(void)refreshView:(id)response aticlePage:(int)page;
-(void)sendRequestByAticlePage:(int)page;
@end
