//
//  LGNewView.m
//  LoverGo
//
//  Created by YeXiao on 14-5-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGNewView.h"

@implementation LGNewView

@synthesize slideSwitchView = _slideSwitchView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"资讯"];
        [titleLab setBackgroundColor:[UIColor orangeColor]];
        [self addSubview:titleLab];
        
        _slideSwitchView = [[QCSlideSwitchView alloc] initWithFrame:CGRectMake(0, STATUSBAR+40, SCREEN_WIDTH, SCREEN_HEIGHT - 40 - TOOLBAR) size:50.0f];
        _slideSwitchView.tabItemNormalColor = [QCSlideSwitchView colorFromHexRGB:@"000000"];
        _slideSwitchView.tabItemSelectedColor = [QCSlideSwitchView colorFromHexRGB:@"ff0000"];
        _slideSwitchView.shadowImage = [[UIImage imageNamed:@"red_line_and_shadow.png"]
                                        stretchableImageWithLeftCapWidth:59.0f topCapHeight:0.0f];
        [self addSubview:_slideSwitchView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
