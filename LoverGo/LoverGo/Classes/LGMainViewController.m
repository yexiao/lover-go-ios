//
//  LGMainViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-1-10.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGMainViewController.h"
#import "LGPlaceViewController.h"
#import "LGShareViewController.h"
#import "LGInfoViewController.h"
#import "LGLineViewController.h"
#import "LGMeViewController.h"
#import "MMLocationManager.h"
#import "LGUserInfo.h"
#import "LGRequestManager.h"
#import "LGUtilDefine.h"
#import "LGDataCenter.h"
#import "LGNewViewController.h"
#import "LGNavigationController.h"
#import "LGCollectionViewController.h"

@interface LGMainViewController ()

@end

@implementation LGMainViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSucc:) name:loginSPath object:nil];
    //初始化城市
    NSUserDefaults *standard = [NSUserDefaults standardUserDefaults];
    if ([standard objectForKey:MMLastCity] == NULL) {
        [standard setObject:@"39.92" forKey:MMLastLongitude];
        [standard setObject:@"116.46" forKey:MMLastLatitude];
        [standard setObject:@"北京" forKey:MMLastCity];
        [standard setObject:@"北京市" forKey:MMLastAddress];
    }
    NSString *uEmail = [standard objectForKey:@"UserEmail"];
    NSString *uPwd = [standard objectForKey:@"UserPwd"];
    if ( uEmail!= NULL && uPwd != NULL) {
        NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                              uEmail,@"appUser.email",
                              uPwd,@"appUser.password",
                              nil];
        
        [[LGRequestManager getSingleton] sendRequestAboutUser:loginSPath para:para];
    }
    LGNewViewController *infoVC = [[LGNewViewController alloc] initWithTitle:@"资讯" image:[UIImage imageNamed:@"maintab_search_selected.png"] type:MainViewTypeInfo];
    
//    LGPlaceViewController *pVC = [[LGPlaceViewController alloc] initWithTitle:@"地点" image:[UIImage imageNamed:@"maintab_collect_selected.png"] type:MainViewTypePlace];
    
    LGCollectionViewController *lVC = [[LGCollectionViewController alloc] initWithTitle:@"收藏" image:[UIImage imageNamed:@"maintab_home_selected.png"] type:MainViewTypeLine];
//    LGLineViewController *lVC = [[LGLineViewController alloc] initWithTitle:@"收藏" image:[UIImage imageNamed:@"tab_friends.png"] type:MainViewTypeLine];
    
//    LGShareViewController *sVC =[[LGShareViewController alloc] initWithTitle:@"分享" image:[UIImage imageNamed:@"tab_heart.png"] type:MainViewTypeShare];
    
    
    LGMeViewController *mVC = [[LGMeViewController alloc] initWithTitle:@"个人" image:[UIImage imageNamed:@"maintab_setting_selected.png"] type:MainViewTypeMe];
    
    self.viewControllers = [NSArray arrayWithObjects:infoVC/*,pVC*/,lVC,/*sVC,*/mVC,nil];
}

-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:loginSPath name:nil object:self];
}
-(void)loginSucc:(NSNotification *)nof
{
    if ([[nof name] compare:loginSPath] == 0) {
        NSLog(@"打开客户端时候自动登录成功");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}
@end
