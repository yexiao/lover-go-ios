//
//  ELHeaderView.h
//  NetEasyLikeNavigation
//
//  Created by ZhouQuan on 14-1-13.
//  Copyright (c) 2014年 iOSTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMAsyncImageView.h"

@interface ELHeaderView : UIView{
    
}

@property (nonatomic, strong) EMAsyncImageView *backImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *subTitleLabel;
@property (nonatomic, assign) CGPoint prePoint;
@property (nonatomic, strong) UIViewController *viewController;
@property (nonatomic, strong) UIScrollView *scrollView;
/**
 *  initialization
 *
 *  @param frame          frame
 *  @param backImageURL   Background Image‘s URL
 *  @param headerImageURL the Image in the Center
 *  @param title          The Header Tiltle
 *  @param subTitle       The Subtitle
 *
 *  @return A Animated Header View
 */
- (id)initWithFrame:(CGRect)frame title:(NSString *)title subTitle:(NSString *)subTitle;
-(void)updateSubViewsWithScrollOffset:(CGPoint)newOffset;
-(void)updateImgURL:(NSString*)bURL hURL:(NSString*)hUrl;
@end
