//
//  LGUtil.m
//  LoverGo
//
//  Created by YeXiao on 14-4-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "MJPhoto.h"
#import "MJPhotoBrowser.h"
#import "LGActivityImageInfo.h"
#import "LGUtilDefine.h"
#import "UMSocialShakeService.h"
#import "UMSocialScreenShoter.h"

@implementation LGUtil

+(UIButton*)get9ScalBtn:(CGRect)rect type:(TypeColor)type
{
    UIEdgeInsets insets1 = UIEdgeInsetsMake(10, 10, 9, 21);
    
    UIImage *s_Img = nil;
    UIImage *s_HImg = nil;
    switch (type) {
        case TypeColorOrange:
        {
            s_Img = [[UIImage imageNamed:@"setting_login.png"] resizableImageWithCapInsets:insets1];
            s_HImg = [[UIImage imageNamed:@"setting_login_h.png"] resizableImageWithCapInsets:insets1];
            break;
        }
        case TypeColorGreen:
        {
            s_Img = [[UIImage imageNamed:@"setting_regrister.png"] resizableImageWithCapInsets:insets1];
            s_HImg = [[UIImage imageNamed:@"setting_regrister_h.png"] resizableImageWithCapInsets:insets1];
            
            break;
        }
        default:
            break;
    }
    
    UIButton *btn = [[UIButton alloc] initWithFrame:rect];
    [btn setBackgroundImage:s_Img forState:UIControlStateNormal];
    [btn setBackgroundImage:s_HImg forState:UIControlStateHighlighted];
    return btn;
}
+(UIImage*)get9ScalImg:(TypeImg)type
{
    UIImage *img = nil;
    switch (type) {
        case TypeImgBG:
            img = [[UIImage imageNamed:@"setting_lab_bg.png"] resizableImageWithCapInsets: UIEdgeInsetsMake(13, 3, 13, 3)];
            break;
            
        default:
            break;
    }
    return img;
}
+ (UIImage *)getUnFoundImage
{
    return [UIImage imageNamed:@"img_default.png"];
}

+ (UIButton *)getBackBtn
{
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 72/2., 77/2.)];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"info_city_close_btn_n.png"] forState:UIControlStateNormal];
    return backBtn;
}
+(UIButton*)returnLeftBtn
{
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 110/2., 60/2.)];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"returnLeft.png"] forState:UIControlStateNormal];
    return backBtn;
}

+ (UIButton *)returnRightBtn
{
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44/2., 46/2.)];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"more_button_down.png"] forState:UIControlStateNormal];
    return backBtn;
}
+(UILabel*)getTitleLab:(CGRect)rect title:(NSString *)str
{
    UILabel *titleLab = [[UILabel alloc] initWithFrame:rect];
    [titleLab setBackgroundColor:[UIColor colorWithRed:240/255. green:100/255. blue:10/255. alpha:255]];
    [titleLab setShadowColor:[UIColor grayColor]];
    [titleLab setFont:[UIFont fontWithName:@"Helvetica" size:22]];
    [titleLab setShadowOffset:CGSizeMake(0.5, 0.5)];
    [titleLab setTextColor:[UIColor whiteColor]];
    [titleLab setText:str];
    [titleLab setTextAlignment:NSTextAlignmentCenter];
    return titleLab;
}
//判断number是否符合要求
+(BOOL)judgeNumber:(NSString *)_number{
    NSString *phoneCheck=@"^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
    NSPredicate *phonelTest=[NSPredicate predicateWithFormat:@"SELF MATCHES%@",phoneCheck];
    return [phonelTest evaluateWithObject:_number];
    
}
+(UIViewController*)getControllerByView:(UIView *)view
{
    //获取view的controller
    for (UIView* next = [view superview]; next; next = next.superview){
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            UIViewController *mV = (UIViewController *)nextResponder;
            return mV;
//            [mV.navigationController pushViewController:vc animated:YES];
//            break;
        }
    }
    return NULL;
}
+(void)cleanCache
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    
    NSFileManager *fileManage = [NSFileManager defaultManager];
    //方法一
    NSArray *files = [fileManage subpathsOfDirectoryAtPath: cachePath error:nil];
    for (int i = 0; i<files.count; i++) {
        //更改到待操作的目录下
        [fileManage changeCurrentDirectoryPath:[cachePath stringByExpandingTildeInPath]];
        NSString* fileName = [files objectAtIndex:i];
        BOOL bRet = [fileManage fileExistsAtPath:fileName];
        if (bRet) {
            //
            NSError *err;
            [fileManage removeItemAtPath:fileName error:&err];
        }
    }
}
+ (void)shareToSNSURL:(NSString*)url title:(NSString*)title img:(UIImage*)img content:(NSString*)content controller:(UIViewController*)c
{
    //分享
    //UMSocialWXMessageTypeImage 为纯图片类型
    [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
    //分享图文样式到微信朋友圈显示字数比较少，只显示分享标题
    [UMSocialData defaultData].extConfig.title = title;
    //设置微信好友或者朋友圈的分享url,下面是微信好友，微信朋友圈对应wechatTimelineData
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = url;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = url;
    
    [UMSocialSnsService presentSnsIconSheetView:c
                                         appKey:UmengAppkey
                                      shareText:content
                                     shareImage:img
                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToRenren,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToWechatFavorite,nil]
                                       delegate:nil];
}
+(void)showImages:(NSArray*)array
{
    NSMutableArray *photos = [NSMutableArray arrayWithCapacity: 2 ];
    for (int i =0; i<array.count; i++) {
        LGActivityImageInfo* info = [array objectAtIndex:i];
        MJPhoto *photo = [[MJPhoto alloc] init];
        photo.srcImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 250)];
        photo.url = [NSURL URLWithString: [NSString stringWithFormat:@"%@lover-go/admin/%@",baseUrl,info.imageUrl] ]; // 图片路径
        [photos addObject:photo];
    }
    
    // 2.显示相册
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.currentPhotoIndex = 0; // 弹出相册时显示的第一张图片是？
    browser.photos = photos; // 设置所有的图片
    [browser show];
}
@end
