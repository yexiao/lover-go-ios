//
//  LGUtil.h
//  LoverGo
//
//  Created by YeXiao on 14-4-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    TypeColorOrange = 0,
    TypeColorGreen,
}TypeColor;
typedef enum{
    TypeImgBG = 0,
}TypeImg;

@interface LGUtil : NSObject

+ (UIButton *)get9ScalBtn:(CGRect )rect type:(TypeColor)type;
+ (UIImage *)get9ScalImg:(TypeImg)type;
+ (UIButton *)getBackBtn;
+ (UILabel *)getTitleLab:(CGRect )rect title:(NSString*)str;
+ (UIButton *)returnLeftBtn;
+ (UIButton *)returnRightBtn;
+ (UIImage *)getUnFoundImage;
+(UIViewController*)getControllerByView:(UIView*)view;
+(void)cleanCache;
+(BOOL)judgeNumber:(NSString *)_number;
+ (void)shareToSNSURL:(NSString*)url title:(NSString*)title img:(UIImage*)img content:(NSString*)content controller:(UIViewController*)c;
+(void)showImages:(NSArray*)array;
@end
