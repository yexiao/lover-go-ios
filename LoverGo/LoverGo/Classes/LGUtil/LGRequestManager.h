//
//  LGRequestManager.h
//  LoverGo
//
//  Created by YeXiao on 14-2-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LGUtilDefine.h"
#import "SVHTTPClient.h"

typedef enum {
    RequestTypeLogin = 0,//登录
    RequestTypeRegister,
}RequestType;

@interface LGRequestManager : NSObject
{
    NSUserDefaults *standard;
    NSTimer  *showTimer;
    SVHTTPClient *svhttpClient;
}
+ (LGRequestManager *)getSingleton;
-(void)stopLoading;
-(void)sendRequest:(NSString*)sPath para:(NSDictionary*)p;
-(void)sendRequestAboutUser:(NSString*)sPath para:(NSDictionary*)p;//相关用户通知：登录、更改资料、退出

@end
