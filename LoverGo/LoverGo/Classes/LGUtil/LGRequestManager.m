//
//  LGRequestManager.m
//  LoverGo
//
//  Created by YeXiao on 14-2-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUserInfo.h"
#import "MJExtension.h"
#import "LGDataCenter.h"
#import "LGRequestManager.h"


LGRequestManager *singletonM = nil;

@implementation LGRequestManager


+ (LGRequestManager *)getSingleton {
    if(!singletonM) {
        singletonM = [[LGRequestManager alloc] init];
        singletonM->svhttpClient = [SVHTTPClient sharedClient];
        [singletonM->svhttpClient setBasePath:baseUrl];
        singletonM->standard = [NSUserDefaults standardUserDefaults];
        singletonM->showTimer = nil;
    }
    return singletonM;
}


-(void)stopLoading
{
    if (showTimer) {
        [showTimer invalidate];
        showTimer = nil;
    }
}
-(void)sendRequest:(NSString*)sPath para:(NSDictionary*)p
{
    [[SVHTTPClient sharedClient] GET:sPath
                          parameters:p
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              //                              NSString *str = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
                              NSError *jsonError = nil;
                              id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
                              
                              [[NSNotificationCenter defaultCenter]postNotificationName:sPath object:jsonObject];
                          }];
}
-(void)sendRequestAboutUser:(NSString*)sPath para:(NSDictionary*)p
{
    [[SVHTTPClient sharedClient] POST:sPath
                          parameters:p
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  NSLog(@"报错啦!");
                                  return ;
                              }
                              NSError *jsonError = nil;
                              id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
                              if (jsonError) {
                                  NSLog(@"解析报错啦!");
                              }else{
                                  NSArray* arr= [LGUserInfo objectArrayWithKeyValuesArray:[jsonObject objectForKey:@"list"]];
                                  if (arr && arr.count > 0) {
                                      LGUserInfo *uInfo = [arr objectAtIndex:0];
                                      if (uInfo) {
                                          [[LGDataCenter getSingleton] setUserInfo:uInfo];
                                      }
                                      [[NSNotificationCenter defaultCenter]postNotificationName:sPath object:jsonObject];
                                  }else
                                  {
                                      [[NSNotificationCenter defaultCenter]postNotificationName:ErrorNotification object:jsonObject];
                                  }
                              }
                          }];
}
@end
