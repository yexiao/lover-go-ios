//
//  LGDataCenter.h
//  LoverGo
//
//  Created by YeXiao on 14-2-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUserInfo.h"
#import "MJExtension.h"
#import <Foundation/Foundation.h>

@interface LGDataCenter : NSObject
{
    
}
@property(nonatomic,strong)LGUserInfo *userInfo;
@property(nonatomic,strong)NSMutableDictionary* cachedPics;


+ (LGDataCenter *)getSingleton;

-(void)signOut;
-(NSString*)getPlatform;

@end
