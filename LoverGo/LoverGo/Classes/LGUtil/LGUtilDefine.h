//
//  ASICloudFilesCDNRequest_LGUtilDef.h
//  LoverGo
//
//  Created by YeXiao on 14-4-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#define UmengAppkey                             @"537eba8856240b9bad06ad50"

#define LGMainObj                               @"LGMainObj"//解析最主要的object

#define baseUrl                                 @"http://lover-go.com:8588/"
#define baseUploadImages                        @"lover-go/admin/UploadImages/"
#define loginSPath                              @"lover-go/appUser/login"
#define userUpDateSPath                         @"lover-go/appUser/update"
#define registerSPath                           @"lover-go/appUser/register"
#define addFriendSPath                          @"lover-go/appUser/addFriend"
#define deleteFriendSPath                       @"lover-go/appUser/addFriend"
#define getPlaceByMapSPath                      @"lover-go/place/getPlaceByMap"
#define findListByCityCodeSPath                 @"lover-go/route/findListByCityCode"
#define findRouteByUuidSPath                    @"lover-go/route/findRouteByUuid"
#define getArticleByMapSPath                    @"lover-go/article/getArticleByMap"
#define findActivityByUuidSPath                 @"lover-go/phoneActivity/findActivityByUuid"
#define findActivityByMapSPath                  @"lover-go/phoneActivity/findActivityByMap"
#define getPlaceCommentByUuid                   @"lover-go/placeComment/getPlaceCommentByUuid"
#define getPlaceByUuid                          @"lover-go/place/getPlaceByUuid"
#define getArticleByUuid                        @"lover-go/article/getArticleByUuid"
#define getListByCityCode                       @"lover-go/route/getListByCityCode"
#define getRouteByUuid                          @"lover-go/route/getRouteByUuid"
#define addActivityOrderSPath                   @"lover-go/phoneActivity/addActivityOrder"
#define addRoute                                @"lover-go/route/addRoute"
#define getImageByUuid                          @"lover-go/image/getImageByUuid"
#define addOrder                                @"lover-go/businessOrder/addOrder"
#define getOrderByUserUuid                      @"lover-go/businessOrder/getOrderByUserUuid"
#define getServiceByPlaceUuid                   @"lover-go/service/getServiceByPlaceUuid"
#define getServiceItemsByService                @"lover-go/service/getServiceItemsByService"
#define getFindRouteInfoSPath                   @"lover-go/openRoute/findRouteInfo"
#define getOriginPlaceInfo                      @"lover-go/openRoute/getOriginPlaceInfo"
#define getCollectionByTypeSPath                @"lover-go/collection/getCollectionByType"
#define addCollectionByUserSPath                @"lover-go/collection/addCollectionByUser"
#define findCheckNumByTelSPath                  @"lover-go/appUser/findCheckNumByTel"
#define payAOrderPath                           @"lover-go/phoneActivity/payActivityOrderByOrderUuid"
#define findPayAOrderSPath                      @"lover-go/phoneActivity/findPayedActivityOrderByUserUuid"
#define findUnPayAOrderSPath                    @"lover-go/phoneActivity/findUnPayedActivityOrderByUserUuid"
#define cancleAOrderSPath                       @"lover-go/phoneActivity/cancleActivityOrderByUuid"


#define SuccNotification @"SuccNotification"
#define ErrorNotification @"RegristerError"

