//
//  LGPlaceDetailView.m
//  LoverGo
//
//  Created by YeXiao on 14-4-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGActivityImageInfo.h"
#import "LGUtilDefine.h"
#import "LGPlaceDetailView.h"

@implementation LGPlaceDetailView

@synthesize delegate = _delegate;
@synthesize cTView = _cTView;
@synthesize itemIView = _itemIView;

-(UILabel* )getLabelWithText:(NSString*)title fontSize:(int)fSize
{
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 25)];
    [label setBackgroundColor:[UIColor whiteColor]];
    [label setShadowColor:[UIColor whiteColor]];
    [label setShadowOffset:CGSizeMake(0.1, 0.1)];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:fSize];
    [label setFont:font];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label setText:title];
    return label;
}

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame isHaveR:YES];
}
-(id)initWithFrame:(CGRect)frame isHaveR:(BOOL)b
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor grayColor];
        
        _itemIView = [[EMAsyncImageView alloc] initWithFrame:CGRectMake(0, STATUSBAR, 320, 200) cornerRadius:0];//701,466
        [self addSubview:_itemIView];
        _itemIView.userInteractionEnabled = YES;
        UILabel* _titleLab = [self getLabelWithText:@"点击查看更多图片" fontSize:18];
        [_titleLab setBackgroundColor:[UIColor lightGrayColor]];
        [_titleLab setCenter:CGPointMake(160, 180)];
        [_titleLab setTextAlignment:NSTextAlignmentCenter];
        [_itemIView addSubview:_titleLab];
        
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"商家"];
        titleLab.userInteractionEnabled = true;
        [self addSubview:titleLab];
        
        
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:1];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        if (b) {
            UIButton *rightBtn = [LGUtil returnRightBtn];
            [rightBtn setTag:2];
            [rightBtn setCenter:CGPointMake(290, 20)];
            [rightBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
            [titleLab addSubview:rightBtn];
        }
        
    }
    return self;
}
-(void)btnClieck:(UIButton*)sender
{
    if (sender.tag == 2) {
        [_delegate showRight];
    }else{
        [_delegate returnLeftBtn];
    }
}
-(void)imageClieck:(UILabel*)sender
{
    [_delegate showImg];
}
-(void)telLabelEvent:(UILabel*)sender
{
    [_delegate callPhone:_aInfo.tel];
}
-(void)placeLabelEvent:(UILabel*)sender
{
    [_delegate callAdress];
}
-(void)seeLabelEvent:(UILabel*)sender
{
    [_delegate callDetailView];
}
-(void)reloadImages:(NSArray*)imgArr
{
    if (imgArr.count >=1) {
        LGActivityImageInfo* info = [imgArr objectAtIndex:1];
        if ([info.imageUrl compare:@""] != 0) {
            _itemIView.imageUrl = [NSString stringWithFormat:@"%@lover-go/admin/%@",baseUrl,info.imageUrl];
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClieck:)];
            [_itemIView addGestureRecognizer:singleTap];
        }else{
            [_itemIView setImage:[LGUtil getUnFoundImage]];
        }
    }
}
-(void)reloadDetailData:(LGPlaceInfo*)info;
{
    _aInfo = info;
    _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 200+STATUSBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (200+STATUSBAR))];
    _contentScrollView.backgroundColor = [UIColor grayColor];
    [_contentScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, 490)];
    [self addSubview:_contentScrollView];
    
    UILabel* _titleLab = [self getLabelWithText:_aInfo.placeName fontSize:18];
    [_titleLab setCenter:CGPointMake(160, 12)];
    [_contentScrollView addSubview:_titleLab];
    
    UILabel* _numLab =[self getLabelWithText:@"浪漫指数:" fontSize:15];
    [_numLab setFrame:CGRectMake(0, 26, 320, 50)];
    _numLab.userInteractionEnabled = true;
    [_contentScrollView addSubview:_numLab];
    
    //加星星
    _loveIconView = [[UIView alloc] initWithFrame:CGRectMake(80, 40, 100, 20)];
    [_contentScrollView addSubview:_loveIconView];
    [self setLoveIcon:[_aInfo.romanticPoint intValue]];
    
    //
    UILabel* _totalLab = [self getLabelWithText:@"浪漫理由" fontSize:18];
    [_totalLab setCenter:CGPointMake(160, 100)];
    [_contentScrollView addSubview:_totalLab];
    UILabel* _tLab = [self getLabelWithText:_aInfo.romanticReason fontSize:15];
    _tLab.lineBreakMode = UILineBreakModeWordWrap;
    _tLab.numberOfLines = 4 ;
    [_tLab setFrame:CGRectMake(0, 115, 320,80)];
    [_contentScrollView addSubview:_tLab];
    
    //电话
    UILabel* _telLab = [self getLabelWithText:_aInfo.tel fontSize:18];
    [_telLab setFrame:CGRectMake(0, 210, 320, 40)];
    _telLab.userInteractionEnabled = true;
    [_contentScrollView addSubview:_telLab];
    UITapGestureRecognizer *tapGestureTel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(telLabelEvent:)];
    [_telLab addGestureRecognizer:tapGestureTel];
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"next_pic.png"]];
    [imgView setCenter:CGPointMake(290, 20)];
    [_telLab addSubview:imgView];
    
    //地址
    UILabel* _placeLab = [self getLabelWithText:_aInfo.detailAdress fontSize:18];
    [_placeLab setFrame:CGRectMake(0, 254, 320, 40)];
    _placeLab.userInteractionEnabled = true;
    [_contentScrollView addSubview:_placeLab];
    UITapGestureRecognizer *tapGestureTel1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(placeLabelEvent:)];
    [_placeLab addGestureRecognizer:tapGestureTel1];
    
    UIImageView *img1View = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"next_pic.png"]];
    [img1View setCenter:CGPointMake(290, 20)];
    [_placeLab addSubview:img1View];

    UILabel* _timelLab = [self getLabelWithText:@"商家介绍：" fontSize:18];
    [_timelLab setFrame:CGRectMake(0, 300, 320, 40)];
    [_contentScrollView addSubview:_timelLab];
    UILabel* _atimeLabb = [self getLabelWithText:_aInfo.placeInfo fontSize:15];
    _atimeLabb.lineBreakMode = UILineBreakModeWordWrap;
    _atimeLabb.numberOfLines = 5 ;
    [_atimeLabb setFrame:CGRectMake(0, 345, 320, 90)];
    [_contentScrollView addSubview:_atimeLabb];

    UILabel* _seeLab = [self getLabelWithText:@"商家详情" fontSize:18];
    [_seeLab setFrame:CGRectMake(0, 445, 320, 40)];
    _seeLab.userInteractionEnabled = true;
    [_contentScrollView addSubview:_seeLab];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(seeLabelEvent:)];
    [_seeLab addGestureRecognizer:tap2];
    
    UIImageView *img2View = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"next_pic.png"]];
    [img2View setCenter:CGPointMake(290, 20)];
    [_seeLab addSubview:img2View];
}
-(void)setLoveIcon:(int)f
{
    for (UIView *view in [_loveIconView subviews]) {
        [view removeFromSuperview];
    }
    if (f<0){
        f = 1;
    }
    if (f>=5) {
        f=5;
    }
    
    for (int i = 1; i<= 5; i++) {
        UIImageView* icon = [[UIImageView alloc] initWithFrame:CGRectMake((i-1) * 20, 1, 20, 20)];
        if (i<=f) {
            [icon setImage:[UIImage imageNamed:@"hot_heart_red.png"]];
        }else{
            [icon setImage:[UIImage imageNamed:@"hot_heart_gray.png"]];
        }
        [_loveIconView addSubview:icon];
    }
}

@end
