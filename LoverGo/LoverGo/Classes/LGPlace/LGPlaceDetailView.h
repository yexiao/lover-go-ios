//
//  LGPlaceDetailView.h
//  LoverGo
//
//  Created by YeXiao on 14-4-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGPlaceInfo.h"
#import "ELHeaderView.h"

@protocol LGPlaceDetailViewDelegate <NSObject>

-(void)returnLeftBtn;
-(void)showRight;
-(void)showImg;
-(void)callPhone:(NSString* )phoneNum;
-(void)callAdress;
-(void)callDetailView;

@end

@interface LGPlaceDetailView : UIView
{
    UIScrollView *_contentScrollView;
    UITableView* _cTView;
    LGPlaceInfo* _aInfo;
    UIView* _loveIconView;
    id<LGPlaceDetailViewDelegate> _delegate;
    EMAsyncImageView* _itemIView;
}
@property(nonatomic,strong)UITableView* cTView;
@property(nonatomic,strong)EMAsyncImageView* itemIView;
@property(nonatomic,strong) id<LGPlaceDetailViewDelegate> delegate;
-(void)reloadDetailData:(LGPlaceInfo*)info;
-(void)reloadImages:(NSArray*)imgArr;
-(id)initWithFrame:(CGRect)frame isHaveR:(BOOL)b;

@end
