//
//  LGPlaceDetailViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-4-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "MJExtension.h"
#import "LGUtil.h"
#import "MapViewController.h"
#import "Toast+UIView.h"
#import "LGActivityImageInfo.h"
#import "LGUtilDefine.h"
#import "LGDataCenter.h"
#import "LGRequestManager.h"
#import "LGShowViewController.h"
#import "LGPlaceDetailViewController.h"

@implementation LGPlaceDetailViewController

@synthesize detailView = _detailView;

-(id)initWithPlaceInfo:(LGPlaceInfo*)info
{
    return [self initWithPlaceInfo:info haveRigh:YES];
}
-(id)initWithPlaceInfo:(LGPlaceInfo *)info haveRigh:(BOOL)b
{
    _isHaveR = b;
    _phoneNum = nil;
    self = [super init];
    if (self) {
        _pInfo = info;
        __block LGPlaceDetailViewController *_blockSelf = self;
        NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                              info.uuid,@"uuid",
                              nil];
        [[SVHTTPClient sharedClient] GET:getPlaceByUuid
                              parameters:para
                              completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                                  if (error) {
                                      //抛报错通知
                                      return ;
                                  }
                                  [_blockSelf refreshView:response];
                              }];
    }
    return self;
}
-(void)refreshView:(id)response
{
    if (response == nil) {
        NSLog(@"加载失败!，没有数据");
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        NSLog(@"加载失败!");
        return;
    }else{
        NSArray* list = [jsonObject objectForKey:@"list"];
        if (list.count == 0) {
            [_detailView reloadDetailData:_pInfo];//刷新数据;
            return;
        }
        _pInfo = [LGPlaceInfo objectWithKeyValues:[list objectAtIndex:0]];
        [_detailView reloadDetailData:_pInfo];//刷新数据
        [self getPlaceImages];
    }
}
-(void)getPlaceImages
{
    __block LGPlaceDetailViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          _pInfo.uuid,@"uuid",
                          nil];
    [[SVHTTPClient sharedClient] GET:getImageByUuid
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  [_blockSelf.view makeToast:@"请求网络失败"];
                                  return ;
                              }if (response == nil) {
                                  [_blockSelf.view makeToast:@"没有数据"];
                                  return;
                              }
                              NSError *jsonError = nil;
                              id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
                              if (jsonError) {
                                  [_blockSelf.view makeToast:@"解析失败"];
                                  return;
                              }else{
                                  NSArray *tmpArray = [LGActivityImageInfo objectArrayWithKeyValuesArray:[jsonObject objectForKey:@"list"]];
                                  if (tmpArray.count == 0) {
                                      [_blockSelf.view makeToast:@"没有更多图片了"];
                                      return;
                                  }
                                  
                                  [_detailView reloadImages:tmpArray];//刷新数据
//                                  [LGUtil showImages:tmpArray];
                              }
                          }];
}

-(void)loadView
{
    _detailView = [[LGPlaceDetailView alloc] initWithFrame:CGRectMake(0, STATUSBAR, SCREEN_WIDTH, SCREEN_HEIGHT) isHaveR:_isHaveR];
    _detailView.cTView.dataSource = self;
    _detailView.cTView.delegate = self;
    _detailView.delegate = self;
//    [_detailView.cTView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ELViewControllerCellIdentifier"];
    [self setView:_detailView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 15;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ELViewControllerCellIdentifier" forIndexPath:indexPath];
    
    [cell.textLabel setText:@"text Label"];
    return cell;
}
-(void)returnLeftBtn
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)showImg
{
    __block LGPlaceDetailViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          _pInfo.uuid,@"uuid",
                          nil];
    [[SVHTTPClient sharedClient] GET:getImageByUuid
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  [_blockSelf.view makeToast:@"请求网络失败"];
                                  return ;
                              }if (response == nil) {
                                  [_blockSelf.view makeToast:@"没有数据"];
                                  return;
                              }
                              NSError *jsonError = nil;
                              id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
                              if (jsonError) {
                                  [_blockSelf.view makeToast:@"解析失败"];
                                  return;
                              }else{
                                  NSArray *tmpArray = [LGActivityImageInfo objectArrayWithKeyValuesArray:[jsonObject objectForKey:@"list"]];
                                  if (tmpArray.count == 0) {
                                      [_blockSelf.view makeToast:@"没有更多图片了"];
                                      return;
                                  }
                                  [LGUtil showImages:tmpArray];
                              }
                          }];
}
-(void)callPhone:(NSString *)phoneNum
{
    _phoneNum = phoneNum;
    _asheet = [[LXActionSheet alloc]initWithTitle:@"是否呼叫商家" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"呼叫" otherButtonTitles:nil];
    _asheet.tag =2;
    [_asheet showInView:self.view];
    
//    [self.navigationController popViewControllerAnimated:YES];
}
-(void)callAdress
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                         _pInfo.placeName,@"address",
                                         /*@"上海市",@"city",*/
                                         @"baidu",@"from_map_type",
                                         _pInfo.latitude,@"baidu_lat",
                                         _pInfo.longitude,@"baidu_lng",
                                         /*@"浦东新区",@"region",*/ nil];
    
    
    MapViewController *mv = [[MapViewController alloc] init];
    mv.navDic = dic;
    mv.mapType = RegionNavi;
    [self.navigationController pushViewController:mv animated:YES];
}
-(void)callDetailView
{
    LGShowViewController *vc = [[LGShowViewController alloc]initWithURL:_pInfo.htmlName];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)showRight
{
    _asheet = [[LXActionSheet alloc]initWithTitle:@"看看我们能做啥" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"收藏" otherButtonTitles:@[@"分享",@"到这里去",@"加到心愿清单"]];
    _asheet.tag =1;
    [_asheet showInView:self.view];
}
#pragma mark - LXActionSheetDelegate

- (void)didClickOnButtonIndex:(NSInteger *)buttonIndex
{
    int tag = (int)buttonIndex;
    if (_asheet.tag ==1) {
        if (tag == 1) {
            //分享
            UIImage *_shareImg = nil;
            if (_detailView.itemIView) {
                _shareImg = _detailView.itemIView.image;
            }
            
            [LGUtil shareToSNSURL:[NSString stringWithFormat:@"%@%@?placeUuid=%@",baseUrl,getOriginPlaceInfo,_pInfo.uuid] title:@"情侣去哪儿" img:_shareImg content:_pInfo.placeName
                       controller:self];
        }else if (tag ==2){
            //到这里去
            [self callAdress];
        }else if (tag == 3){
            //加到心愿清单
            [self wishOrPlace:YES];
        }
    }if (_asheet.tag ==2) {
        _phoneNum = [_phoneNum stringByReplacingOccurrencesOfString:@"-" withString:@""];
        _phoneNum = [_phoneNum stringByReplacingOccurrencesOfString:@" " withString:@""];
        UIWebView *web =[[UIWebView alloc]init];
        [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%s",[_phoneNum UTF8String]]]]];
        [self.view addSubview:web];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",_phoneNum]]];
        _phoneNum = nil;
    }
}

- (void)didClickOnDestructiveButton
{
    //收藏按钮
    if (_asheet.tag ==1) {
        [self wishOrPlace:NO];
    }else if(_asheet.tag == 2){
    }
}
-(void)wishOrPlace:(BOOL)b
{
    NSString* str = @"place";
    NSString* titleStr = @"收藏";
    if (b) {
        str = @"wish";
        titleStr = @"加入心愿清单";
    }
    LGUserInfo* info =[LGDataCenter getSingleton].userInfo;
    if (info == NULL) {
        [self.view makeToast:[NSString stringWithFormat:@"请先登录才能%@哦!",titleStr]];
        return;
    }
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          info.uuid,@"userUuid",
                          _pInfo.uuid,@"objectUuid",
                          str,@"collectionType",
                          nil];
    [[SVHTTPClient sharedClient] GET:addCollectionByUserSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  [self.view makeToast:@"请求失败!"];
                                  return ;
                              }
                              [self.view makeToast:[NSString stringWithFormat:@"%@成功!",titleStr]];
                          }];
}

- (void)didClickOnCancelButton
{
    NSLog(@"cancelButton donothing");
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}
-(void)viewDidUnload
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
