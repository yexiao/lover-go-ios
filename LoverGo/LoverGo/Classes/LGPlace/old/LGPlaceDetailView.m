//
//  LGPlaceDetailView.m
//  LoverGo
//
//  Created by YeXiao on 14-4-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGPlaceDetailView.h"

@implementation LGPlaceDetailView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"商家详情"];
        [titleLab setBackgroundColor:[UIColor orangeColor]];
        [self addSubview:titleLab];
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, STATUSBAR + 40, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR + TABBAR))];
        [self addSubview:_scrollView];
        
        _textLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 500)];
        _textLab.numberOfLines = 0;
        [_scrollView addSubview:_textLab];
        
        [_scrollView setContentSize:CGSizeMake(320, 800)];
        
    }
    return self;
}
-(void)reloadDetailData:(LGPlaceInfo*)info;
{
    NSString* tmpStr = [NSString stringWithFormat:@"%@\n人均%@元\n浪漫理由\n%@\n联系电话%@\n%@\n商家介绍\n%@\n",info.placeName,info.payRank,info.romanticReason,info.tel,info.detailAdress,info.placeInfo];
    [_textLab setText:tmpStr];
}

@end
