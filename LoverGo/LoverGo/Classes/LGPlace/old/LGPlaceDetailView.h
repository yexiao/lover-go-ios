//
//  LGPlaceDetailView.h
//  LoverGo
//
//  Created by YeXiao on 14-4-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGPlaceInfo.h"

@interface LGPlaceDetailView : UIView
{
    UIScrollView *_scrollView;
    UILabel *_textLab;
}
-(void)reloadDetailData:(LGPlaceInfo*)info;

@end
