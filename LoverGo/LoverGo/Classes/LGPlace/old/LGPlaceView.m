//
//  LGPlaceView.m
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGPlaceView.h"


@implementation LGPlaceView

@synthesize reloading = _reloading;
@synthesize qtmquitView = _qtmquitView;
@synthesize delegate = _delegate;
@synthesize refreshHeaderView = _refreshHeaderView;
@synthesize refreshFooterView = _refreshFooterView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _qtmquitView = [[TMQuiltView alloc] initWithFrame:frame];
        _qtmquitView.backgroundColor = [UIColor grayColor];
        [self addSubview:_qtmquitView];
    }
    return self;
}

-(void)reloadAllData
{
    [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.0];
}
-(void)refreshView
{
    [_qtmquitView reloadData];
    [self createHeaderView];
    [self finishReloadingData];
    [self setFooterView];
}
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
//初始化刷新视图
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
#pragma mark
#pragma methods for creating and removing the header view

-(void)createHeaderView{
    if (_refreshHeaderView && [_refreshHeaderView superview]) {
        [_refreshHeaderView removeFromSuperview];
    }
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:
                          CGRectMake(0.0f, 0.0f - _qtmquitView.bounds.size.height,
                                     _qtmquitView.frame.size.width, _qtmquitView.bounds.size.height)];
    
    _refreshHeaderView.delegate = self;
	[_qtmquitView addSubview:_refreshHeaderView];
    NSLog(@"yyyyyyyy:%f\n",_qtmquitView.contentOffset.y);
    [_refreshHeaderView refreshLastUpdatedDate];
}
#pragma mark -
#pragma mark method that should be called when the refreshing is finished
- (void)finishReloadingData{
	
	//  model should call this when its done loading
	_reloading = NO;
    
	if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_qtmquitView];
    }
    
    if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:_qtmquitView];
        [self setFooterView];
    }
    
    // overide, the actula reloading tableView operation and reseting position operation is done in the subclass
}
-(void)setFooterView{
	//    UIEdgeInsets test = self.aoView.contentInset;
    // if the footerView is nil, then create it, reset the position of the footer
    CGFloat height = MAX(_qtmquitView.contentSize.height, _qtmquitView.frame.size.height);
    if (_refreshFooterView && [_refreshFooterView superview])
	{
        // reset position
        _refreshFooterView.frame = CGRectMake(0.0f,height,_qtmquitView.frame.size.width,_qtmquitView.frame.size.height);
    }else
	{
        // create the footerView
        _refreshFooterView = [[EGORefreshTableFooterView alloc] initWithFrame:
                              CGRectMake(0.0f, height,_qtmquitView.frame.size.width, _qtmquitView.frame.size.height)];
        _refreshFooterView.delegate = self;
        [_qtmquitView addSubview:_refreshFooterView];
    }
    if (_refreshFooterView)
	{
        [_refreshFooterView refreshLastUpdatedDate];
    }
}
-(void)removeFooterView
{
    if (_refreshFooterView && [_refreshFooterView superview])
	{
        [_refreshFooterView removeFromSuperview];
    }
    _refreshFooterView = nil;
}

#pragma mark -
#pragma mark EGORefreshTableDelegate Methods

- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    _reloading = YES;
	if (_delegate) {
        [_delegate beginToReloadData:self RefreshType:aRefreshPos];
    }
}

- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}


// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}
@end
