//
//  LGPlaceDetailViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-4-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtilDefine.h"
#import "LGRequestManager.h"
#import "LGPlaceDetailViewController.h"

@implementation LGPlaceDetailViewController

@synthesize info = _info;
@synthesize detailView = _detailView;

-(id)initWithUuid:(NSString*)uuid
{
    self = [super init];
    if (self) {
        _uuid = uuid;
    }
    return self;
}

-(void)loadView
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PlaceByUuid:) name:getPlaceByUuid object:nil];
    
    _detailView = [[LGPlaceDetailView alloc] initWithFrame:CGRectMake(0, STATUSBAR, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self setView:_detailView];
    
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          _uuid,@"uuid",
                          nil];
    
    [[LGRequestManager getSingleton] sendRequest:para mapper:[LGPlaceInfo getObjectMapping] serverPath:getPlaceByUuid jsonPath:@"list" canTouch:NO];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:getPlaceByUuid name:nil object:self];
}
-(void)PlaceByUuid:(NSNotification *)nof
{
    
    
    id obj = [nof object];
    if (!obj) {
        return;
    }
    if ([obj isKindOfClass:[NSArray class]] && [(NSArray*)obj count] == 1) {
        LGPlaceInfo *pInfo = [(NSArray*)obj objectAtIndex:0];
        if ([pInfo isKindOfClass:[LGPlaceInfo class]]) {
            _info = pInfo;
            [_detailView reloadDetailData:_info];//刷新数据
        }
    }
    else if([obj isKindOfClass:[NSString class]])
    {
        [[LGRequestManager getSingleton] starLoading:@"加载失败" Type:ShowLoading Enabled:NO];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
