//
//  LGPlaceViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGPlaceView.h"
#import "BaseViewController.h"
#import "LGPlaceByMapInfo.h"

@interface LGPlaceViewController : UIViewController<LGPlaceViewDelegate,TMQuiltViewDataSource,TMQuiltViewDelegate>
{
    LGPlaceView* _placeView;
    NSMutableArray* _placeArray;
}

-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype;
-(LGPlaceInfo*)getPlaceInfoWithIndex:(int)index;
@end
