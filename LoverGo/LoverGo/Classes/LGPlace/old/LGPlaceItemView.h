//
//  LGPlaceItemView.h
//  LoverGo
//
//  Created by YeXiao on 14-4-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "TMQuiltViewCell.h"
#import "LGPlaceInfo.h"

@interface LGPlaceItemView : TMQuiltViewCell
{
    UIImageView* _iconIView;
    UILabel *_titleLabel;
    UILabel *_avgLabel;
    UILabel* _contentLabel;
    UIView* _loveIconView;
}

-(void)reloadData:(LGPlaceInfo*)info;
-(void)setIconImgView:(UIImage*)img;
-(void)setLoveIcon:(int)f;//设置心的个数

@end
