//
//  LGPlaceView.h
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMQuiltView.h"
#import "EGORefreshTableHeaderView.h"
#import "EGORefreshTableFooterView.h"

@class LGPlaceView;

@protocol LGPlaceViewDelegate <NSObject>

-(void)beginToReloadData:(LGPlaceView*)pView RefreshType:(EGORefreshPos)aRefreshPos;

@end

@interface LGPlaceView : UIView<EGORefreshTableDelegate>
{
    TMQuiltView *_qtmquitView;
	//EGOHeader
    EGORefreshTableHeaderView *_refreshHeaderView;
    //EGOFoot
    EGORefreshTableFooterView *_refreshFooterView;
    
    BOOL _reloading;
}
@property(nonatomic,assign)BOOL reloading;
@property(nonatomic,strong)TMQuiltView *qtmquitView;
@property(nonatomic,assign) id <LGPlaceViewDelegate> delegate;
@property(nonatomic,strong)EGORefreshTableHeaderView *refreshHeaderView;
@property(nonatomic,strong)EGORefreshTableFooterView *refreshFooterView;


-(void)reloadAllData;
@end
