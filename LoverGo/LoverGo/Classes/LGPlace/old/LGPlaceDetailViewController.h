//
//  LGPlaceDetailViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-4-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGPlaceInfo.h"
#import "LGPlaceDetailView.h"

@interface LGPlaceDetailViewController : UIViewController
{
    NSString* _uuid;
    LGPlaceInfo *info;
    LGPlaceDetailView * _detailView;
}
@property(nonatomic,strong)LGPlaceInfo *info;
@property(nonatomic,strong)LGPlaceDetailView* detailView;


-(id)initWithUuid:(NSString*)uuid;

@end
