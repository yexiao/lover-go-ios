//
//  LGPlaceItemView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-2.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMAsyncImageView.h"
#import "LGPlaceInfo.h"
#import "SWTableViewCell.h"

@interface LGPlaceItemView : SWTableViewCell
{
    EMAsyncImageView* _iconIView;
    UILabel *_titleLabel;
    UILabel *_avgLabel;
    UILabel *_contentLabel;
    UIView* _loveIconView;
}
-(void)reloadPlaceData:(LGPlaceInfo*)info;
@end
