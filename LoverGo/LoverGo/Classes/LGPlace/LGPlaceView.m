//
//  LGPlaceView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-2.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGPlaceView.h"

@implementation LGPlaceView

@synthesize tableView = _tableView;
@synthesize header = _header;
@synthesize footer = _footer;
@synthesize dropDownView = _dropDownView;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"地点"];
        [titleLab setBackgroundColor:[UIColor orangeColor]];
        [self addSubview:titleLab];
        _dropDownView = NULL;
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 80+STATUSBAR, frame.size.width, frame.size.height - 30)];
        _tableView.backgroundColor = [UIColor whiteColor];
        _footer = [MJRefreshFooterView footer];
        _footer.scrollView = _tableView;
        _header = [MJRefreshHeaderView header];
        _header.scrollView = _tableView;
        [self addSubview:_tableView];
    }
    return self;
}
-(void)setDropDownListView:(UIViewController*)controller
{
    _dropDownView = [[DropDownListView alloc] initWithFrame:CGRectMake(0,STATUSBAR+40, SCREEN_WIDTH, 40) dataSource:controller delegate:controller];
    _dropDownView.mSuperView = controller.view;
    [self addSubview:_dropDownView];
}
-(void)btnClieck:(UIButton*)sender
{
    NSLog(@"1111");
}

@end
