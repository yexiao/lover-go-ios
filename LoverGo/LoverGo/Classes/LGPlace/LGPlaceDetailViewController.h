//
//  LGPlaceDetailViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-4-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGPlaceInfo.h"
#import "LXActionSheet.h"
#import "LGPlaceDetailView.h"

@interface LGPlaceDetailViewController : UIViewController<LGPlaceDetailViewDelegate,LXActionSheetDelegate,UITableViewDataSource,UITableViewDelegate>
{
    BOOL _isHaveR;
    NSString* _uuid;
    LGPlaceInfo *_pInfo;
    LGPlaceDetailView * _detailView;
    LXActionSheet* _asheet;
    NSString* _phoneNum;
    
}
@property (nonatomic,strong)NSArray *annotations;
@property(nonatomic,strong)LGPlaceDetailView* detailView;


-(id)initWithPlaceInfo:(LGPlaceInfo*)info;

-(id)initWithPlaceInfo:(LGPlaceInfo*)info haveRigh:(BOOL)b;

@end
