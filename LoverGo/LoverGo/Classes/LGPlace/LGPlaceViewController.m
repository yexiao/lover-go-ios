//
//  LGPlaceViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-5-31.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "MJExtension.h"
#import "LGRequestManager.h"
#import "LGPlaceItemView.h"
#import "LGPlaceViewController.h"
#import "LGPlaceDetailViewController.h"

@interface LGPlaceViewController ()

@end

@implementation LGPlaceViewController

-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype
{
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:img tag:mvtype];
        
        firstSet = 0;
        para = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                @"1",@"page",
                @"cat001",@"placeType",
                @"tj",@"city",/*
                               @"tj01aa",@"regionCode",
                               @"",@"longitude",
                               @"",@"latitude",*/
                nil];
        
        chooseArray = [NSMutableArray arrayWithArray:@[
                                                       @[@"餐饮",@"娱乐"],
                                                       @[@"默认风格",@"文艺范儿",@"高压",@"热闹",@"静谧",@"官方推荐"],
                                                       @[@"默认排序",@"浪漫指数",@"价格最高",@"价格最低",@"离我最近"]
                                                       ]];
        
        _placeArray = [[NSMutableArray alloc] init];
        _utilModel = nil;
    }
    return self;
}
-(void)loadView
{
    _placeView = [[LGPlaceView alloc] initWithFrame:CGRectMake(0, STATUSBAR, SCREEN_WIDTH, SCREEN_HEIGHT-90 - STATUSBAR)];
    _placeView.tableView.delegate = self;
    _placeView.tableView.dataSource = self;
    _placeView.tableView.rowHeight = 100;
    _placeView.tableView.allowsSelection = NO; // We essentially implement our own selection
    _placeView.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0); // Makes the horizontal row seperator stretch the entire length of the table view
    
    [self setView:_placeView];
    [_placeView setDropDownListView:self];
    
    
    __block LGPlaceViewController *_blockSelf = self;
    _placeView.header.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //发送初始请求
        [_blockSelf sendRequestByPlacePage:1];
    };
    [_placeView.header beginRefreshing];
    
    _placeView.footer.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //进入刷新状态
        if (_blockSelf->_utilModel) {
            int nextPage = [_blockSelf->_utilModel.nowPage intValue] +1;
            if (nextPage > [_blockSelf->_utilModel.totalPage intValue]) {
                [_blockSelf->_placeView.footer endRefreshing];
                return ;
            }
            [_blockSelf sendRequestByPlacePage:nextPage];
        }
    };
}
-(void)sendRequestByPlacePage:(int)page
{
    __block LGPlaceViewController *_blockSelf = self;
    if (page !=1) {
        [para setValue:[NSString stringWithFormat:@"%d",page] forKeyPath:@"page"];
    }
    [[SVHTTPClient sharedClient] GET:getPlaceByMapSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  //抛报错通知
                                  return ;
                              }
                              [_blockSelf refreshView:response placePage:page];
                          }];
}
-(void)refreshView:(id)response placePage:(int)page
{
    [_placeView.header endRefreshing];
    [_placeView.footer endRefreshing];
    if (response == nil) {
        NSLog(@"加载失败!，没有数据");
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        NSLog(@"加载失败!");
        return;
    }else{
        if (page == 1) {
            [_placeArray removeAllObjects];
        }
        _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        [_placeArray addObjectsFromArray:[LGPlaceInfo objectArrayWithKeyValuesArray:_utilModel.listData]];
        [_placeView.tableView reloadData];//刷新数据
        
    }
}
#pragma mark - Table view data source
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _placeArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGPlaceInfo *pInfo= [_placeArray objectAtIndex:indexPath.row];
    
    LGPlaceDetailViewController *vc = [[LGPlaceDetailViewController alloc] initWithPlaceInfo:pInfo];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * showUserInfoCellIdentifier = @"LGAtiviteItemView";
    LGPlaceItemView * cell = [tableView dequeueReusableCellWithIdentifier:showUserInfoCellIdentifier];
    if (cell == nil)
    {
        NSMutableArray *leftBtns = [NSMutableArray new];
        NSMutableArray *rightBtns = [NSMutableArray new];
        
        [leftBtns addUtilityButtonWithColor:
         [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0]
                                      title:@"到这里去"];
        [leftBtns addUtilityButtonWithColor:
         [UIColor orangeColor] title:@"心愿清单"];
        [rightBtns addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                       title:@"分享"];
        [rightBtns addUtilityButtonWithColor:
         [UIColor orangeColor] title:@"收藏"];
        
        cell = [[LGPlaceItemView alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:showUserInfoCellIdentifier
                                  containingTableView:_placeView.tableView // Used for row height and selection
                                   leftUtilityButtons:leftBtns
                                  rightUtilityButtons:rightBtns];
        cell.delegate = self;
    }
    
    cell.detailTextLabel.text = @"Some detail text";
//    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    LGPlaceInfo *pInfo= [_placeArray objectAtIndex:indexPath.row];
    [cell reloadPlaceData:pInfo];
    return cell;
}
#pragma mark - SWTableViewDelegate

- (void)swippableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"left 0");
            break;
        case 1:
            NSLog(@"left 1");
            break;
        default:
            break;
    }
}

- (void)swippableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
        {
            NSLog(@"More button was pressed");
            break;
        }
        case 1:
        {
            NSLog(@"More button was pressed");
            break;
        }
        default:
            break;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark -- dropDownListDelegate
-(void) chooseAtSection:(NSInteger)section index:(NSInteger)index
{
    [para setValue:@"1" forKeyPath:@"page"];
    if (section == 0) {
        firstSet = index;
        [para setValue:[NSString stringWithFormat:@"cat00%d",index+1] forKeyPath:@"placeType"];
        if (index == 0) {
            chooseArray[1] = @[@"默认风格",@"文艺范儿",@"高压",@"热闹",@"静谧",@"官方推荐"];
            [para removeObjectForKey:@"labelUuid"];
        }
        if (index == 1) {
            //更改第二个标签内容
            chooseArray[1] = @[@"默认风格",@"大自然",@"健身",@"文艺",@"活力"];
            [para removeObjectForKey:@"labelUuid"];
        }
    }
    if (section == 1) {
        if (firstSet == 0) {
            switch (index) {
                case 0:
                    [para removeObjectForKey:@"labelUuid"];
                    break;
                case 1:
                    [para setValue:@"96e99b95-65f8-11e3-a401-060815525ede" forKeyPath:@"labelUuid"];
                    break;
                case 2:
                    [para setValue:@"f39da61f448ff487014491ffc56a0011" forKeyPath:@"labelUuid"];
                    break;
                case 3:
                    [para setValue:@"f39da61f448ff487014491ffb199000e" forKeyPath:@"labelUuid"];
                    break;
                case 4:
                    [para setValue:@"f39da61f448ff487014491ff7c8b000b" forKeyPath:@"labelUuid"];
                    break;
                case 5:
                    [para setValue:@"f39da61f448ff487014491ff345e0005" forKeyPath:@"labelUuid"];
                    break;
                    
                default:
                    break;
            }
        }else if (firstSet == 1){
            switch (index) {
                case 0:
                    [para removeObjectForKey:@"labelUuid"];
                    break;
                case 1:
                    [para setValue:@"5acf92ee-64df-11e3-a401-060815525ede" forKeyPath:@"labelUuid"];
                    break;
                case 2:
                    [para setValue:@"5a72cd40-64df-11e3-a401-060815525ede" forKeyPath:@"labelUuid"];
                    break;
                case 3:
                    [para setValue:@"5b699b0e-64df-11e3-a401-060815525ede" forKeyPath:@"labelUuid"];
                    break;
                case 4:
                    [para setValue:@"5a95795b-64df-11e3-a401-060815525ede" forKeyPath:@"labelUuid"];
                    break;
                    
                default:
                    break;
            }
        }
        
    }
    if (section == 2) {
        switch (index) {
            case 0:
                [para removeObjectForKey:@"orderBy"];
                break;
            case 1:
                [para setValue:@"romanticPoint" forKeyPath:@"orderBy"];
                break;
            case 2:
                [para setValue:@"payRankHigh" forKeyPath:@"orderBy"];
                break;
            case 3:
                [para setValue:@"payRankLow" forKeyPath:@"orderBy"];
                break;
            case 4://自动定位
                [para setValue:@"" forKeyPath:@"orderBy"];
                break;
                
            default:
                break;
        }
    }
    int page = [[para objectForKey:@"page"] intValue];
    
    if (page !=1) {
        [para setValue:@"1" forKeyPath:@"page"];
    }
    __block LGPlaceViewController *_blockSelf = self;
    [[SVHTTPClient sharedClient] GET:getPlaceByMapSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  //抛报错通知
                                  return ;
                              }
                              [_blockSelf refreshView:response placePage:page];
                          }];
}

#pragma mark -- dropdownList DataSource
-(NSInteger)numberOfSections
{
    return [chooseArray count];
}
-(NSInteger)numberOfRowsInSection:(NSInteger)section
{
    NSArray *arry =chooseArray[section];
    return [arry count];
}
-(NSString *)titleInSection:(NSInteger)section index:(NSInteger) index
{
    return chooseArray[section][index];
}
-(NSInteger)defaultShowSection:(NSInteger)section
{
    return 0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
