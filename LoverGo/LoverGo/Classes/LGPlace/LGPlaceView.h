//
//  LGPlaceView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-2.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"
#import "DropDownListView.h"

@interface LGPlaceView : UIView
{
    MJRefreshHeaderView *_header;
    MJRefreshFooterView *_footer;
    UITableView* _tableView;
    DropDownListView * _dropDownView;
}
@property(nonatomic,strong)DropDownListView* dropDownView;
@property(nonatomic,strong)MJRefreshHeaderView *header;
@property(nonatomic,strong)MJRefreshFooterView *footer;
@property(nonatomic,strong)UITableView* tableView;

-(void)setDropDownListView:(UIViewController*)controller;

@end
