//
//  LGPlaceViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-5-31.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGPlaceView.h"
#import "LGUtilModel.h"
#import "BaseViewController.h"
#import "SWTableViewCell.h"

@interface LGPlaceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate>
{
    LGPlaceView *_placeView;
    NSMutableArray* chooseArray;
    NSMutableArray* _placeArray;
    LGUtilModel *_utilModel;
    NSMutableDictionary *para;//请求的参数
    int firstSet;//第一个选项卡的选择的值
}

-(void)refreshView:(id)response placePage:(int)page;
-(void)sendRequestByPlacePage:(int)page;

-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype;

@end
