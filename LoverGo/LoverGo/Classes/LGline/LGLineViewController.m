//
//  LGLineViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGLineViewController.h"

@interface LGLineViewController ()

@end

@implementation LGLineViewController

-(void)loadView
{
    _lineView = [[LGLineView alloc] initWithFrame:CGRectMake(0, STATUSBAR, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self setView:_lineView];
}
-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype
{
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:img tag:mvtype];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
