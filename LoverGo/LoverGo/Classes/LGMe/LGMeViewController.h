//
//  LGMeViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGMeView.h"
#import "LGSignInView.h"
#import "LXActionSheet.h"
#import "EMAsyncImageView.h"
#import "VPImageCropperViewController.h"
#import "BaseViewController.h"

@interface LGMeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,LGSignInViewDelegate,LGMeViewDelegate,LXActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate, VPImageCropperDelegate>
{
    LGMeView* _meView;
    EMAsyncImageView *_itemIView;
}
@property (nonatomic,strong) LXActionSheet *actionSheet;

-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype;
@end
