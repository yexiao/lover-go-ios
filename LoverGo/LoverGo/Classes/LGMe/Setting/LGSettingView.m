//
//  LGSettingView.m
//  LoverGo
//
//  Created by YeXiao on 14-5-21.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGSettingView.h"

@implementation LGSettingView

@synthesize tableView = _tableView;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"个人信息"];
        titleLab.userInteractionEnabled = YES;
        [self addSubview:titleLab];
        
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:0];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, STATUSBAR+40, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR + TABBAR - 40)) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [_tableView setDirectionalLockEnabled:YES];
        [self addSubview:_tableView];
        
    }
    return self;
}
-(void)btnClieck:(UIButton *)sender
{
    [_delegate backLeft];
}

@end
