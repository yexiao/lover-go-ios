//
//  LGChangeNameViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-5-22.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGDataCenter.h"
#import "LGRequestManager.h"
#import "LGChangeViewController.h"

@interface LGChangeViewController ()

@end

@implementation LGChangeViewController

-(id)initWithType:(SettingItemType)sitemType
{
    if ([self init]) {
        _sitemType = sitemType;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _cNameView =[[LGChangeView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) type:_sitemType];
    _cNameView.delegate = self;
    [self setView:_cNameView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeSucc:) name:userUpDateSPath object:nil];
}
-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:userUpDateSPath name:nil object:self];
}
-(void)changeSucc:(NSNotification *)nof
{
    if ([[nof name] compare:userUpDateSPath] == 0) {
        [self dismissModalViewControllerAnimated:YES];
    }
}

-(void)LGChangeView:(LGChangeView*)view withType:(ChangeBtnType)type
{
    if (type == ChangeBtnTypeCanel) {
        [self dismissModalViewControllerAnimated:YES];
    }else{
        NSMutableDictionary *para = [[NSMutableDictionary alloc] init];
        [para setObject:[LGDataCenter getSingleton].userInfo.uuid forKey:@"appUser.uuid"];
        switch (_sitemType) {
            case SettingItemTypeName:
                [para setObject:[_cNameView.nameTF text] forKey:@"appUser.userName"];
                break;
            case SettingItemTypeSay:
                [para setObject:[_cNameView.nameTF text] forKey:@"appUser.say"];
                break;
            case SettingItemTypePwd:
                [para setObject:[_cNameView.nameTF text] forKey:@"appUser.password"];
                break;
            case SettingItemTypeFriend:
                [para setObject:[_cNameView.nameTF text] forKey:@"friendEmail"];
                break;
            default:
                break;
        }
        
        [[LGRequestManager getSingleton] sendRequestAboutUser:userUpDateSPath para:para];
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
