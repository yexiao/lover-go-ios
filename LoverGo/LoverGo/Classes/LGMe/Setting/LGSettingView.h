//
//  LGSettingView.h
//  LoverGo
//
//  Created by YeXiao on 14-5-21.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    SettingItemTypeHead = 0,
    SettingItemTypeName,
    SettingItemTypeSay,
    SettingItemTypeRegin,
    SettingItemTypePwd,
    SettingItemTypeSex,
    SettingItemTypeFriend,
}SettingItemType;

@protocol LGSettingViewDelegate <NSObject>

-(void)backLeft;

@end

@interface LGSettingView : UIView
{
    UITableView* _tableView;
    id<LGSettingViewDelegate> _delegate;
}
@property UITableView* tableView;
@property(nonatomic,strong) id<LGSettingViewDelegate> delegate;

@end
