//
//  LGChangeNameView.m
//  LoverGo
//
//  Created by YeXiao on 14-5-22.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGChangeView.h"


@implementation LGChangeView

@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame type:(SettingItemType)type
{
    self = [super initWithFrame:frame];
    if (self) {
        _sItemType = type;
        [self setBackgroundColor:[UIColor whiteColor]];
        
        UIImageView *bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 60, 280, 100)];
        [bgImgView setImage:[LGUtil get9ScalImg:TypeImgBG]];
        [self addSubview:bgImgView];
        bgImgView.userInteractionEnabled = YES;
        
        UILabel* nLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 280, 40)];
        [nLabel setBackgroundColor:[UIColor clearColor]];
        switch (_sItemType) {
            case SettingItemTypeName:
                [nLabel setText:@"请输入新的昵称"];
                break;
            case SettingItemTypeSay:
                [nLabel setText:@"请输入新的签名"];
                break;
            case SettingItemTypeRegin:
                [nLabel setText:@"请选择所在地"];
                break;
            case SettingItemTypePwd:
                [nLabel setText:@"请输入新的密码"];
                break;
            case SettingItemTypeSex:
                [nLabel setText:@"请选择性别"];
                break;
            case SettingItemTypeFriend:
                [nLabel setText:@"请输入绑定好友email"];
                break;
                
            default:
                break;
        }
        [bgImgView addSubview:nLabel];
        
        _nameTF = [[UITextField alloc] initWithFrame:CGRectMake(0, 55, 275, 30)];
        [_nameTF setBorderStyle:UITextBorderStyleBezel]; //外框类型
        _nameTF.delegate = self;
        [_nameTF becomeFirstResponder];
        [bgImgView addSubview:_nameTF];
        
        UIButton *sureBtn =[LGUtil get9ScalBtn:CGRectMake(60, 200, 200, 44) type:TypeColorOrange];
        
        [sureBtn setTitle:@"修改" forState:UIControlStateNormal];
        [sureBtn setTag:ChangeBtnTypeSure];
        [sureBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:sureBtn];
        
        
        UIButton *backBtn = [LGUtil getBackBtn];
        [backBtn setCenter:CGPointMake(30, 20+STATUSBAR)];
        [backBtn setTag:ChangeBtnTypeCanel];
        [backBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backBtn];
    }
    return self;
}
-(void)btnClieck:(UIButton*)btn
{
    if (btn.tag ==ChangeBtnTypeSure && [_nameTF.text compare:@""] == 0) {
        //弹提示
        return;
    }
    [_delegate LGChangeView:self withType:btn.tag];
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_nameTF resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _nameTF) {
        [_nameTF resignFirstResponder];
    }
    return YES;
}

@end
