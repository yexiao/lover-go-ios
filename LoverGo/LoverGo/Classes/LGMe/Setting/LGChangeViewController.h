//
//  LGChangeNameViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-5-22.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGChangeView.h"
#import "LGSettingView.h"

@interface LGChangeViewController : UIViewController<LGChangeViewDelegate>
{
    LGChangeView* _cNameView;
    SettingItemType _sitemType;
}

-(id)initWithType:(SettingItemType)sitemType;
@end
