//
//  LGSettingItemController.m
//  LoverGo
//
//  Created by YeXiao on 14-5-21.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGDataCenter.h"
#import "LGUtilDefine.h"
#import "LGChangeViewController.h"
#import "LGSettingItemController.h"

@implementation LGSettingItemController
-(void)loadView
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginNof:) name:userUpDateSPath object:nil];
    _settingView = [[LGSettingView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _settingView.tableView.delegate = self;
    _settingView.tableView.dataSource = self;
    _settingView.delegate = self;
    [self setView:_settingView];
}
-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:userUpDateSPath name:nil object:self];
}
-(void)loginNof:(NSNotification *)nof
{
    if ([[nof name] compare:userUpDateSPath] == 0) {
        [self loadView];
    }
    
}
-(void)backLeft
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    LGUserInfo *info = [LGDataCenter getSingleton].userInfo;
    switch (indexPath.row) {
        case SettingItemTypeHead:
        {
            UILabel *_textLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 70, 60)];
            [_textLab setText:@"头像"];
            [cell addSubview:_textLab];
            
            UIImageView *bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 1, 58, 58)];
            [bgImgView setImage:[UIImage imageNamed:@"me_headicon_bg.png"]];
            [cell addSubview:bgImgView];
            bgImgView.userInteractionEnabled = YES;
            UIImageView *headimg = [[UIImageView alloc] initWithFrame:CGRectMake(0.5, 0.5, 57, 57)];
            [bgImgView addSubview:headimg];
            headimg.userInteractionEnabled = YES;
            
            if (info !=nil) {
                if ([info.headImg compare:@""] != 0) {
//                    NSURL *headUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",baseUrl,baseUploadImages,info.headImg]];
                    //加载头像
                }else
                {
                    if (info.sex == 0) {
                        [headimg setImage:[UIImage imageNamed:@"me_headicon_boy.png"]];
                    }else{
                        [headimg setImage:[UIImage imageNamed:@"me_headicon_girl.png"]];
                    }
                }
            }
            break;
        }
        case SettingItemTypeName:
        {
            UILabel *_textLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 200, 60)];
            if (info !=nil) {
                [_textLab setText:[NSString stringWithFormat:@"昵称:%@",info.userName]];
            }
            [cell addSubview:_textLab];
            break;
        }
        case SettingItemTypeSay:
        {
            UILabel *_textLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, SCREEN_WIDTH, 60)];
            _textLab.numberOfLines = 0;
            if (info !=nil) {
                [_textLab setText:[NSString stringWithFormat:@"个性签名:%@",info.say]];
            }
            [cell addSubview:_textLab];
            break;
        }
        case SettingItemTypeRegin:
        {
            UILabel *_textLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 200, 60)];
            if (info !=nil) {
                [_textLab setText:[NSString stringWithFormat:@"所在区域:%@",info.regionUuid]];
            }
            [cell addSubview:_textLab];
            break;
        }
        case SettingItemTypePwd:
        {
            UILabel *_textLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 200, 60)];
            if (info !=nil) {
                [_textLab setText:@"更改密码"];
            }
            [cell addSubview:_textLab];
            break;
        }
        case SettingItemTypeSex:
        {
            UILabel *_textLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 200, 60)];
            if (info !=nil) {
                NSString *sexStr = @"女";
                if (info.sex == 0){
                    sexStr = @"男";
                }
                [_textLab setText:[NSString stringWithFormat:@"性别:%@",sexStr]];
            }
            [cell addSubview:_textLab];
            break;
        }
        case SettingItemTypeFriend:
        {
            UILabel *_textLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 200, 60)];
            if (info !=nil) {
                if ([info.friendEmail compare:@""]){
                    [_textLab setText:[NSString stringWithFormat:@"好友账号:%@",info.friendEmail]];
                }else
                {
                    [_textLab setText:@"绑定好友账号"];
                }
            }
            [cell addSubview:_textLab];
            break;
        }
            
        default:
            break;
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        return 60.0f;
    }else
    {
        return 40;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        //修改头像
    }else{
        LGChangeViewController *vc = [[LGChangeViewController alloc]initWithType:indexPath.row];
        [self presentModalViewController:vc animated:YES];
    }
}
@end
