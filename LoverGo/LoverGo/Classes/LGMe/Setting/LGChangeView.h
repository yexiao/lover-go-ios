//
//  LGChangeView.h
//  LoverGo
//
//  Created by YeXiao on 14-5-22.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGSettingView.h"

typedef enum {
    ChangeBtnTypeCanel = 0,
    ChangeBtnTypeSure
}ChangeBtnType;

@class LGChangeView;

@protocol LGChangeViewDelegate <NSObject>

-(void)LGChangeView:(LGChangeView*)view withType:(ChangeBtnType)type;

@end


@interface LGChangeView : UIView<UITextFieldDelegate>
{
    SettingItemType _sItemType;
    UITextField* _nameTF;
    id<LGChangeViewDelegate> _delegate;
}
@property UITextField* nameTF;
@property id<LGChangeViewDelegate> delegate;

- (id)initWithFrame:(CGRect)frame type:(SettingItemType)type;

@end
