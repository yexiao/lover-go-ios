//
//  LGMeView.m
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGMeView.h"
#import "LGDataCenter.h"
#include "LGUtil.h"
#import "LGSignInView.h"

@implementation LGMeView

@synthesize meTView = _meTView;
@synthesize delegate = _delegate;

-(void)setDefaultLabel:(UILabel*)lab b:(BOOL)gray
{
    [lab setBackgroundColor:[UIColor whiteColor]];
    [lab setShadowColor:[UIColor whiteColor]];
    [lab setShadowOffset:CGSizeMake(0.1, 0.1)];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:18];
    [lab setFont:font];
    if (gray) {
        [lab setTextColor:[UIColor lightGrayColor]];
    }else{
        [lab setTextColor:[UIColor blackColor]];
    }
    [lab setTextAlignment:NSTextAlignmentLeft];
}
-(UILabel*)getLabelFrame:(CGRect)f title:(NSString*)title type:(LGSignInType)t
{
    UILabel* sysLab = [[UILabel alloc]initWithFrame:f];
    [self setDefaultLabel:sysLab b:YES];
    sysLab.userInteractionEnabled = true;
    [sysLab setText:title];
    UITapGestureRecognizer *tapGestureTel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelEvent:)];
    [sysLab addGestureRecognizer:tapGestureTel];
    tapGestureTel.view.tag = t;
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"next_pic.png"]];
    [imgView setCenter:CGPointMake(290, STATUSBAR)];
    [sysLab addSubview:imgView];
    return sysLab;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"个人中心"];
        [self addSubview:titleLab];
        
        _meTView = [[UITableView alloc] initWithFrame:CGRectMake(0, STATUSBAR + 40, frame.size.width, frame.size.height - 40) style:UITableViewStylePlain];
        _meTView.allowsSelection = YES;
        _meTView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [self addSubview:_meTView];
    }
    return self;
}

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        self.backgroundColor = [UIColor lightGrayColor];
//        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR +IOS7BAR, frame.size.width, 40) title:@"个人中心"];
//        [self addSubview:titleLab];
//        
//        //加头像
//        UIImageView *meIV= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cover_requests_avatar.png"]];
//        [meIV setFrame:CGRectMake(10, STATUSBAR + 51, 64, 64)];
//        [self addSubview:meIV];
//        //线条
//        UIImageView *lineIV= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash_binding.png"]];
//        [lineIV setFrame:CGRectMake(78, STATUSBAR + 71, 164, 5)];
//        [self addSubview:lineIV];
//        
//        UIImageView *otherIV= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cover_requests_avatar.png"]];
//        [otherIV setFrame:CGRectMake(SCREEN_WIDTH - 64 - 10, STATUSBAR + 51, 64, 64)];
//        [self addSubview:otherIV];
//        
//        UILabel* logoLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, STATUSBAR+123, SCREEN_WIDTH, 40)];
//        [self setDefaultLabel:logoLabel b:NO];
//        [logoLabel setText:@"情侣去哪儿账号"];
//        [self addSubview:logoLabel];
//        
//        UILabel* visitLabel =[self getLabelFrame:CGRectMake(0, STATUSBAR + 164, SCREEN_WIDTH, 40)title:@"个人信息" type:LGSignInTypeMe];
//        [self addSubview:visitLabel];
//        
//        UILabel* sysLab =[self getLabelFrame:CGRectMake(0, STATUSBAR + 205, SCREEN_WIDTH, 40) title:@"系统消息" type:LGSignInTypeSys];
//        [self addSubview:sysLab];
//        
//        
//        UILabel* helpLab =[self getLabelFrame:CGRectMake(0, STATUSBAR + 246, SCREEN_WIDTH, 40)title:@"帮助与反馈" type:LGSignInTypeHelp];
//        [self addSubview:helpLab];
//        
//        UILabel* aboutLab =[self getLabelFrame:CGRectMake(0, STATUSBAR + 287, SCREEN_WIDTH, 40)title:@"关于情侣去哪儿" type:LGSignInTypeAbout];
//        [self addSubview:aboutLab];
//        
//        UIButton *signOutBtn =[LGUtil get9ScalBtn:CGRectMake(10, SCREEN_HEIGHT - 70 - TABBAR, 300, 44) type:TypeColorGreen];
//        [signOutBtn setTitle:@"注销" forState:UIControlStateNormal];
//        [signOutBtn addTarget:self action:@selector(signOutBtnClieck) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:signOutBtn];
//
//        UILabel* notLab = [[UILabel alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 20 - TABBAR, 320, 20)];
//        [notLab setBackgroundColor:[UIColor clearColor]];
//        UIFont *font = [UIFont fontWithName:@"Helvetica" size:14];
//        [notLab setFont:font];
//        [notLab setTextColor:[UIColor blackColor]];
//        [notLab setTextAlignment:NSTextAlignmentCenter];
//        [notLab setText:@"情侣去哪儿,发现生活中的浪漫"];
//        [self addSubview:notLab];
//
//    }
//    return self;
//}
//-(void)signOutBtnClieck
//{
//    [_delegate LGMeViewSignOut];
//}
-(void)labelEvent:(UITapGestureRecognizer*)t
{
    [_delegate LGSignInViewBtnClieck:t.view.tag];
}

@end
