//
//  LGSignInView.h
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    LGSignInTypeBtnRegrister = 0,
    LGSignInTypeLogin,
    LGSignInTypeSys,
    LGSignInTypeHelp,
    LGSignInTypeAbout,
    LGSignInTypeMe
}LGSignInType;

@protocol LGSignInViewDelegate <NSObject>

-(void)LGSignInViewBtnClieck:(LGSignInType)type;

@end

@interface LGSignInView : UIView
{
    id<LGSignInViewDelegate> _delegate;
}
@property id<LGSignInViewDelegate> delegate;

-(void)singInClieck:(UIButton *)btn;
@end
