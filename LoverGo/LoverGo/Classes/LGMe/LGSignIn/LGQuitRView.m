//
//  LGQuitRView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-8.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "Toast+UIView.h"
#import "LGQuitRView.h"

@implementation LGQuitRView

@synthesize delegate = _delegate;
@synthesize pTF = _pTF;
@synthesize phoneTF = _phoneTF;
@synthesize phLabel = _phLabel;

-(void)setDefaultLabel:(UILabel*)lab color:(UIColor*)c havePic:(BOOL)b
{
    [lab setBackgroundColor:[UIColor clearColor]];
    [lab setShadowColor:[UIColor whiteColor]];
    [lab setShadowOffset:CGSizeMake(0.1, 0.1)];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:18];
    [lab setFont:font];
    [lab setTextColor:c];
    [lab setTextAlignment:NSTextAlignmentCenter];
    if (b) {
        UIImageView *picV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"next_pic.png"]];
        [picV setCenter:CGPointMake(105, 20)];
        [lab addSubview:picV];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _checkStrs = NULL;
        _pTF = NULL;
        self.backgroundColor = [UIColor lightGrayColor];
        
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"用户注册"];
        titleLab.userInteractionEnabled = true;
        [self addSubview:titleLab];
        //返回按钮
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:0];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        la1 = [[UILabel alloc] initWithFrame:CGRectMake(0, STATUSBAR + 40, 110, 40)];
        [self setDefaultLabel:la1 color:[UIColor greenColor] havePic:YES];
        [la1 setText:@"手机验证"];
        [self addSubview:la1];
        la2 = [[UILabel alloc] initWithFrame:CGRectMake(110, STATUSBAR + 40, 110, 40)];
        [self setDefaultLabel:la2 color:[UIColor blackColor] havePic:YES];
        [la2 setText:@"密码设置"];
        [self addSubview:la2];
        la3 = [[UILabel alloc] initWithFrame:CGRectMake(220, STATUSBAR + 40, 110, 40)];
        [self setDefaultLabel:la3 color:[UIColor blackColor] havePic:NO];
        [la3 setText:@"注册完成"];
        [self addSubview:la3];
        
        [self setFirstView];
        
    }
    return self;
}
-(void)setFirstView
{
    bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, STATUSBAR+90, 280, 40)];
    [bgImgView setImage:[LGUtil get9ScalImg:TypeImgBG]];
    [self addSubview:bgImgView];
    bgImgView.userInteractionEnabled = YES;
    
    nLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 40)];
    [nLabel setBackgroundColor:[UIColor clearColor]];
    [nLabel setText:@"手机号码:"];
    [bgImgView addSubview:nLabel];
    
    _phoneTF = [[UITextField alloc] initWithFrame:CGRectMake(85, 5, 190, 30)];
    [_phoneTF setBorderStyle:UITextBorderStyleBezel]; //外框类型
    _phoneTF.delegate = self;
    [_phoneTF becomeFirstResponder];
    [bgImgView addSubview:_phoneTF];
    
    
    _rBtn =[LGUtil get9ScalBtn:CGRectMake(10, STATUSBAR + 190, 300, 44) type:TypeColorOrange];
    [_rBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [_rBtn setTag:1];
    [_rBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_rBtn];
}
-(void)setSecondView:(NSString*)checkStr
{
    _checkStrs = checkStr;
    UILabel *phonlab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 85, 40)];
    [phonlab setBackgroundColor:[UIColor clearColor]];
    [phonlab setText:@"手机号:"];
    [bgImgView addSubview:phonlab];
    
    _phLabel = [[UILabel alloc]initWithFrame:CGRectMake(85, 0, 280, 40)];
    [_phLabel setBackgroundColor:[UIColor clearColor]];
    [_phLabel setText:[_phoneTF text]];
    [bgImgView addSubview:_phLabel];
    
    [_phoneTF setFrame:CGRectMake(85, 40, 190, 30)];
    [_phoneTF setText:@""];
    [self setDefaultLabel:la2 color:[UIColor greenColor] havePic:NO];
    [_rBtn setTitle:@"注册" forState:UIControlStateNormal];
    [_rBtn setTag:2];
    [_rBtn setFrame:CGRectMake(10, STATUSBAR + 240, 300, 44)];
    [bgImgView setFrame:CGRectMake(20, STATUSBAR+90, 280, 120)];
    [nLabel setText:@"验证码:"];
    [nLabel setFrame:CGRectMake(0, 35, 80, 40)];
    
    UILabel *pLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, 80, 40)];
    [pLabel setBackgroundColor:[UIColor clearColor]];
    [pLabel setText:@"设置密码:"];
    [bgImgView addSubview:pLabel];
    
    _pTF = [[UITextField alloc] initWithFrame:CGRectMake(85, 85, 190, 30)];
    [_pTF setBorderStyle:UITextBorderStyleBezel]; //外框类型
    _pTF.secureTextEntry = YES; //密码
    _pTF.delegate = self;
    [_phoneTF becomeFirstResponder];
    [bgImgView addSubview:_pTF];
}
-(void)btnClieck:(UIButton*)sender
{
    if (sender.tag == 2) {//注册，验证填验证码对了么
        if ([[_phoneTF text] compare:@""] != 0 && [[_phoneTF text] compare:_checkStrs] == 0) {
            if ([[_pTF text] compare:@""] == 0) {
                [self makeToast:@"请输入初始密码!"];
                return;
            }
        }
    }else if (sender.tag == 1){
        if ([[_phoneTF text] compare:@""] == 0) {
            [self makeToast:@"请输入手机号!"];
            return;
        }
        if ([LGUtil judgeNumber:_phoneTF.text] == NO) {
            [self makeToast:@"请填写正确手机号码"];
            return;
        }
        //判断手机号格式
    }
    [_delegate QuiRView:sender.tag];
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_phoneTF resignFirstResponder];
    [_pTF resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _phoneTF) {
        if (_pTF) {
            [_pTF becomeFirstResponder];
        }
        [_phoneTF resignFirstResponder];
    }if (textField == _pTF) {
        [_pTF resignFirstResponder];
    }
    return YES;
}

@end
