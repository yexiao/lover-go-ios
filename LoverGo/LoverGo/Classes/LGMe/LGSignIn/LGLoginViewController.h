//
//  LGLoginViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGLoginView.h"

@interface LGLoginViewController : UIViewController<LGLoginViewDelegate>
{
    LGLoginView* _loginView;
}
@end
