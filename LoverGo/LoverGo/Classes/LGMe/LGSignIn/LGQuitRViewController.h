//
//  LGQuitRViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-8.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGQuitRView.h"

@interface LGQuitRViewController : UIViewController<LGQuitRViewDelegate>
{
    
    LGQuitRView* _qView;
}

@end
