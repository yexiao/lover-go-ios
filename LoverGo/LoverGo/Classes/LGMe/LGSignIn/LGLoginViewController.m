//
//  LGLoginViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUserInfo.h"
#import "LGDataCenter.h"
#import "Toast+UIView.h"
#import "LGRequestManager.h"
#import "LGQuitRViewController.h"
#import "LGLoginViewController.h"

@interface LGLoginViewController ()

@end

@implementation LGLoginViewController

-(void)loadView
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSucc:) name:loginSPath object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSucc:) name:registerSPath object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSucc:) name:ErrorNotification object:nil];
    _loginView = [[LGLoginView alloc] initWithFrame:CGRectMake(0, STATUSBAR +TOOLBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR +TOOLBAR))];
    _loginView.delegate = self;
    
    [self setView:_loginView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:loginSPath name:nil object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:registerSPath name:nil object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:ErrorNotification name:nil object:self];
}
-(void)LGLoginView:(LGLoginView *)view withType:(LGLoginType)type
{
    if (type == LGLoginTypeSub) {//登陆
        
        NSUserDefaults *uDef = [NSUserDefaults standardUserDefaults];
        [uDef setObject:[NSString stringWithFormat:@"%@",[view.nameTF text]] forKey:@"UserEmail"];
        [uDef setObject:[NSString stringWithFormat:@"%@",[view.passwordTF text]] forKey:@"UserPwd"];
        
        NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                              [view.nameTF text],@"appUser.email",
                              [view.passwordTF text],@"appUser.password",
                              nil];
        [[LGRequestManager getSingleton] sendRequestAboutUser:loginSPath para:para];
    }
    if (type == LGLoginTypeBack) {
        [self.navigationController popViewControllerAnimated:YES];
    }else if (type == LGLoginTypeRegister)
    {
        LGQuitRViewController* vc = [[LGQuitRViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
-(void)loginSucc:(NSNotification *)nof
{
    if ([[nof name] compare:loginSPath] == 0 
//        [[nof name] compare:registerSPath] == 0
        ) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    if ([[nof name] compare:ErrorNotification] == 0 ) {
        [self.view makeToast:@"登录失败，请检查账号密码"];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
