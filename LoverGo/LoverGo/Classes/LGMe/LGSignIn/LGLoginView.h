//
//  LGLoginView.h
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    LGLoginTypeBack = 0,
    LGLoginTypeSub,
    LGLoginTypeRegister
}LGLoginType;

@class LGLoginView;

@protocol LGLoginViewDelegate <NSObject>

-(void)LGLoginView:(LGLoginView*)view withType:(LGLoginType)type;

@end

@interface LGLoginView : UIControl<UITextFieldDelegate>
{
    UITextField* _nameTF;
    UITextField* _passwordTF;
    id<LGLoginViewDelegate> _delegate;
}
@property UITextField* nameTF;
@property UITextField* passwordTF;
@property id<LGLoginViewDelegate> delegate;

@end
