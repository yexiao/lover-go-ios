//
//  LGSignInView.m
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGSignInView.h"
#include "LGUtil.h"

@implementation LGSignInView

@synthesize delegate = _delegate;

-(void)setDefaultLabel:(UILabel*)lab b:(BOOL)gray
{
    [lab setBackgroundColor:[UIColor whiteColor]];
    [lab setShadowColor:[UIColor whiteColor]];
    [lab setShadowOffset:CGSizeMake(0.1, 0.1)];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:18];
    [lab setFont:font];
    if (gray) {
        [lab setTextColor:[UIColor lightGrayColor]];
    }else{
        [lab setTextColor:[UIColor blackColor]];
    }
    [lab setTextAlignment:NSTextAlignmentLeft];
}
-(UILabel*)getLabelFrame:(CGRect)f title:(NSString*)title type:(LGSignInType)t
{
    UILabel* sysLab = [[UILabel alloc]initWithFrame:f];
    [self setDefaultLabel:sysLab b:YES];
    sysLab.userInteractionEnabled = true;
    [sysLab setText:title];
    UITapGestureRecognizer *tapGestureTel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelEvent:)];
    [sysLab addGestureRecognizer:tapGestureTel];
    tapGestureTel.view.tag = t;
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"next_pic.png"]];
    [imgView setCenter:CGPointMake(290, STATUSBAR)];
    [sysLab addSubview:imgView];
    return sysLab;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
        
        UILabel* logoLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, STATUSBAR, SCREEN_WIDTH, 40)];
        [self setDefaultLabel:logoLabel b:NO];
        [logoLabel setText:@"情侣去哪儿账号"];
        [self addSubview:logoLabel];
        UILabel* visitLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, STATUSBAR + 41, SCREEN_WIDTH, 40)];
        [self setDefaultLabel:visitLabel b:YES];
        [visitLabel setText:@"个人信息(访客)"];
        [self addSubview:visitLabel];
        
        UIButton *loginBtn =[LGUtil get9ScalBtn:CGRectMake(10, STATUSBAR + 90, 300, 44) type:TypeColorOrange];
        
        [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
        [loginBtn setTag:LGSignInTypeLogin];
        [loginBtn addTarget:self action:@selector(singInClieck:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:loginBtn];
        
        UILabel* sysLab =[self getLabelFrame:CGRectMake(0, STATUSBAR + 140, SCREEN_WIDTH, 40) title:@"系统消息" type:LGSignInTypeSys];
        [self addSubview:sysLab];
        
        
        UILabel* helpLab =[self getLabelFrame:CGRectMake(0, STATUSBAR + 181, SCREEN_WIDTH, 40)title:@"帮助与反馈" type:LGSignInTypeHelp];
        [self addSubview:helpLab];
        
        UILabel* aboutLab =[self getLabelFrame:CGRectMake(0, STATUSBAR + 222, SCREEN_WIDTH, 40)title:@"关于情侣去哪儿" type:LGSignInTypeAbout];
        [self addSubview:aboutLab];
        
    }
    return self;
}
-(void)labelEvent:(UITapGestureRecognizer*)t
{
    [_delegate LGSignInViewBtnClieck:t.view.tag];
}
-(void)singInClieck:(UIButton *)btn
{
    [_delegate LGSignInViewBtnClieck:btn.tag];
}

@end
