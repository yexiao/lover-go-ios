//
//  LGRegisterViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGDataCenter.h"
#import "LGRequestManager.h"
#import "LGRegisterViewController.h"

@interface LGRegisterViewController ()

@end

@implementation LGRegisterViewController

-(void)loadView
{
    _rView = [[LGRegisterView alloc] initWithFrame:CGRectMake(0, STATUSBAR +TOOLBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR +TOOLBAR))];
    _rView.delegate = self;
    
    [self setView:_rView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RegisterNof:) name:registerSPath object:nil];

	// Do any additional setup after loading the view.
}
-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:registerSPath name:nil object:self];
}

-(void)LGRegisterView:(LGRegisterView *)view withType:(LGRegisterType)type
{
    if (type == LGRegisterTypeSub) {
        //注册
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setValue:[view.nameTF text] forKey:@"appUser.userName"];
        [dic setValue:[view.passwordTF text] forKey:@"appUser.password"];
        [dic setValue:[[LGDataCenter getSingleton] getPlatform] forKey:@"appUser.mobile"];
        
        if ([[view.emailTF text] compare:@""] != 0) {
            [dic setValue:[view.emailTF text] forKey:@"appUser.email"];
        }
        [[LGRequestManager getSingleton] sendRequestAboutUser:registerSPath para:dic];
    }
}

-(void)RegisterNof:(NSNotification*)nof
{
    if ([[nof name] compare:registerSPath] == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
