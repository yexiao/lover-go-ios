//
//  LGQuitRViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-8.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "SVHTTPClient.h"
#import "LGUtilDefine.h"
#import "Toast+UIView.h"
#import "LGDataCenter.h"
#import "LGRequestManager.h"
#import "LGQuitRViewController.h"

@interface LGQuitRViewController ()

@end

@implementation LGQuitRViewController

-(void)loadView
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RegisterNof:) name:registerSPath object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RegisterNof:) name:ErrorNotification object:nil];
    
    _qView = [[LGQuitRView alloc] initWithFrame:CGRectMake(0, STATUSBAR +TOOLBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR +TOOLBAR))];
    _qView.delegate = self;
    
    [self setView:_qView];
}
-(void)QuiRView:(int)type
{
    switch (type) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
        {
            //判断
            if ([[_qView.phoneTF text] compare:@""] == 0) {
                [self.view makeToast:@"请输入手机号"];
                return;
            }
            __block LGQuitRViewController *_blockSelf = self;
            NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%@",[_qView.phoneTF text]] ,@"appUser.email",
                                  @"other" ,@"doWhat",
                                  nil];
            [[SVHTTPClient sharedClient] GET:findCheckNumByTelSPath
                                  parameters:para
                                  completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                                      if (error) {
                                          [_blockSelf.view makeToast:@"网络连接失败"];
                                          return ;
                                      }
                                      if (response == nil) {
                                          [_blockSelf.view makeToast:@"请求失败"];
                                          return;
                                      }
                                      NSError *jsonError = nil;
                                      id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
                                      if (jsonError) {
                                          [_blockSelf.view makeToast:@"解析错误"];
                                      }else{
                                          NSString *cheNum = [jsonObject objectForKey:@"checkNum"];
                                          if (cheNum) {
                                              [_blockSelf->_qView setSecondView:cheNum];
                                          }else{
                                              [_blockSelf.view makeToast:@"返回错误"];
                                          }
                                      }
                                  }];
            break;
        }
        case 2:{
            if ([[_qView.phoneTF text] compare:@""] == 0) {
                [self.view makeToast:@"请输入验证码"];
                return;
            }
            if ([[_qView.pTF text] compare:@""] == 0) {
                [self.view makeToast:@"请输入密码"];
                return;
            }
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setValue:[_qView.phLabel text] forKey:@"appUser.email"];
            [dic setValue:[_qView.pTF text] forKey:@"appUser.password"];
            [[LGRequestManager getSingleton] sendRequestAboutUser:registerSPath para:dic];
            break;
        }
        default:
            break;
    }
}
-(void)RegisterNof:(NSNotification*)nof
{
    if ([[nof name] compare:registerSPath] == 0) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    if ([[nof name] compare:ErrorNotification] == 0 ) {
        [self.view makeToast:@"注册失败，该手机号已经注册"];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:registerSPath name:nil object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:ErrorNotification name:nil object:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
