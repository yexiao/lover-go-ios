//
//  LGQuitRView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-8.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGQuitRViewDelegate <NSObject>

-(void)QuiRView:(int)type;

@end

@interface LGQuitRView : UIView<UITextFieldDelegate>
{
    UITextField* _phoneTF;
    UITextField* _pTF;
    id<LGQuitRViewDelegate> _delegate;
    UILabel *la1;
    UILabel *la2;
    UILabel *la3;
    UIButton *_rBtn;
    NSString* _checkStrs;
    UIImageView *bgImgView;
    UILabel* nLabel;
    UILabel* _phLabel;
}
@property(nonatomic,strong)UILabel* phLabel;
@property(nonatomic,strong)UITextField* phoneTF;
@property(nonatomic,strong)UITextField* pTF;
@property(nonatomic,strong)id<LGQuitRViewDelegate> delegate;

-(void)setSecondView:(NSString*)checkStr;
@end
