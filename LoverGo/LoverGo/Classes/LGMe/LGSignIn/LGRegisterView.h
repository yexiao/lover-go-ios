//
//  LGRegisterView.h
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    LGRegisterTypeCanel = 0,
    LGRegisterTypeSub
}LGRegisterType;

@class LGRegisterView;

@protocol LGRegisterViewDelegate <NSObject>

-(void)LGRegisterView:(LGRegisterView*)view withType:(LGRegisterType)type;

@end

@interface LGRegisterView : UIControl<UITextFieldDelegate>
{
    UITextField* _nameTF;
    UITextField* _passwordTF;
    UITextField* _emailTF;
    id<LGRegisterViewDelegate> _delegate;
    
}
@property UITextField* nameTF;
@property UITextField* passwordTF;
@property UITextField* emailTF;
@property id<LGRegisterViewDelegate> delegate;

-(void)btnClieck:(UIButton*)btn;

@end
