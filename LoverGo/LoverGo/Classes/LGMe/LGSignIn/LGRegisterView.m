//
//  LGRegisterView.m
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGRegisterView.h"
#include "LGUtil.h"

@implementation LGRegisterView

@synthesize nameTF = _nameTF;
@synthesize passwordTF = _passwordTF;
@synthesize emailTF = _emailTF;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        UIImageView *bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 60, 280, 150)];
        [bgImgView setImage:[LGUtil get9ScalImg:TypeImgBG]];
        [self addSubview:bgImgView];
        bgImgView.userInteractionEnabled = YES;
        
        UILabel* nLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 50, 40)];
        [nLabel setBackgroundColor:[UIColor clearColor]];
        [nLabel setText:@"账号:"];
        [bgImgView addSubview:nLabel];
        
        _nameTF = [[UITextField alloc] initWithFrame:CGRectMake(50, 15, 220, 30)];
        [_nameTF setBorderStyle:UITextBorderStyleBezel]; //外框类型
        _nameTF.delegate = self;
        [_nameTF becomeFirstResponder];
        [bgImgView addSubview:_nameTF];
        
        UILabel* pLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, 50, 40)];
        [pLabel setBackgroundColor:[UIColor clearColor]];
        [pLabel setText:@"密码:"];
        [bgImgView addSubview:pLabel];
        
        _passwordTF = [[UITextField alloc] initWithFrame:CGRectMake(50, 55, 220, 30)];
        [_passwordTF setBorderStyle:UITextBorderStyleBezel]; //外框类型
        _passwordTF.delegate = self;
        _passwordTF.secureTextEntry = YES; //密码
        [bgImgView addSubview:_passwordTF];
        
        UILabel* eLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 90, 50, 40)];
        [eLabel setBackgroundColor:[UIColor clearColor]];
        [eLabel setText:@"邮箱:"];
        [bgImgView addSubview:eLabel];
        
        _emailTF = [[UITextField alloc] initWithFrame:CGRectMake(50, 95, 220, 30)];
        [_emailTF setBorderStyle:UITextBorderStyleBezel]; //外框类型
        _emailTF.delegate = self;
        [bgImgView addSubview:_emailTF];
        
        UIButton *regBtn =[LGUtil get9ScalBtn:CGRectMake(60, 230, 200, 44) type:TypeColorOrange];
        
        [regBtn setTitle:@"注册" forState:UIControlStateNormal];
        [regBtn setTag:LGRegisterTypeSub];
        [regBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:regBtn];
        
        
        UIButton *backBtn = [LGUtil getBackBtn];
        [backBtn setTag:LGRegisterTypeCanel];
        [backBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backBtn];
    }
    return self;
}
-(void)btnClieck:(UIButton*)btn
{
    if (btn.tag == LGRegisterTypeSub) {
        //_emailTF可以不需要填
        if ([_nameTF.text compare:@""] == 0 || [_passwordTF.text compare:@""] == 0) {
            return;
        }
    }
    [_delegate LGRegisterView:self withType:(LGRegisterType)btn.tag];
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_nameTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    [_emailTF resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _nameTF) {
        [_passwordTF becomeFirstResponder];
    }
    if (textField == _passwordTF) {
        [_emailTF becomeFirstResponder];
    }
    if (textField == _emailTF) {
        [_emailTF resignFirstResponder];
    }
    
    return YES;
}

@end
