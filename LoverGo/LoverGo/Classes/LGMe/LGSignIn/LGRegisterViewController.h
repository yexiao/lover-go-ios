//
//  LGRegisterViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGRegisterView.h"

@interface LGRegisterViewController : UIViewController<LGRegisterViewDelegate>
{
    LGRegisterView *_rView;
}

@end
