//
//  LGLoginView.m
//  LoverGo
//
//  Created by YeXiao on 14-1-18.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGLoginView.h"
#import "Toast+UIView.h"
#include "LGUtil.h"

@implementation LGLoginView

@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"用户登录"];
        titleLab.userInteractionEnabled = true;
        [self addSubview:titleLab];
        //返回按钮
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:LGLoginTypeBack];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        
        UIImageView *bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, STATUSBAR+60, 280, 100)];
        [bgImgView setImage:[LGUtil get9ScalImg:TypeImgBG]];
        [self addSubview:bgImgView];
        bgImgView.userInteractionEnabled = YES;
        
        UILabel* nLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 50, 40)];
        [nLabel setBackgroundColor:[UIColor clearColor]];
        [nLabel setText:@"账号:"];
        [bgImgView addSubview:nLabel];
        
        _nameTF = [[UITextField alloc] initWithFrame:CGRectMake(50, 15, 220, 30)];
        [_nameTF setBorderStyle:UITextBorderStyleBezel]; //外框类型
        _nameTF.delegate = self;
        [_nameTF becomeFirstResponder];
        [bgImgView addSubview:_nameTF];
        
        UILabel* pLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, 50, 40)];
        [pLabel setBackgroundColor:[UIColor clearColor]];
        [pLabel setText:@"密码:"];
        [bgImgView addSubview:pLabel];
        
        _passwordTF = [[UITextField alloc] initWithFrame:CGRectMake(50, 55, 220, 30)];
        [_passwordTF setBorderStyle:UITextBorderStyleBezel]; //外框类型
        _passwordTF.delegate = self;
        _passwordTF.secureTextEntry = YES; //密码
        [bgImgView addSubview:_passwordTF];
        
        UIButton *subBtn =[LGUtil get9ScalBtn:CGRectMake(170, STATUSBAR+170, 140, 44) type:TypeColorOrange];
        
        [subBtn setTitle:@"登陆" forState:UIControlStateNormal];
        [subBtn setTag:LGLoginTypeSub];
        [subBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:subBtn];
        UIButton *regBtn =[LGUtil get9ScalBtn:CGRectMake(20, STATUSBAR+170, 140, 44) type:TypeColorGreen];
        
        [regBtn setTitle:@"快捷注册" forState:UIControlStateNormal];
        [regBtn setTag:LGLoginTypeRegister];
        [regBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:regBtn];
    }
    return self;
}
-(void)btnClieck:(UIButton*)btn
{
    if (btn.tag == LGLoginTypeSub) {
        if ([_nameTF.text compare:@""] == 0 || [_passwordTF.text compare:@""] == 0) {
            [self makeToast:@"请输入账号和密码"];
            return;
        }
        if ([LGUtil judgeNumber:_nameTF.text] == NO) {
            [self makeToast:@"请填写正确手机号码"];
            return;
        }
    }
    [_nameTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    [_delegate LGLoginView:self withType:(LGLoginType)btn.tag];
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_nameTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _nameTF) {
        [_passwordTF becomeFirstResponder];
    }
    if (textField == _passwordTF) {
        [_passwordTF resignFirstResponder];
    }
    return YES;
}

@end
