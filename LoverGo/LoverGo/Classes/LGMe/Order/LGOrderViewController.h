//
//  LGOrderViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-23.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGOrderView.h"
#import "LXActionSheet.h"
#import "LGLGOrderInfo.h"

@interface LGOrderViewController : UIViewController<UIWebViewDelegate,LGOrderViewDelegate>
{
    LGOrderView *_aView;
    LGLGOrderInfo* _info;
}
-(id)initWithOrderInfo:(LGLGOrderInfo*)info;

@end
