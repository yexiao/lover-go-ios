//
//  HavePayViewController.h
//  LoverGo
//
//  Created by YeXiao on 13-6-25.
//  Copyright (c) 2013年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HavePayView.h"
#import "LGUtilModel.h"

@interface HavePayViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    HavePayView *_hpView;
    NSMutableArray* _roadArray;
    LGUtilModel *_utilModel;
}

-(void)loadFirst;
-(void)refreshView:(id)response page:(int)page;
@end
