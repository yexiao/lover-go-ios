//
//  HavePayViewController.m
//  LoverGo
//
//  Created by YeXiao on 13-6-25.
//  Copyright (c) 2013年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "MJExtension.h"
#import "PayItemView.h"
#import "Toast+UIView.h"
#import "LGRequestManager.h"
#import "LGLGOrderInfo.h"
#import "LGDataCenter.h"
#import "LGUtilDefine.h"
#import "OrderDetailViewController.h"
#import "HavePayViewController.h"

@interface HavePayViewController ()

@end

@implementation HavePayViewController
-(void)loadView
{
    _roadArray = [[NSMutableArray alloc] init];
    _hpView = [[HavePayView alloc] initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH, SCREEN_HEIGHT-90 - STATUSBAR)];
    _hpView.tableView.dataSource = self;
    _hpView.tableView.delegate = self;
    [self setView:_hpView];
    _utilModel = nil;
}
-(void)loadFirst
{
    __block HavePayViewController *_blockSelf = self;
    _hpView.header.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //发送初始请求
        [_blockSelf sendRequestByPage:1];
    };
    [_hpView.header beginRefreshing];
    
    _hpView.footer.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //进入刷新状态
        if (_blockSelf->_utilModel) {
            int nextPage = [_blockSelf->_utilModel.nowPage intValue] +1;
            if (nextPage > [_blockSelf->_utilModel.totalPage intValue]) {
                [_blockSelf setDefaultFresh];
                [_blockSelf.view makeToast:@"数据已经是最新的啦!"];
                return ;
            }
            [_blockSelf sendRequestByPage:nextPage];
        }else{
            [_blockSelf setDefaultFresh];
            [_blockSelf.view makeToast:@"请先登录!"];
        }
    };
}

-(void)setDefaultFresh{
    [_hpView.footer endRefreshing];
    [_hpView.header endRefreshing];
}
-(void)refreshView:(id)response page:(int)page
{
    [self setDefaultFresh];
    if (response == nil) {
        [self.view makeToast:@"加载失败，没有请求到数据!"];
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        [self.view makeToast:@"解析失败!"];
    }else{
        if (page == 1) {
            [_roadArray removeAllObjects];
        }
        _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        [_roadArray addObjectsFromArray:[LGLGOrderInfo objectArrayWithKeyValuesArray:_utilModel.listData]];
        [_hpView.tableView reloadData];//刷新数据
        
    }
}
-(void)sendRequestByPage:(int)page
{
    LGUserInfo  *uInfo= [LGDataCenter getSingleton].userInfo;
    if (uInfo == NULL) {
        //用户未设置数据，请跳转至数据界面
        [self setDefaultFresh];
        [self.view makeToast:@"请先登录哦!"];
        return;
    }
    __block HavePayViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          uInfo.uuid,@"appUserUuid",
                          [NSString stringWithFormat:@"%d",page],@"page",
                          nil];
    [[SVHTTPClient sharedClient] GET:findPayAOrderSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  //抛报错通知
                                  [self setDefaultFresh];
                                  [_blockSelf.view makeToast:@"网络连接失败，请检查网络!"];
                              }
                              [_blockSelf refreshView:response page:page];
                          }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGLGOrderInfo *ccInfo= [_roadArray objectAtIndex:indexPath.row];
    
    OrderDetailViewController *vc = [[OrderDetailViewController alloc] initWithOrder:ccInfo];
    [[LGUtil getControllerByView:self.view].navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _roadArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PayItemView * cell = [[PayItemView alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    LGLGOrderInfo *oInfo= [_roadArray objectAtIndex:indexPath.row];
    [cell reloadRoadData:oInfo];
    return cell;
}

@end
