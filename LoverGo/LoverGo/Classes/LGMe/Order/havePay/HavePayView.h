//
//  HavePayView.h
//  LoverGo
//
//  Created by YeXiao on 13-6-25.
//  Copyright (c) 2013年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"


@interface HavePayView : UIView
{
    MJRefreshHeaderView *_header;
    MJRefreshFooterView *_footer;
    UITableView* _tableView;
}

@property(nonatomic,strong)MJRefreshHeaderView *header;
@property(nonatomic,strong)MJRefreshFooterView *footer;
@property(nonatomic,strong)UITableView* tableView;
@end
