//
//  OrderDetailViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-29.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailView.h"
#import "LGLGOrderInfo.h"

@interface OrderDetailViewController : UIViewController<OrderDetailViewDelegate>
{
    OrderDetailView* _odView;
    LGLGOrderInfo* _info;
}

-(id)initWithOrder:(LGLGOrderInfo*)info;

@end
