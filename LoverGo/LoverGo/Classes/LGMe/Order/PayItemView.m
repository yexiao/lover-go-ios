//
//  PayItemView.m
//  LoverGo
//
//  Created by YeXiao on 13-6-25.
//  Copyright (c) 2013年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "PayItemView.h"
#import "LGUtilDefine.h"
#import "LGDataCenter.h"

@implementation PayItemView

-(void)setDefauleLabel:(UILabel*)lab
{
    [lab setBackgroundColor:[UIColor clearColor]];
//    [lab setShadowColor:[UIColor grayColor]];
    [lab setFont:[UIFont fontWithName:@"Helvetica" size:18]];
//    [lab setShadowOffset:CGSizeMake(0.05, 0.05)];
    [lab setTextColor:[UIColor blackColor]];
    [lab setTextAlignment:NSTextAlignmentLeft];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setDefauleLabel:_costLab];
        [self addSubview:_costLab];
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 4, 260, 25)];
        [self setDefauleLabel:_titleLab];
        [_titleLab setShadowOffset:CGSizeMake(0.5, 0.5)];
        [self addSubview:_titleLab];
        
        _phoneLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 38, 160, 25)];
        [self setDefauleLabel:_phoneLab];
        [self addSubview:_phoneLab];
        
        _costLab= [[UILabel alloc] initWithFrame:CGRectMake(261, 22, 60, 25)];
        [self setDefauleLabel:_costLab];
        [_costLab setTextAlignment:NSTextAlignmentRight];
        [self addSubview:_costLab];
    }
    return self;
}
-(void)reloadRoadData:(LGLGOrderInfo*)info
{
    [_phoneLab setText:info.appUserEmail];
    [_titleLab setText:info.orderName];
    if (info.totalPrice) {
        [_costLab setText:[NSString stringWithFormat:@"%.2f",ceilf([info.totalPrice floatValue]*100)/100]];
    }else{
        [_costLab setText:[NSString stringWithFormat:@"0"]];
    }
}
-(void)removeAll
{
    [_phoneLab setText:NULL];
    [_titleLab setText:NULL];
    [_costLab setText:NULL];
}
@end
