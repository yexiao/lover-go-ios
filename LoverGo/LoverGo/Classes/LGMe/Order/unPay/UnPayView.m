//
//  UnPayView.m
//  LoverGo
//
//  Created by YeXiao on 13-6-25.
//  Copyright (c) 2013年 yexiao. All rights reserved.
//

#import "UnPayView.h"

@implementation UnPayView

@synthesize tableView = _tableView;
@synthesize header = _header;
@synthesize footer = _footer;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _tableView.backgroundColor = [UIColor whiteColor];
        _footer = [MJRefreshFooterView footer];
        _footer.scrollView = _tableView;
        _header = [MJRefreshHeaderView header];
        _header.scrollView = _tableView;
        [self addSubview:_tableView];
    }
    return self;
}


@end
