//
//  UnPayViewController.h
//  LoverGo
//
//  Created by YeXiao on 13-6-25.
//  Copyright (c) 2013年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnPayView.h"
#import "LGUtilModel.h"

@interface UnPayViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UnPayView *_upView;
    NSMutableArray* _roadArray;
    LGUtilModel *_utilModel;
}

-(void)refreshView:(id)response page:(int)page;
@end
