//
//  UnPayViewController.m
//  LoverGo
//
//  Created by YeXiao on 13-6-25.
//  Copyright (c) 2013年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "MJExtension.h"
#import "PayItemView.h"
#import "Toast+UIView.h"
#import "LGRequestManager.h"
#import "LGLGOrderInfo.h"
#import "LGDataCenter.h"
#import "LGUtilDefine.h"
#import "OrderDetailViewController.h"
#import "UnPayViewController.h"

@interface UnPayViewController ()

@end

@implementation UnPayViewController
-(void)loadView
{
    _roadArray = [[NSMutableArray alloc] init];
    _upView = [[UnPayView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-90)];
    _upView.tableView.dataSource = self;
    _upView.tableView.delegate = self;
    [self setView:_upView];
    _utilModel = nil;
    
    __block UnPayViewController *_blockSelf = self;
    _upView.header.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //发送初始请求
        [_blockSelf sendRequestByPage:1];
    };
    [_upView.header beginRefreshing];
    
    _upView.footer.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //进入刷新状态
        if (_blockSelf->_utilModel) {
            int nextPage = [_blockSelf->_utilModel.nowPage intValue] +1;
            if (nextPage > [_blockSelf->_utilModel.totalPage intValue]) {
                [_blockSelf setDefaultFresh];
                [_blockSelf.view makeToast:@"数据已经是最新的啦!"];
                return ;
            }
            [_blockSelf sendRequestByPage:nextPage];
        }else{
            [_blockSelf setDefaultFresh];
            [_blockSelf.view makeToast:@"请先登录!"];
        }
    };
}

-(void)setDefaultFresh{
    [_upView.footer endRefreshing];
    [_upView.header endRefreshing];
}
-(void)refreshView:(id)response page:(int)page
{
    [self setDefaultFresh];
    if (response == nil) {
        [self.view makeToast:@"加载失败，没有请求到数据!"];
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        [self.view makeToast:@"解析失败!"];
    }else{
        if (page == 1) {
            [_roadArray removeAllObjects];
        }
        _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        [_roadArray addObjectsFromArray:[LGLGOrderInfo objectArrayWithKeyValuesArray:_utilModel.listData]];
        [_upView.tableView reloadData];//刷新数据
        
    }
}
-(void)sendRequestByPage:(int)page
{
    LGUserInfo  *uInfo= [LGDataCenter getSingleton].userInfo;
    if (uInfo == NULL) {
        //用户未设置数据，请跳转至数据界面
        [self setDefaultFresh];
        [self.view makeToast:@"请先登录哦!"];
        return;
    }
    __block UnPayViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          uInfo.uuid,@"appUserUuid",
                          [NSString stringWithFormat:@"%d",page],@"page",
                          nil];
    [[SVHTTPClient sharedClient] GET:findUnPayAOrderSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  //抛报错通知
                                  [self setDefaultFresh];
                                  [_blockSelf.view makeToast:@"网络连接失败，请检查网络!"];
                              }
                              [_blockSelf refreshView:response page:page];
                          }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGLGOrderInfo *ccInfo= [_roadArray objectAtIndex:indexPath.row];

    OrderDetailViewController *vc = [[OrderDetailViewController alloc] initWithOrder:ccInfo];
    [[LGUtil getControllerByView:self.view].navigationController pushViewController:vc animated:YES];

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _roadArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * showUserInfoCellIdentifier = @"PayItemView";
    PayItemView * cell = [tableView dequeueReusableCellWithIdentifier:showUserInfoCellIdentifier];
    if (cell == nil)
    {
        cell = [[PayItemView alloc] init];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    LGLGOrderInfo *oInfo= [_roadArray objectAtIndex:indexPath.row];
    [cell reloadRoadData:oInfo];
    return cell;
}
@end
