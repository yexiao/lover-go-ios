//
//  ALLOrderViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "ALLOrderViewController.h"

@interface ALLOrderViewController ()

@end

@implementation ALLOrderViewController

-(void)loadView
{
    _oView = [[ALLOrderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _oView.slideSwitchView.slideSwitchViewDelegate = self;
    _oView.delegate = self;
    
    _vc1 =[[UnPayViewController alloc] init];
    _vc1.title = @"待付款";
    _vc2 = [[HavePayViewController alloc] init];
    _vc2.title = @"已付款";
    
    
    [_oView.slideSwitchView buildUI];
    
    [self setView:_oView];
}
- (NSUInteger)numberOfTab:(QCSlideSwitchView *)view
{
    return 2;
}

- (UIViewController *)slideSwitchView:(QCSlideSwitchView *)view viewOfTab:(NSUInteger)number
{
    switch (number) {
        case 0:
            return _vc1;
            break;
        case 1:
            return _vc2;
            break;
        default:
            break;
    }
    return NULL;
}
- (void)slideSwitchView:(QCSlideSwitchView *)view didselectTab:(NSUInteger)number
{
    UIViewController *vController;
    switch (number) {
        case 0:
            vController = _vc1;
            break;
        case 1:
            [_vc2 loadFirst];
            vController = _vc2;
            break;
        default:
            break;
    }
}

-(void)turnBackLeft
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
