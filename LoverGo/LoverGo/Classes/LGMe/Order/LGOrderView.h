//
//  LGOrderView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-23.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGOrderViewDelegate <NSObject>

-(void)turnBackLeft;

@end

@interface LGOrderView : UIView
{
    UIWebView *_webView;
    UIActivityIndicatorView *_aIView;
    id<LGOrderViewDelegate> _delegate;
}

@property(nonatomic,strong)UIActivityIndicatorView *aIView;
@property(nonatomic,strong)UIWebView* webView;
@property(nonatomic,strong)id<LGOrderViewDelegate> delegate;

-(void)loadOrderByURL:(NSString*)url;

@end
