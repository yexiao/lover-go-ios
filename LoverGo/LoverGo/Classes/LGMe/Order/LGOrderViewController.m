//
//  LGOrderViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-23.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtilDefine.h"
#import "Toast+UIView.h"
#import "LGOrderViewController.h"

@interface LGOrderViewController ()

@end

@implementation LGOrderViewController

-(id)initWithOrderInfo:(LGLGOrderInfo*)info
{
    self = [super init];
    if (self) {
        _info = info;
    }
    return self;
}
-(void)loadView
{
    _aView = [[LGOrderView alloc] initWithFrame:CGRectMake(0, STATUSBAR +TOOLBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR +TOOLBAR))];
    _aView.delegate = self;
    _aView.webView.delegate = self;
    [self setView:_aView];
    [_aView loadOrderByURL:[NSString stringWithFormat:@"%@%@?orderUuid=%@",baseUrl,payAOrderPath,_info.uuid]];
}
-(void)turnBackLeft
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
