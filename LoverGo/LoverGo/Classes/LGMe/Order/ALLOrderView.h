//
//  ALLOrderView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QCSlideSwitchView.h"

@protocol ALLOrderViewDelegate <NSObject>

-(void)turnBackLeft;

@end

@interface ALLOrderView : UIView
{
    QCSlideSwitchView *_slideSwitchView;
    id<ALLOrderViewDelegate> _delegate;
}
@property(nonatomic,strong)UITableView* oTView;
@property(nonatomic,strong)id<ALLOrderViewDelegate> delegate;
@property(nonatomic,strong)QCSlideSwitchView *slideSwitchView;

@end
