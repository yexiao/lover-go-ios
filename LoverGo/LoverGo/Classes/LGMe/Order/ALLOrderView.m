//
//  ALLOrderView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "ALLOrderView.h"

@implementation ALLOrderView

@synthesize oTView = _oTView;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"订单详情"];
        titleLab.userInteractionEnabled = YES;
        [self addSubview:titleLab];
        
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:1];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        _slideSwitchView = [[QCSlideSwitchView alloc] initWithFrame:CGRectMake(0, STATUSBAR+40, SCREEN_WIDTH, SCREEN_HEIGHT -40 - STATUSBAR) size:65.0f];
        _slideSwitchView.tabItemNormalColor = [QCSlideSwitchView colorFromHexRGB:@"000000"];
        _slideSwitchView.tabItemSelectedColor = [QCSlideSwitchView colorFromHexRGB:@"ff0000"];
        _slideSwitchView.shadowImage = [[UIImage imageNamed:@"red_line_and_shadow.png"]
                                        stretchableImageWithLeftCapWidth:80.0f topCapHeight:10.0f];
        [self addSubview:_slideSwitchView];
    }
    return self;
}
-(void)btnClieck:(UIButton*)sender
{
    [_delegate turnBackLeft];
}
@end
