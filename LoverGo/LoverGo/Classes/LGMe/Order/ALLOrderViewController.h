//
//  ALLOrderViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-24.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALLOrderView.h"
#import "SWTableViewCell.h"
#import "UnPayViewController.h"
#import "HavePayViewController.h"

@interface ALLOrderViewController : UIViewController<ALLOrderViewDelegate,SWTableViewCellDelegate,QCSlideSwitchViewDelegate>
{
    ALLOrderView *_oView;
    UnPayViewController *_vc1;
    HavePayViewController *_vc2;
}

@end
