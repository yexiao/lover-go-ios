//
//  OrderDetailView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-29.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "OrderDetailView.h"

@implementation OrderDetailView

@synthesize delegate = _delegate;

-(void)defaultLabel:(UILabel*)lab blod:(BOOL)b
{
    [lab setBackgroundColor:[UIColor whiteColor]];
    if (b) {
        [lab setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    }else{
        [lab setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    }
    [lab setTextColor:[UIColor blackColor]];
    [lab setTextAlignment:NSTextAlignmentLeft];
}

- (id)initWithFrame:(CGRect)frame withOrderInfo:(LGLGOrderInfo*)info
{
    self = [super initWithFrame:frame];
    if (self) {
        _info = info;
        [self setBackgroundColor:[UIColor grayColor]];
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"订单详情"];
        titleLab.userInteractionEnabled = true;
        [self addSubview:titleLab];
        
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:1];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        UIScrollView* cSView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, STATUSBAR+40, SCREEN_WIDTH, SCREEN_HEIGHT - STATUSBAR- 40)];
        [self addSubview:cSView];
        
        UILabel* _tLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH, 40)];
        [_tLabel setText:@"订单名称"];
        [self defaultLabel:_tLabel blod:YES];
        [cSView addSubview:_tLabel];
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 61, SCREEN_WIDTH, 40)];
        [_titleLabel setText:info.orderName];
        [self defaultLabel:_titleLabel blod:NO];
        [cSView addSubview:_titleLabel];
        
        UILabel* dtLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 125, SCREEN_WIDTH, 40)];
        [dtLabel setText:@"下单时间:"];
        [self defaultLabel:dtLabel blod:YES];
        [cSView addSubview:dtLabel];
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 166, SCREEN_WIDTH, 40)];
        [_timeLabel setText:info.insertTime];
        [self defaultLabel:_timeLabel blod:NO];
        [cSView addSubview:_timeLabel];
        
        UILabel* cLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 210, SCREEN_WIDTH, 40)];
        [cLabel setText:@"付款金额:"];
        [self defaultLabel:cLabel blod:YES];
        [cSView addSubview:cLabel];
        _costLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 251, SCREEN_WIDTH, 40)];
        [_costLabel setText:[NSString stringWithFormat:@"%0.2f",[info.totalPrice floatValue]]];
        [self defaultLabel:_costLabel blod:NO];
        [cSView addSubview:_costLabel];
        
        UILabel* sLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 300, SCREEN_WIDTH, 40)];
        [sLabel setText:@"订单状态:"];
        [self defaultLabel:sLabel blod:YES];
        [cSView addSubview:sLabel];
        _statuLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 341, SCREEN_WIDTH, 40)];
        if (info.orderState == 0) {
            [_statuLabel setText:@"未支付"];
        }else{
            [_statuLabel setText:@"已经完成支付"];
        }
        [self defaultLabel:_statuLabel blod:NO];
        [cSView addSubview:_statuLabel];
        
        //活动详情
        UILabel* showLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 390, SCREEN_WIDTH, 40)];
        [showLabel setText:@"活动详情"];
        showLabel.userInteractionEnabled = YES;
        [self defaultLabel:showLabel blod:YES];
        [cSView addSubview:showLabel];
        UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(detailActive:)];
        [showLabel addGestureRecognizer:tgr];
        
        UIImageView *picV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"next_pic.png"]];
        [picV setCenter:CGPointMake(300, 20)];
        [showLabel addSubview:picV];
        
        
        _payBtn = [LGUtil get9ScalBtn:CGRectMake(0, 440, SCREEN_WIDTH, 40) type:TypeColorOrange];
        [_payBtn setTag:1];
        if ([info.orderState compare:@"pay2"] == 0 ) {
            [_payBtn setTitle:@"已经支付" forState:UIControlStateNormal];
        }else{
            [_payBtn setTitle:@"继续支付" forState:UIControlStateNormal];
            [_payBtn addTarget:self action:@selector(payBtnClieck:) forControlEvents:UIControlEventTouchUpInside];
        }
        [cSView addSubview:_payBtn];
        
    }
    return self;
}
-(void)btnClieck:(id)sender
{
    [_delegate turnLeft];
}
-(void)payBtnClieck:(id)sender
{
    [_delegate payOrder:_info];
}
-(void)detailActive:(id)sender
{
    [_delegate toDetailAtivite:_info];
}

@end
