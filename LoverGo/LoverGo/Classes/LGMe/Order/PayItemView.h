//
//  PayItemView.h
//  LoverGo
//
//  Created by YeXiao on 13-6-25.
//  Copyright (c) 2013年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGLGOrderInfo.h"

@interface PayItemView : UITableViewCell
{
    UILabel *_titleLab;
    UILabel *_phoneLab;
    UILabel *_costLab;
}
-(void)reloadRoadData:(LGLGOrderInfo*)info;
-(void)removeAll;
@end
