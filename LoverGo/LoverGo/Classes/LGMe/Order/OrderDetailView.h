//
//  OrderDetailView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-29.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGLGOrderInfo.h"

@protocol OrderDetailViewDelegate <NSObject>

-(void)turnLeft;
-(void)payOrder:(LGLGOrderInfo*)info;
-(void)toDetailAtivite:(LGLGOrderInfo*)info;

@end

@interface OrderDetailView : UIView
{
    
    UILabel* _titleLabel;
    UILabel* _timeLabel;
    UILabel* _costLabel;
    UILabel* _statuLabel;
    UIButton* _payBtn;
    LGLGOrderInfo* _info;
    
    id<OrderDetailViewDelegate> _delegate;
}
@property(nonatomic,strong)id<OrderDetailViewDelegate> delegate;

- (id)initWithFrame:(CGRect)frame withOrderInfo:(LGLGOrderInfo*)info;

@end
