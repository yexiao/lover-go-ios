//
//  OrderDetailViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-29.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGActivityInfo.h"
#import "OrderDetailViewController.h"
#import "LGOrderViewController.h"
#import "DetailAtiviteViewController.h"

@interface OrderDetailViewController ()

@end

@implementation OrderDetailViewController

-(id)initWithOrder:(LGLGOrderInfo*)info
{
    self = [super init];
    if (self ) {
        _info = info;
    }
    return self;
}

-(void)loadView
{
    _odView = [[OrderDetailView alloc] initWithFrame:CGRectMake(0, STATUSBAR +TOOLBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR +TOOLBAR))withOrderInfo:_info];
    _odView.delegate = self;
    [self setView:_odView];
}

-(void)turnLeft
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)payOrder:(LGLGOrderInfo *)info
{
    LGOrderViewController *ocl = [[LGOrderViewController alloc] initWithOrderInfo:info];
    [self.navigationController pushViewController:ocl animated:YES];
}
-(void)toDetailAtivite:(LGLGOrderInfo *)info
{
    
    LGActivityInfo *aInfo= [[LGActivityInfo alloc]init];
    aInfo.uuid = info.activityUuid;
    
    DetailAtiviteViewController *vc = [[DetailAtiviteViewController alloc] initWithActivityInfo:aInfo];
    [self.navigationController pushViewController:vc animated:YES];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
