//
//  LGMeView.h
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGMeViewDelegate <NSObject>

-(void)LGSignInViewBtnClieck:(int)tag;

@end

@interface LGMeView : UIView
{
    id<LGMeViewDelegate> _delegate;
    UIButton* btn;
    UITableView* _meTView;
}
@property(nonatomic,strong)UITableView* meTView;
@property id<LGMeViewDelegate> delegate;


@end
