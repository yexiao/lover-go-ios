//
//  AboutViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-7-8.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "AboutViewController.h"
#import "XieYiViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

-(UILabel*)getLabel:(NSString*)title
{
    UILabel *_textLab = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, SCREEN_WIDTH, 40)];
    [_textLab setText:title];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"next_pic.png"]];
    [imgView setCenter:CGPointMake(290, STATUSBAR)];
    [_textLab addSubview:imgView];
    return _textLab;
}

-(void)loadView
{
    _aView = [[AboutView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _aView.delegate = self;
    _aView.tableView.delegate = self;
    _aView.tableView.dataSource = self;
    [self setView:_aView];
}
-(void)backLeft
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    switch (indexPath.row) {
        case mailType:
        {
            [cell addSubview:[self getLabel:@"客服邮箱:cs@lover-go.com"]];
            break;
        }
        case xieyiType:
        {
            [cell addSubview:[self getLabel:@"情侣去哪儿协议"]];
            break;
        }
        case kefuType:
        {
            [cell addSubview:[self getLabel:@"联系客服"]];
            break;
        }
            
        default:
            break;
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        return 60.0f;
    }else
    {
        return 40;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case mailType:
        {
            break;
        }
        case xieyiType:
        {
            XieYiViewController *vc = [[XieYiViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case kefuType:
        {
            _asheet = [[LXActionSheet alloc]initWithTitle:@"是否联系客服" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"呼叫" otherButtonTitles:nil];
            _asheet.tag =2;
            [_asheet showInView:self.view];
            break;
        }
        default:
            break;
    }
}
#pragma mark - LXActionSheetDelegate

- (void)didClickOnButtonIndex:(NSInteger *)buttonIndex
{
    if (_asheet.tag ==2) {
        UIWebView *web =[[UIWebView alloc]init];
        [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"tel:10086"]]];
        [self.view addSubview:web];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
