//
//  XieYiViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-7-9.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "XieYiViewController.h"

@interface XieYiViewController ()

@end

@implementation XieYiViewController

-(void)loadView
{
    _myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _myView.backgroundColor = [UIColor lightGrayColor];
    
    
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, STATUSBAR, SCREEN_WIDTH, SCREEN_HEIGHT - STATUSBAR)];
	_scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    _coreTextView = [[FTCoreTextView alloc] initWithFrame:CGRectMake(0, STATUSBAR, SCREEN_WIDTH, SCREEN_HEIGHT - STATUSBAR)];
	_coreTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    

    
    [_scrollView addSubview:_coreTextView];
    
    [_myView addSubview:_scrollView];
    
    [_coreTextView addStyles:[self coreTextStyle]];
    _coreTextView.text = [self textForView];
    _coreTextView.delegate = self;
    
    UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, SCREEN_WIDTH, 40) title:@"关于情侣去哪儿"];
    titleLab.userInteractionEnabled = YES;
    [_myView addSubview:titleLab];
    
    UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
    [returnLeftBtn setTag:0];
    [returnLeftBtn setCenter:CGPointMake(30, 20)];
    [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
    [titleLab addSubview:returnLeftBtn];
    
    [self setView:_myView];
    
}
-(void)btnClieck:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    //  We need to recalculate fit height on every layout because
    //  when the device orientation changes, the FTCoreText's width changes
    
    //  Make the FTCoreTextView to automatically adjust it's height
    //  so it fits all its rendered text using the actual width
	[_coreTextView fitToSuggestedHeight];
    
    //  Adjust the scroll view's content size so it can scroll all
    //  the FTCoreTextView's content
    [_scrollView setContentSize:CGSizeMake(CGRectGetWidth(_scrollView.bounds), CGRectGetMaxY(_coreTextView.frame)+20.0f)];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

}
- (NSString *)textForView
{
    return [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"xieyi" ofType:@"txt"] encoding:NSUTF8StringEncoding error:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSArray *)coreTextStyle
{
    NSMutableArray *result = [NSMutableArray array];
    
    //  This will be default style of the text not closed in any tag
	FTCoreTextStyle *defaultStyle = [FTCoreTextStyle new];
	defaultStyle.name = FTCoreTextTagDefault;	//thought the default name is already set to FTCoreTextTagDefault
	defaultStyle.font = [UIFont fontWithName:@"Helvetica" size:16.f];
	defaultStyle.textAlignment = FTCoreTextAlignementJustified;
	[result addObject:defaultStyle];
	
    //  Create style using convenience method
	FTCoreTextStyle *titleStyle = [FTCoreTextStyle styleWithName:@"title"];
	titleStyle.font = [UIFont fontWithName:@"Helvetica" size:40.f];
	titleStyle.paragraphInset = UIEdgeInsetsMake(20.f, 0, 25.f, 0);
	titleStyle.textAlignment = FTCoreTextAlignementCenter;
	[result addObject:titleStyle];
	
    //  Image will be centered
	FTCoreTextStyle *imageStyle = [FTCoreTextStyle new];
	imageStyle.name = FTCoreTextTagImage;
	imageStyle.textAlignment = FTCoreTextAlignementCenter;
	[result addObject:imageStyle];
	
	FTCoreTextStyle *firstLetterStyle = [FTCoreTextStyle new];
	firstLetterStyle.name = @"firstLetter";
	firstLetterStyle.font = [UIFont fontWithName:@"Helvetica" size:30.f];
	[result addObject:firstLetterStyle];
	
    //  This is the link style
    //  Notice that you can make copy of FTCoreTextStyle
    //  and just change any required properties
	FTCoreTextStyle *linkStyle = [defaultStyle copy];
	linkStyle.name = FTCoreTextTagLink;
	linkStyle.color = [UIColor orangeColor];
	[result addObject:linkStyle];
	
	FTCoreTextStyle *subtitleStyle = [FTCoreTextStyle styleWithName:@"subtitle"];
	subtitleStyle.font = [UIFont fontWithName:@"Helvetica" size:25.f];
	subtitleStyle.color = [UIColor brownColor];
	subtitleStyle.paragraphInset = UIEdgeInsetsMake(10, 0, 10, 0);
	[result addObject:subtitleStyle];
	
    //  This will be list of items
    //  You can specify custom style for a bullet
	FTCoreTextStyle *bulletStyle = [defaultStyle copy];
	bulletStyle.name = FTCoreTextTagBullet;
	bulletStyle.bulletFont = [UIFont fontWithName:@"Helvetica" size:16.f];
	bulletStyle.bulletColor = [UIColor orangeColor];
	bulletStyle.bulletCharacter = @"❧";
	bulletStyle.paragraphInset = UIEdgeInsetsMake(0, 20.f, 0, 0);
	[result addObject:bulletStyle];
    
    FTCoreTextStyle *italicStyle = [defaultStyle copy];
	italicStyle.name = @"italic";
	italicStyle.underlined = YES;
    italicStyle.font = [UIFont fontWithName:@"Helvetica" size:16.f];
	[result addObject:italicStyle];
    
    FTCoreTextStyle *boldStyle = [defaultStyle copy];
	boldStyle.name = @"bold";
    boldStyle.font = [UIFont fontWithName:@"Helvetica" size:16.f];
	[result addObject:boldStyle];
    
    FTCoreTextStyle *coloredStyle = [defaultStyle copy];
    [coloredStyle setName:@"colored"];
    [coloredStyle setColor:[UIColor redColor]];
	[result addObject:coloredStyle];
    
    return  result;
}

#pragma mark FTCoreTextViewDelegate

- (void)coreTextView:(FTCoreTextView *)acoreTextView receivedTouchOnData:(NSDictionary *)data
{
//    //  You can get detailed info about the touched links
//    
//    //  Name (type) of selected tag
//    NSString *tagName = [data objectForKey:FTCoreTextDataName];
//    
//    //  URL if the touched data was link
//    NSURL *url = [data objectForKey:FTCoreTextDataURL];
//    
//    //  Frame of the touched element
//    //  Notice that frame is returned as a string returned by NSStringFromCGRect function
//    CGRect touchedFrame = CGRectFromString([data objectForKey:FTCoreTextDataFrame]);
//    
//    //  You can get detailed CoreText information
//    NSDictionary *coreTextAttributes = [data objectForKey:FTCoreTextDataAttributes];
//    
//    NSLog(@"Received touched on element:\n"
//          @"Tag name: %@\n"
//          @"URL: %@\n"
//          @"Frame: %@\n"
//          @"CoreText attributes: %@",
//          tagName, url, NSStringFromCGRect(touchedFrame), coreTextAttributes
//          );
}

@end
