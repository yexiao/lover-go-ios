//
//  AboutView.m
//  LoverGo
//
//  Created by YeXiao on 14-7-8.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "AboutView.h"

@implementation AboutView

@synthesize delegate = _delegate;
@synthesize tableView = _tableView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"关于情侣去哪儿"];
        titleLab.userInteractionEnabled = YES;
        [self addSubview:titleLab];
        
        UIButton *returnLeftBtn = [LGUtil returnLeftBtn];
        [returnLeftBtn setTag:0];
        [returnLeftBtn setCenter:CGPointMake(30, 20)];
        [returnLeftBtn addTarget:self action:@selector(btnClieck:) forControlEvents:UIControlEventTouchUpInside];
        [titleLab addSubview:returnLeftBtn];
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, STATUSBAR+40, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR + TABBAR - 40)) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [_tableView setDirectionalLockEnabled:YES];
        [self addSubview:_tableView];
        
    }
    return self;
}
-(void)btnClieck:(UIButton *)sender
{
    [_delegate backLeft];
}

@end
