//
//  AboutView.h
//  LoverGo
//
//  Created by YeXiao on 14-7-8.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AboutViewDelegate <NSObject>

-(void)backLeft;

@end

@interface AboutView : UIView
{
    UITableView* _tableView;
    id<AboutViewDelegate> _delegate;
}
@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong) id<AboutViewDelegate> delegate;

@end
