//
//  AboutViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-7-8.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AboutView.h"
#import "LXActionSheet.h"

typedef enum {
    mailType = 1,
    xieyiType = 2,
    kefuType = 3,
}TableType;

@interface AboutViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,AboutViewDelegate,LXActionSheetDelegate>
{
    AboutView* _aView;
    LXActionSheet* _asheet;
}

@end
