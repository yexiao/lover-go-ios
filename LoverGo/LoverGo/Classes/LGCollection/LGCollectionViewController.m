//
//  LGCollectionViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-5.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGCollectionViewController.h"

@interface LGCollectionViewController ()

@end

@implementation LGCollectionViewController
-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype
{
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:img tag:mvtype];
    }
    return self;
}

-(void)loadView
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    _cView = [[LGCollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _cView.slideSwitchView.slideSwitchViewDelegate = self;
    [self setView:_cView];
    
    
    _vc1 =[[LGCPlaceViewController alloc] init];
    _vc1.title = @"地点";
    _vc2 = [[LGCRoadViewController alloc] init];
    _vc2.title = @"线路";
    _vc3 = [[LGCAticleViewController alloc] init];
    _vc3.title = @"文章";
    
    
    [_cView.slideSwitchView buildUI];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}
#pragma mark - 滑动tab视图代理方法


- (NSUInteger)numberOfTab:(QCSlideSwitchView *)view
{
    return 3;
}

- (UIViewController *)slideSwitchView:(QCSlideSwitchView *)view viewOfTab:(NSUInteger)number
{
    switch (number) {
        case 0:
            return _vc1;
            break;
        case 1:
            return _vc2;
            break;
        case 2:
            return _vc3;
            break;
        default:
            break;
    }
    return NULL;
}
- (void)slideSwitchView:(QCSlideSwitchView *)view didselectTab:(NSUInteger)number
{
    UIViewController *vController;
    switch (number) {
        case 0:
            vController = _vc1;
            break;
        case 1:
            [_vc2 loadFirst];
            vController = _vc2;
            break;
        case 2:
            [_vc3 loadFirst];
            vController =_vc3;
            break;
        default:
            break;
    }
}


@end
