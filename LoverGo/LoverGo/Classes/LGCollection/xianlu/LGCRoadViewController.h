//
//  LGCRoadViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-5.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGCRoadView.h"
#import "LGUtilModel.h"

@interface LGCRoadViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    LGCRoadView *_cRView;
    NSMutableArray* _roadArray;
    LGUtilModel *_utilModel;
}
-(void)loadFirst;
-(void)refreshView:(id)response page:(int)page;
@end
