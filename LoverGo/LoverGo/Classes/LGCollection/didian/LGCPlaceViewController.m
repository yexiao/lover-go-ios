//
//  LGCPlaceViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-5.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//
#import "MJExtension.h"
#import "LGCityCodeInfo.h"
#import "Toast+UIView.h"
#import "LGDataCenter.h"
#import "LGCPlaceItemView.h"
#import "LGRequestManager.h"
#import "LGPlaceDetailViewController.h"
#import "LGCPlaceViewController.h"

@interface LGCPlaceViewController ()

@end

@implementation LGCPlaceViewController

-(void)loadView
{
    _placeArray = [[NSMutableArray alloc] init];
    _cpView = [[LGCPlaceView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - TOOLBAR - (40+44))];
    _cpView.tableView.dataSource = self;
    _cpView.tableView.delegate = self;
    [self setView:_cpView];
    _utilModel = nil;
    
    
    __block LGCPlaceViewController *_blockSelf = self;
    _cpView.header.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //发送初始请求
        [_blockSelf sendRequestByPage:1];
    };
    _cpView.header.refreshStateChangeBlock = ^(MJRefreshBaseView* refreshViews,MJRefreshState state){
        //状态改变
    };
    [_cpView.header beginRefreshing];
    
    _cpView.footer.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //进入刷新状态
        if (_blockSelf->_utilModel) {
            int nextPage = [_blockSelf->_utilModel.nowPage intValue] +1;
            if (nextPage > [_blockSelf->_utilModel.totalPage intValue]) {
                [_blockSelf setDefaultFresh];
                [_blockSelf.view makeToast:@"已经是最新数据了!"];
                return ;
            }
            [_blockSelf sendRequestByPage:nextPage];
        }else{
            [_blockSelf setDefaultFresh];
            [_blockSelf.view makeToast:@"请先登录!"];
        }
    };
    
}
-(void)setDefaultFresh{
    [_cpView.footer endRefreshing];
    [_cpView.header endRefreshing];
}
-(void)refreshView:(id)response page:(int)page
{
    [self setDefaultFresh];
    if (response == nil) {
        [self.view makeToast:@"数据加载失败!"];
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        [self.view makeToast:@"数据解析失败!"];
        return;
    }else{
        if (page == 1) {
            [_placeArray removeAllObjects];
        }
        _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        [_placeArray addObjectsFromArray:[LGPlaceInfo objectArrayWithKeyValuesArray:_utilModel.listData]];
        [_cpView.tableView reloadData];//刷新数据
        
    }
}
-(void)sendRequestByPage:(int)page
{
    LGUserInfo  *uInfo= [LGDataCenter getSingleton].userInfo;
    if (uInfo == NULL) {
        [self setDefaultFresh];
        //用户未设置数据，请跳转至数据界面
        [self.view makeToast:@"请登录!"];
        return;
    }
    __block LGCPlaceViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          uInfo.uuid,@"userUuid",
                          @"place",@"collectionType",
                          [NSString stringWithFormat:@"%d",page],@"page",
                          nil];
    [[SVHTTPClient sharedClient] GET:getCollectionByTypeSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  //抛报错通知
                                  [self setDefaultFresh];
                                  [_blockSelf.view makeToast:@"网络连接失败，请检查网络!"];
                              }
                              [_blockSelf refreshView:response page:page];
                          }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGPlaceInfo *pInfo= [_placeArray objectAtIndex:indexPath.row];
    
    LGPlaceDetailViewController *vc = [[LGPlaceDetailViewController alloc] initWithPlaceInfo:pInfo haveRigh:NO];
    //获取view的controller
    for (UIView* next = [self.view superview]; next; next = next.superview){
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            UIViewController *mV = (UIViewController *)nextResponder;
            [mV.navigationController pushViewController:vc animated:YES];
            break;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _placeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGCPlaceItemView * cell= [[LGCPlaceItemView alloc] init];
    cell.userInteractionEnabled = YES;
    LGPlaceInfo *pInfo= [_placeArray objectAtIndex:indexPath.row];
    [cell reloadPlaceData:pInfo];
    return cell;
}

@end
