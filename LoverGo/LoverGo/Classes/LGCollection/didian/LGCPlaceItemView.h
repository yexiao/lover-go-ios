//
//  LGCPlaceItemView.h
//  LoverGo
//
//  Created by YeXiao on 14-6-29.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGPlaceInfo.h"
#import "EMAsyncImageView.h"

@interface LGCPlaceItemView : UITableViewCell
{
    EMAsyncImageView* _iconIView;
    UILabel *_titleLabel;
    UILabel *_avgLabel;
    UILabel *_contentLabel;
    UIView* _loveIconView;
}
-(void)reloadPlaceData:(LGPlaceInfo*)info;
@end
