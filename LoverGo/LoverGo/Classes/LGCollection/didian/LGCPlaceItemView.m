//
//  LGCPlaceItemView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-29.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGUtilDefine.h"
#import "LGCPlaceItemView.h"

@implementation LGCPlaceItemView

- (id)init
{
    self = [super init];
    if (self){
        _iconIView = [[EMAsyncImageView alloc] initWithFrame:CGRectMake(2, 2, 120, 96) cornerRadius:0];//701,466
        [self.contentView addSubview:_iconIView];
        _iconIView.userInteractionEnabled = YES;
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(132, 2, 180, 19)];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        //        [_titleLabel setShadowColor:[UIColor grayColor]];
        [_titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
        //        [_titleLabel setShadowOffset:CGSizeMake(0.05, 0.05)];
        [_titleLabel setTextColor:[UIColor blackColor]];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_titleLabel];
        
        _loveIconView = [[UIView alloc] initWithFrame:CGRectMake(132, 32, 100, 20)];
        [self.contentView addSubview:_loveIconView];
        
        _avgLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 34, 100, 18)];
        [_avgLabel setBackgroundColor:[UIColor clearColor]];
        //        [_avgLabel setShadowColor:[UIColor grayColor]];
        [_avgLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
        [_avgLabel setTextAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:_avgLabel];
        
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(132, 60, 190, 18)];
        [_contentLabel setBackgroundColor:[UIColor clearColor]];
        //        [_contentLabel setShadowColor:[UIColor grayColor]];
        [_contentLabel setFont:[UIFont fontWithName:@"Helvetica" size:14]];
        [_contentLabel setTextAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_contentLabel];
    }
    return self;
}
-(void)reloadPlaceData:(LGPlaceInfo*)info
{
    [_titleLabel setText:info.placeName];
    [_avgLabel setText:[NSString stringWithFormat:@"人均:%@ 元",info.payRank]];
    //多行显示
    CGSize maxSize = CGSizeMake(190, 18*2);
    CGSize labelSize = [info.detailAdress sizeWithFont:_contentLabel.font constrainedToSize:maxSize lineBreakMode:NSLineBreakByTruncatingTail];
    _contentLabel.frame = CGRectMake(_contentLabel.frame.origin.x, _contentLabel.frame.origin.y, labelSize.width, labelSize.height);
    [_contentLabel setText:info.detailAdress];
    _contentLabel.numberOfLines = 2;
    [_contentLabel sizeToFit];
    
    
    [self setLoveIcon:[info.romanticPoint intValue]];
    
    if ([info.icoImageUrl compare:@""] != 0) {
        _iconIView.imageUrl = [NSString stringWithFormat:@"%@lover-go/admin/%@",baseUrl,info.icoImageUrl];
    }else
    {
        [_iconIView setImage:[LGUtil getUnFoundImage]];
    }
}
-(void)setLoveIcon:(int)f
{
    for (UIView *view in [_loveIconView subviews]) {
        [view removeFromSuperview];
    }
    if (f<0){
        f = 1;
    }
    if (f>=5) {
        f=5;
    }
    
    for (int i = 1; i<= 5; i++) {
        UIImageView* icon = [[UIImageView alloc] initWithFrame:CGRectMake((i-1) * 20, 1, 20, 20)];
        if (i<=f) {
            [icon setImage:[UIImage imageNamed:@"hot_heart_red.png"]];
        }else{
            [icon setImage:[UIImage imageNamed:@"hot_heart_gray.png"]];
        }
        [_loveIconView addSubview:icon];
    }
}
@end
