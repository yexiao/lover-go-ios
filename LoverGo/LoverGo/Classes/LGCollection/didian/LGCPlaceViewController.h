//
//  LGCPlaceViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-5.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGCPlaceView.h"
#import "LGUtilModel.h"

@interface LGCPlaceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    LGCPlaceView *_cpView;
    NSMutableArray* _placeArray;
    LGUtilModel *_utilModel;
}

-(void)refreshView:(id)response page:(int)page;
@end
