//
//  LGCollectionView.m
//  LoverGo
//
//  Created by YeXiao on 14-6-5.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGUtil.h"
#import "LGCollectionView.h"

@implementation LGCollectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *titleLab = [LGUtil getTitleLab:CGRectMake(0, STATUSBAR, frame.size.width, 40) title:@"收藏"];
        [titleLab setBackgroundColor:[UIColor orangeColor]];
        [self addSubview:titleLab];
        
        _slideSwitchView = [[QCSlideSwitchView alloc] initWithFrame:CGRectMake(0, STATUSBAR+40, SCREEN_WIDTH, SCREEN_HEIGHT - 40 - TOOLBAR) size:50.0f];
        _slideSwitchView.tabItemNormalColor = [QCSlideSwitchView colorFromHexRGB:@"000000"];
        _slideSwitchView.tabItemSelectedColor = [QCSlideSwitchView colorFromHexRGB:@"ff0000"];
        _slideSwitchView.shadowImage = [[UIImage imageNamed:@"red_line_and_shadow.png"]
                                        stretchableImageWithLeftCapWidth:80.0f topCapHeight:10.0f];
        [self addSubview:_slideSwitchView];
    }
    return self;
}


@end
