//
//  LGCAticleViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-6-29.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "MJExtension.h"
#import "LGAticleItemView.h"
#import "Toast+UIView.h"
#import "LGCityCodeInfo.h"
#import "LGDataCenter.h"
#import "LGRequestManager.h"
#import "LGCAticleViewController.h"
#import "LGDetailAticleViewViewController.h"

@interface LGCAticleViewController ()

@end

@implementation LGCAticleViewController
-(void)loadView
{
    _roadArray = [[NSMutableArray alloc] init];
    _cAView = [[LGCAticleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - TOOLBAR - (40+44))];
    _cAView.tableView.dataSource = self;
    _cAView.tableView.delegate = self;
    [self setView:_cAView];
    _utilModel = nil;
}
-(void)loadFirst
{
    __block LGCAticleViewController *_blockSelf = self;
    _cAView.header.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //发送初始请求
        [_blockSelf sendRequestByPage:1];
    };
    [_cAView.header beginRefreshing];
    
    _cAView.footer.beginRefreshingBlock = ^(MJRefreshBaseView* refreshViews){
        //进入刷新状态
        if (_blockSelf->_utilModel) {
            int nextPage = [_blockSelf->_utilModel.nowPage intValue] +1;
            if (nextPage > [_blockSelf->_utilModel.totalPage intValue]) {
                [_blockSelf setDefaultFresh];
                [_blockSelf.view makeToast:@"数据已经是最新的啦!"];
                return ;
            }
            [_blockSelf sendRequestByPage:nextPage];
        }else{
            [_blockSelf setDefaultFresh];
            [_blockSelf.view makeToast:@"请先登录!"];
        }
    };
}

-(void)setDefaultFresh{
    [_cAView.footer endRefreshing];
    [_cAView.header endRefreshing];
}
-(void)refreshView:(id)response page:(int)page
{
    [self setDefaultFresh];
    if (response == nil) {
        [self.view makeToast:@"加载失败，没有请求到数据!"];
        return;
    }
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&jsonError];
    if (jsonError) {
        [self.view makeToast:@"解析失败!"];
    }else{
        if (page == 1) {
            [_roadArray removeAllObjects];
        }
        _utilModel = [LGUtilModel objectWithKeyValues:jsonObject];
        [_roadArray addObjectsFromArray:[LGArticleInfo objectArrayWithKeyValuesArray:_utilModel.listData]];
        [_cAView.tableView reloadData];//刷新数据
        
    }
}
-(void)sendRequestByPage:(int)page
{
    LGUserInfo  *uInfo= [LGDataCenter getSingleton].userInfo;
    if (uInfo == NULL) {
        //用户未设置数据，请跳转至数据界面
        [self setDefaultFresh];
        [self.view makeToast:@"请先登录哦!"];
        return;
    }
    __block LGCAticleViewController *_blockSelf = self;
    NSDictionary *para = [[NSDictionary alloc] initWithObjectsAndKeys:
                          uInfo.uuid,@"userUuid",
                          @"article",@"collectionType",
                          [NSString stringWithFormat:@"%d",page],@"page",
                          nil];
    [[SVHTTPClient sharedClient] GET:getCollectionByTypeSPath
                          parameters:para
                          completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
                              if (error) {
                                  //抛报错通知
                                  [self setDefaultFresh];
                                  [_blockSelf.view makeToast:@"网络连接失败，请检查网络!"];
                              }
                              [_blockSelf refreshView:response page:page];
                          }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGArticleInfo *ccInfo= [_roadArray objectAtIndex:indexPath.row];
    
    LGDetailAticleViewViewController *vc = [[LGDetailAticleViewViewController alloc] initWithActivityInfo:ccInfo haveR:NO];
    //获取view的controller
    for (UIView* next = [self.view superview]; next; next = next.superview){
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            UIViewController *mV = (UIViewController *)nextResponder;
            [mV.navigationController pushViewController:vc animated:YES];
            break;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _roadArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGAticleItemView * cell = [[LGAticleItemView alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    LGArticleInfo *ccInfo= [_roadArray objectAtIndex:indexPath.row];
    [cell reloadArticleData:ccInfo imgIsLeft:YES];
    return cell;
}
@end
