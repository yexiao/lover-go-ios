//
//  LGCAticleViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-29.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGUtilModel.h"
#import "LGCAticleView.h"

@interface LGCAticleViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    LGCAticleView *_cAView;
    NSMutableArray* _roadArray;
    LGUtilModel *_utilModel;
}
-(void)loadFirst;
-(void)refreshView:(id)response page:(int)page;
@end
