//
//  LGCollectionViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-6-5.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGCollectionView.h"
#import "BaseViewController.h"
#import "LGCRoadViewController.h"
#import "LGCAticleViewController.h"
#import "LGCPlaceViewController.h"

@interface LGCollectionViewController : UIViewController<QCSlideSwitchViewDelegate>
{
    LGCollectionView* _cView;
    LGCPlaceViewController *_vc1;
    LGCRoadViewController *_vc2;
    LGCAticleViewController *_vc3;
}
-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype;

@end
