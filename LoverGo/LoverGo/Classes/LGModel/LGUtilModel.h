//
//  LGUtilModel.h
//  LoverGo
//
//  Created by YeXiao on 14-4-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGUtilModel : NSObject

@property(nonatomic,strong) NSArray* listCount;
@property(nonatomic,strong) NSArray* listData;
@property(nonatomic,strong) NSString* nowPage;
@property(nonatomic,strong) NSString* page;
@property(nonatomic,strong) NSString* rowsPerPage;
@property(nonatomic,strong) NSString* totalNum;
@property(nonatomic,strong) NSString* totalPage;
@property(nonatomic,strong) NSString* status;

@end
