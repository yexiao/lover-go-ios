//
//  LGUserInfo.h
//  LoverGo
//
//  Created by YeXiao on 14-2-16.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGUserInfo : NSObject

@property(nonatomic,strong) NSString* birthday;
@property(nonatomic,strong) NSString* bpw;
@property(nonatomic,strong) NSString* email;
@property(nonatomic,strong) NSString* friendEmail;
@property(nonatomic,strong) NSString* friendUuid;
@property(nonatomic,strong) NSString* grade;
@property(nonatomic,strong) NSString* headImg;
@property(nonatomic,strong) NSString* ifDelete;
@property(nonatomic,strong) NSString* insertTime;
@property(nonatomic,strong) NSString* machineSn;
@property(nonatomic,strong) NSString* mobile;
@property(nonatomic,strong) NSString* password;
@property(nonatomic,strong) NSString* qq;
@property(nonatomic,strong) NSString* regionUuid;
@property(nonatomic,strong) NSString* rankPoints;
@property(nonatomic,strong) NSString* regTime;
@property(nonatomic,strong) NSString* say;
@property(nonatomic,assign) int sex;
@property(nonatomic,strong) NSString* userName;
@property(nonatomic,strong) NSString* uuid;


@end
