//
//  LGLGOrderInfo.h
//  LoverGo
//
//  Created by YeXiao on 14-6-23.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGLGOrderInfo : NSObject

@property(nonatomic,strong) NSString* activityUuid;
@property(nonatomic,strong) NSString* appUserEmail;
@property(nonatomic,strong) NSString* appUserName;
@property(nonatomic,strong) NSString* appUserTel;
@property(nonatomic,strong) NSString* appUserUuid;
@property(nonatomic,strong) NSString* ifDelete;
@property(nonatomic,strong) NSString* insertTime;
@property(nonatomic,strong) NSString* orderName;
@property(nonatomic,strong) NSString* orderNo;
@property(nonatomic,strong) NSString* orderState;
@property(nonatomic,strong) NSString* totalPrice;
@property(nonatomic,strong) NSString* uuid;

@end
