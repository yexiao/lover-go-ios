//
//  LGPlaceInfo.h
//  LoverGo
//
//  Created by YeXiao on 14-4-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LGUserPlacesInfo.h"

@interface LGPlaceInfo : NSObject


@property(nonatomic,strong) NSString* adminUser;
@property(nonatomic,strong) NSString* areaIntro;
@property(nonatomic,strong) NSString* arrCount;
@property(nonatomic,strong) NSString* bussinessState;
@property(nonatomic,strong) NSString* checkstate;
@property(nonatomic,strong) NSString* closeTime;
@property(nonatomic,strong) NSString* content;
@property(nonatomic,strong) NSString* detailAdress;
@property(nonatomic,strong) NSString* empPerformance;
@property(nonatomic,strong) NSString* enviorment;
@property(nonatomic,strong) NSString* filmUrl;
@property(nonatomic,strong) NSString* goodSeason;
@property(nonatomic,strong) NSString* goodTime;
@property(nonatomic,strong) NSString* goodTimeLenth;
@property(nonatomic,strong) NSString* goodTraffic;
@property(nonatomic,strong) NSString* goodWhether;
@property(nonatomic,strong) NSString* htmlName;
@property(nonatomic,strong) NSString* icoImageUrl;
@property(nonatomic,strong) NSString* ifAccount;
@property(nonatomic,strong) NSString* ifDelete;
@property(nonatomic,strong) NSString* insertTime;
@property(nonatomic,strong) NSString* introType;
@property(nonatomic,strong) NSString* latitude;
@property(nonatomic,strong) NSString* listOrder;
@property(nonatomic,strong) NSString* longitude;
@property(nonatomic,strong) NSString* menu;
@property(nonatomic,strong) NSString* openTime;
@property(nonatomic,strong) NSString* parkingIntro;
@property(nonatomic,strong) NSString* payPrice;
@property(nonatomic,strong) NSString* payRank;
@property(nonatomic,strong) NSString* placeCat;
@property(nonatomic,strong) NSString* placeCateTypeParent;
@property(nonatomic,strong) NSString* placeInfo;
@property(nonatomic,strong) NSString* placeIntro;
@property(nonatomic,strong) NSString* placeIntroM;
@property(nonatomic,strong) NSString* placeIntroW;
@property(nonatomic,strong) NSString* placeName;
@property(nonatomic,strong) NSString* placeStory;
@property(nonatomic,strong) NSString* placeType;
@property(nonatomic,strong) NSString* regionCode;
@property(nonatomic,assign) NSString* romanticPoint;
@property(nonatomic,strong) NSString* romanticReason;
@property(nonatomic,strong) NSString* submitUserName;
@property(nonatomic,strong) NSString* tel;
@property(nonatomic,strong) NSString* userPlaces;//对象
@property(nonatomic,strong) NSString* uuid;
@property(nonatomic,strong) NSString* waitIntro;
@property(nonatomic,strong) NSString* webIntro;


@end
