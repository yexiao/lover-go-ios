//
//  LGRoadDetailInfo.h
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGRoadDetailInfo : NSObject

@property(nonatomic,strong) NSString* content;
@property(nonatomic,strong) NSString* endTime;
@property(nonatomic,strong) NSString* favourCount;
@property(nonatomic,strong) NSString* ifDelete;
@property(nonatomic,strong) NSString* ifShare;
@property(nonatomic,strong) NSString* insertTime;
@property(nonatomic,strong) NSString* place;
@property(nonatomic,strong) NSString* routeOrder;
@property(nonatomic,strong) NSString* routeUuid;
@property(nonatomic,strong) NSString* signImg;
@property(nonatomic,strong) NSString* signText;
@property(nonatomic,strong) NSString* starTime;
@property(nonatomic,strong) NSString* traffic;
@property(nonatomic,strong) NSString* userPlaceType;
@property(nonatomic,strong) NSString* userUuid;
@property(nonatomic,strong) NSString* uuid;
@property(nonatomic,strong) NSString* uuidPlace;
@end
