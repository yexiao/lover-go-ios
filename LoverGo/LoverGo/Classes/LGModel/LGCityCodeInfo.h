//
//  LGCityCodeInfo.h
//  LoverGo
//
//  Created by YeXiao on 14-5-25.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGCityCodeInfo : NSObject


@property(nonatomic,strong) NSString* addTime;
@property(nonatomic,strong) NSString* cityCode;
@property(nonatomic,strong) NSString* favourCount;
@property(nonatomic,strong) NSString* goTime;
@property(nonatomic,strong) NSString* headImg;
@property(nonatomic,strong) NSString* ifDelete;
@property(nonatomic,strong) NSString* ifShare;
@property(nonatomic,strong) NSString* regionUuid;
@property(nonatomic,strong) NSString* routeDesc;
@property(nonatomic,strong) NSString* routeName;
@property(nonatomic,strong) NSString* routeType;
@property(nonatomic,strong) NSString* totalPrice;
@property(nonatomic,strong) NSString* userName;
@property(nonatomic,strong) NSString* userUuid;
@property(nonatomic,strong) NSString* uuid;


@end
