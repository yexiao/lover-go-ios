//
//  LGActivityImageInfo.h
//  LoverGo
//
//  Created by YeXiao on 14-6-2.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGActivityImageInfo : NSObject

@property(nonatomic,strong) NSString* content;
@property(nonatomic,strong) NSString* ifDelete;
@property(nonatomic,strong) NSString* imageType;
@property(nonatomic,strong) NSString* imageUrl;
@property(nonatomic,strong) NSString* listOrder;
@property(nonatomic,strong) NSString* ojectUuid;
@property(nonatomic,strong) NSString* smallImageUrl;
@property(nonatomic,strong) NSString* uuid;

@end
