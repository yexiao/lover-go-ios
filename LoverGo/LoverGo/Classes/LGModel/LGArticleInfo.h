//
//  LGArticleInfo.h
//  LoverGo
//
//  Created by YeXiao on 14-5-26.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGArticleInfo : NSObject


@property(nonatomic,strong) NSString* articleAddTime;
@property(nonatomic,strong) NSString* articleAuthor;
@property(nonatomic,strong) NSString* articleContent;
@property(nonatomic,strong) NSString* articleIntro;
@property(nonatomic,strong) NSString* articleKeyword;
@property(nonatomic,strong) NSString* articleOrder;
@property(nonatomic,strong) NSString* articleTitle;
@property(nonatomic,strong) NSString* ifDelete;
@property(nonatomic,strong) NSString* catCode;
@property(nonatomic,strong) NSString* htmlName;
@property(nonatomic,strong) NSString* indexImage;
@property(nonatomic,strong) NSString* placeUuid;
@property(nonatomic,strong) NSString* regionCode;
@property(nonatomic,strong) NSString* uuid;


@end
