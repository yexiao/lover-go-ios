//
//  LGActivityInfo.h
//  LoverGo
//
//  Created by YeXiao on 14-6-1.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGActivityInfo : NSObject

@property(nonatomic,strong) NSString* actTime;
@property(nonatomic,strong) NSString* activityNum;
@property(nonatomic,strong) NSString* activityState;
@property(nonatomic,strong) NSString* adminUserUuid;
@property(nonatomic,strong) NSString* description;
@property(nonatomic,strong) NSString* endTime;
@property(nonatomic,strong) NSString* canJoin;
@property(nonatomic,strong) NSString* htmlName;
@property(nonatomic,strong) NSString* htmlSource;
@property(nonatomic,strong) NSString* ifDelete;
@property(nonatomic,strong) NSString* indexImage;
@property(nonatomic,strong) NSString* insertTime;
@property(nonatomic,strong) NSString* joinNum;
@property(nonatomic,strong) NSString* placeUuid;
@property(nonatomic,strong) NSString* price;
@property(nonatomic,strong) NSString* regionCode;
@property(nonatomic,strong) NSString* title;
@property(nonatomic,strong) NSString* uuid;
@end
