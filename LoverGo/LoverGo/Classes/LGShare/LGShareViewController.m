//
//  LGShareViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGShareViewController.h"

@interface LGShareViewController ()

@end

@implementation LGShareViewController

-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype
{
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:img tag:mvtype];
    }
    return self;
}


-(void)loadView
{
    _mainShareView = [[LGShareView alloc] initWithFrame:CGRectMake(0, STATUSBAR, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self setView:_mainShareView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}


@end
