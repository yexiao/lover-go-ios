//
//  LGShareViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGShareView.h"
#import "BaseViewController.h"

@interface LGShareViewController : UIViewController
{
    LGShareView *_mainShareView;
}
-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype;
@end
