//
//  LGShareView.m
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGShareView.h"

@implementation LGShareView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor redColor]];
    }
    return self;
}

@end
