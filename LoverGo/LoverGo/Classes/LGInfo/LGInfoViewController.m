//
//  LGInfoViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "MMLocationManager.h"
#import "LGInfoViewController.h"
#import "LGCityViewController.h"
#import "LGNavigationController.h"
#import "LGInfoItemViewController.h"
#import "LGMainViewController.h"

@interface LGInfoViewController ()

@end

@implementation LGInfoViewController

-(void)loadView
{
//    NSLog(@"width:%d height:%d\n",SCREEN_WIDTH,SCREEN_HEIGHT);
    _infoView = [[LGInfoView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _infoView.toolBarView.delegate = self;
    _infoView.tableView.delegate = self;
    _infoView.tableView.dataSource = self;
    [self setView:_infoView];
    [_infoView.tableView reloadData];
    
}
-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype
{
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:img tag:mvtype];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MMLocationManager shareLocation] getCity:^(NSString *cityString) {
        if (cityString == nil) {
            [MMLocationManager shareLocation].lastCity =@"北京";
            [MMLocationManager shareLocation].latitude =39.92;
            [MMLocationManager shareLocation].longitude =116.46;
        }
        [_infoView.toolBarView setLabelText:[MMLocationManager shareLocation].lastCity];
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshCityName)
                                                 name:CityCangeNotification object:nil];
    
	// Do any additional setup after loading the view.
}
-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:CityCangeNotification name:nil object:self];
}
-(void)refreshCityName
{
    [_infoView.toolBarView setLabelText:[MMLocationManager shareLocation].lastCity];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)toSelectLocation
{
    LGCityViewController *vController= [[LGCityViewController alloc] init];
    [self presentModalViewController:vController animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    if (indexPath.row == 3) {
        
        [cell.textLabel setText:[NSString stringWithFormat:@"%d",self.navigationController.viewControllers.count]];
    }
    else{
    [cell.textLabel setText:[NSString stringWithFormat:@"%d",indexPath.row +1]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LGInfoItemViewController *vc = [[LGInfoItemViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
