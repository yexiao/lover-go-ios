//
//  LGInfoViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGInfoView.h"
#import "BaseViewController.h"

@interface LGInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,LGInfoTooBarViewDelegate>
{
    NSArray *_provinces;
    NSArray	*_cities;
    LGInfoView* _infoView;
}

-(id)initWithTitle:(NSString *)title image:(UIImage *)img type:(MainViewType)mvtype;
-(void)refreshCityName;

@end
