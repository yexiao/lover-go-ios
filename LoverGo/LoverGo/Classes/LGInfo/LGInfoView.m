//
//  LGInfoView.m
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGInfoView.h"

@implementation LGInfoView

@synthesize tableView = _tableView;
@synthesize toolBarView = _toolBarView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _toolBarView = [[LGInfoTooBarView alloc] initWithFrame:CGRectMake(0, STATUSBAR, SCREEN_WIDTH,TOOLBAR)];
        [self addSubview:_toolBarView];
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, STATUSBAR +TOOLBAR, SCREEN_WIDTH, SCREEN_HEIGHT - (STATUSBAR +TOOLBAR + TABBAR)) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [self addSubview:_tableView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
