//
//  LGInfoView.h
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGInfoTooBarView.h"

@interface LGInfoView : UIView
{
    LGInfoTooBarView* _toolBarView;
    UITableView* _tableView;
}
@property LGInfoTooBarView* toolBarView;
@property UITableView* tableView;
@end
