//
//  LGInfoTooBarView.h
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGInfoTooBarViewDelegate <NSObject>

-(void)toSelectLocation;

@end

@interface LGInfoTooBarView : UIView
{
    UIButton* _locationBtn;
    id<LGInfoTooBarViewDelegate> _delegate;
    UILabel* _locaLabel;
    UIImageView* _localImgView;
    UIButton* _mapBtn;
}
@property UIButton* locationBtn;
@property id<LGInfoTooBarViewDelegate> delegate;

-(void)locationBtnClieck;
-(void)setLabelText:(NSString*)text;
@end
