//
//  LGInfoTooBarView.m
//  LoverGo
//
//  Created by YeXiao on 14-1-14.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGInfoTooBarView.h"
#import "MMLocationManager.h"

@implementation LGInfoTooBarView

@synthesize locationBtn = _locationBtn;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImageView* logoImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lovego_logo.png"]];
        [logoImgView setCenter:CGPointMake(51/2.,44/2.)];
        [self addSubview:logoImgView];
        
        _locationBtn = [[UIButton alloc] initWithFrame:CGRectMake(51, 44-25, 80, 25)];
        [_locationBtn addTarget:self action:@selector(locationBtnClieck) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_locationBtn];
        
        _locaLabel = [[UILabel alloc] init];
        [_locationBtn addSubview:_locaLabel];
        
        _localImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"util_right_tip.png"]];
        [_locationBtn addSubview:_localImgView];
        CGAffineTransform rotate =CGAffineTransformMakeRotation(M_PI/2);
        [_localImgView setTransform:rotate];
        
        [self setLabelText:[MMLocationManager shareLocation].lastCity];
        
        _mapBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 80, 0, 80, 49)];
        [_mapBtn setImage:[UIImage imageNamed:@"info_map_nomal.png"] forState:UIControlStateNormal];
        [_mapBtn setImage:[UIImage imageNamed:@"info_map_sel.png"] forState:UIControlStateSelected];
        [_mapBtn addTarget:self action:@selector(locationBtnClieck) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_mapBtn];
    }
    return self;
}
-(void)setLabelText:(NSString*)text
{
    [_locaLabel setText:text];
    [_locaLabel setBackgroundColor:[UIColor clearColor]];
    [_locaLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    //设置一个行高上限
    CGSize size = CGSizeMake(320,25);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [_locaLabel.text sizeWithFont:_locaLabel.font constrainedToSize:size];
    [_locaLabel  setFrame:CGRectMake(0, 0, labelsize.width, labelsize.height)];
    
    [_localImgView setCenter:CGPointMake(labelsize.width + 11, 8)];

}
-(void)locationBtnClieck
{
    [_delegate toSelectLocation];
}

@end
