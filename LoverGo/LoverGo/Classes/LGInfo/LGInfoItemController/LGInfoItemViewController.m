//
//  LGInfoItemViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-1-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGInfoItemViewController.h"

@interface LGInfoItemViewController ()

@end

@implementation LGInfoItemViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(50, 240, 220, 50)];
    [button setBackgroundColor:[UIColor grayColor]];
    [button setTitle:@"点击进入下一层界面" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = [NSString stringWithFormat:@"第%d层界面",self.navigationController.viewControllers.count];
    [self.navigationController setNavigationBarHidden:NO];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(50, 150, 220, 50)];
    [btn setBackgroundColor:[UIColor grayColor]];
    [btn setTitle:@"左滑动返回" forState:UIControlStateNormal];
    [self.view addSubview:btn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)pressBtn:(UIButton *)sender
{
    
    LGInfoItemViewController *vc = [[LGInfoItemViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}
@end
