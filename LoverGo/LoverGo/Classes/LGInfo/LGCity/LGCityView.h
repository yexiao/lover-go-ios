//
//  LGCityView.h
//  LoverGo
//
//  Created by YeXiao on 14-1-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGCityViewDelegate <NSObject>

-(void)canelBtnClieck;

@end

@interface LGCityView : UIView
{
    UITableView* _tableView;
    id<LGCityViewDelegate> _delegate;
}
@property UITableView* tableView;
@property id<LGCityViewDelegate> delegate;

@end
