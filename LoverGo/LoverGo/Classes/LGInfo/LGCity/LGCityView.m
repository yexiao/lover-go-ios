//
//  LGCityView.m
//  LoverGo
//
//  Created by YeXiao on 14-1-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGCityView.h"
#import "MMLocationManager.h"

@implementation LGCityView

@synthesize tableView = _tableView;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, STATUSBAR +77/2., frame.size.width, frame.size.height - ( STATUSBAR +77/2.)) style:UITableViewStyleGrouped];
        _tableView.allowsSelection = YES;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [self addSubview:_tableView];
        
        
        UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, STATUSBAR, 72/2., 77/2.)];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"info_city_close_btn_n.png"] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(btnClieck) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backBtn];
        
        UILabel *locaLab = [[UILabel alloc] initWithFrame:CGRectMake(0, STATUSBAR, frame.size.width, 77/2.)];
        [locaLab setBackgroundColor:[UIColor clearColor]];
        [locaLab setTextAlignment:NSTextAlignmentCenter];
        [locaLab setText:[NSString stringWithFormat:@"当前选择城市:%@",[MMLocationManager shareLocation].lastCity]];
        [self addSubview:locaLab];
    }
    return self;
}
-(void)btnClieck
{
    [_delegate canelBtnClieck];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
