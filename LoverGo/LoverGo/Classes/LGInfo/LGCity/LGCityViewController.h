//
//  LGCityViewController.h
//  LoverGo
//
//  Created by YeXiao on 14-1-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGCityView.h"

#define CityCangeNotification @"CityCangeNotification"

@interface LGCityViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,LGCityViewDelegate>
{
    NSArray *_provinces;
    NSArray	*_cities;
    LGCityView *_mainView;
}

-(void)setSelectCity;
@end
