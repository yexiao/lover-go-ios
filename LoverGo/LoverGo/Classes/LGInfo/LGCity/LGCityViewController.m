//
//  LGCityViewController.m
//  LoverGo
//
//  Created by YeXiao on 14-1-15.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import "LGCityViewController.h"
#import "MMLocationManager.h"

@interface LGCityViewController ()

@end

@implementation LGCityViewController

-(void)loadView
{
    
    //加载数据
    _provinces = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProvincesAndCities.plist" ofType:nil]];
    
    _mainView = [[LGCityView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _mainView.tableView.dataSource = self;
    _mainView.tableView.delegate = self;
    _mainView.delegate = self;
    [self setView:_mainView];
    [_mainView.tableView reloadData];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [_provinces count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSArray *citySection = [[_provinces objectAtIndex:section] objectForKey:@"Cities"];
    return [citySection count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    NSDictionary *dic =[_provinces objectAtIndex:indexPath.section];
    NSArray* arr = [dic objectForKey:@"Cities"];
    NSDictionary* cityDic = [arr objectAtIndex:indexPath.row];
    
    [cell.textLabel setText:[cityDic objectForKey:@"city"]];
    
    return cell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSDictionary *key = [_provinces objectAtIndex:section];
    
    return [key objectForKey:@"State"];;
}

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{//索引
//    return _provinces;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic =[_provinces objectAtIndex:indexPath.section];
    NSArray* arr = [dic objectForKey:@"Cities"];
    NSDictionary* cityDic = [arr objectAtIndex:indexPath.row];
    
    [MMLocationManager shareLocation].lastCity =[cityDic objectForKey:@"city"];
    [MMLocationManager shareLocation].latitude =[[cityDic objectForKey:@"lat"] floatValue];
    [MMLocationManager shareLocation].longitude =[[cityDic objectForKey:@"lon"] floatValue];
    
    [self setSelectCity];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:CityCangeNotification object:self];
    
    [self dismissModalViewControllerAnimated:YES];
}
-(void)canelBtnClieck
{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)setSelectCity
{
    NSUserDefaults *standard = [NSUserDefaults standardUserDefaults];
    [standard setObject:[MMLocationManager shareLocation].lastCity forKey:MMLastCity];
    [standard setObject:[NSString stringWithFormat:@"%f",[MMLocationManager shareLocation].latitude] forKey:MMLastLatitude];
    [standard setObject:[NSString stringWithFormat:@"%f",[MMLocationManager shareLocation].longitude] forKey:MMLastLongitude];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
