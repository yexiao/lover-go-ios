//
//  main.m
//  LoverGo
//
//  Created by YeXiao on 14-1-10.
//  Copyright (c) 2014年 yexiao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LGAppDelegate class]));
    }
}
